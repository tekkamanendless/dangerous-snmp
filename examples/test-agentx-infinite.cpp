#include <iostream>
#include <thread>

#include <fstream>
#include <cstring>
#include <sys/types.h>
#include <dirent.h>

#include <dangerous/snmp/agentx.hpp>
#include <dangerous/snmp/logger.hpp>
using namespace dangerous;

int main( int argc, char** argv ) {
	snmp::logger.system( snmp::Logger::AGENTX, true );
	for( int i = 0; i < snmp::Logger::MAX; i++ ) {
//		snmp::logger.system( (snmp::Logger::System)i, true );
	}

	snmp::Context context;
	snmp::AgentX agentx( context );

	agentx.description( "Test Agent / Infinite" );

	agentx.uri( "tcp://localhost:705" );

	agentx.addAgentCapabilities( ".1.3.6.1.4.1.41326.999.1", "Dangerous SNMP test / 1000" );

	snmp::AgentX::OidRegistrationEntry oidRegistrationEntry;
	oidRegistrationEntry.type = decltype( oidRegistrationEntry )::TREE;
	oidRegistrationEntry.getFunction = []( const snmp::NumericOid& oid, snmp::Variant& value ) -> bool {
		if( oid.numbers.size() != 9 + 4 ) {
			return false;
		}

		value.set_uint32_t( snmp::Variant::APPLICATION_GAUGE32, oid.numbers[ oid.numbers.size() - 1 ] );
		return true;
	};
	oidRegistrationEntry.getNextFunction = []( const snmp::NumericOid& oid, snmp::NumericOid& nextOid, snmp::Variant& value ) -> bool {
		nextOid = oid;

		if( nextOid.numbers.size() < 9 + 4 ) {
			nextOid.numbers.resize( 9 + 4 );
		}
		if( nextOid.numbers.size() > 9 + 4 ) {
			nextOid.numbers.resize( 9 + 4 );
		}

		bool success = false;
		for( unsigned int i = 9 + 4 - 1; i >= 9; i-- ) {
			uint32_t number = nextOid.numbers[ i ];
			if( number >= 2000000000 ) {
				number = 1;
			} else {
				number++;
				nextOid.numbers[ i ] = number;
				success = true;
				break;
			}
		}
		if( ! success ) {
			return false;
		}
		
		value.set_uint32_t( snmp::Variant::APPLICATION_GAUGE32, nextOid.numbers[ nextOid.numbers.size() - 1 ] );
		return true;
	};
	agentx.registerOid( "1.3.6.1.4.1.41326.999.1", oidRegistrationEntry );
	std::cout << "AgentX: OID: registered." << std::endl;

	while( true ) {
		std::this_thread::sleep_for( std::chrono::seconds( 30 ) );
	}

	return 0;
}

