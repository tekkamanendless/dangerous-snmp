#include <iostream>
#include <thread>

#include <fstream>
#include <cstring>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>

#include <dangerous/snmp/agentx.hpp>
#include <dangerous/snmp/logger.hpp>
using namespace dangerous;

void usage() {
	std::cout <<
		"Usage:" << std::endl <<
		"   dangerous-snmp-agentx-process [<option>[ ...]] [<address>]" << std::endl <<
		std::endl <<
		"This is an AgentX daemon that provides additional per-process statistics." << std::endl <<
		std::endl <<
		"<option> may include:" << std::endl <<
		"   --timeout <#>         | Set the timeout to <#> milliseconds." << std::endl <<
		"   --update-interval <#> | Set the update interval to <#> seconds." << std::endl <<
		std::endl <<
		"   --log <name>          | Enable logging for <name>." << std::endl <<
		"      none               |    Disable all logging." << std::endl <<
		"      all                |    Enable all logging." << std::endl <<
		"      [specifics]        |" << std::endl <<
		"      bytestream         |    Enable ByteStream logging." << std::endl <<
		"      client             |    Enable Client logging." << std::endl <<
		"      decoding           |    Enable decoding logging." << std::endl <<
		"      encoding           |    Enable encoding logging." << std::endl <<
		"      general            |    Enable general logging." << std::endl <<
		"      io                 |    Enable I/O logging." << std::endl <<
		"      parsing            |    Enable parsing logging." << std::endl <<
		"      transport          |    Enable Transport logging." << std::endl <<
		std::endl;
	exit( EXIT_FAILURE );
}


int main( int argc, char** argv ) {
	bool hasTimeout = false;
	std::chrono::milliseconds timeout;

	bool hasAddress = false;
	std::string address;
	
	std::chrono::seconds updateInterval( 15 );

	// Go through the parameters that were passed in.
	for( int i = 1; i < argc; i++ ) {
		//! This is the current parameter.
		std::string parameter( argv[i] );

		// There are two classes of parameters:
		//    1. Those that start with "-".
		//    2. Those that don't.
		if( parameter.length() > 0 && parameter[0] == '-' ) {
			// This parameter starts with "-", which means that it is
			// some kind of option.
			//
			// If there is another "-" following it, then it is a "long"
			// option (that is, one that begins with "--").
			if( parameter.length() > 1 && parameter[1] == '-' ) {
				// Note: this is a long option.
				
				if( parameter.compare( "--help" ) == 0 ) {
					usage();
				} else if( parameter.compare( "--timeout" ) == 0 ) {
					if( i + 1 >= argc ) {
						usage();
					}
					parameter = argv[ ++i ];
					timeout = std::chrono::milliseconds( atoi( parameter.c_str() ) );
					hasTimeout = true;
				} else if( parameter.compare( "--update-interval" ) == 0 ) {
					if( i + 1 >= argc ) {
						usage();
					}
					parameter = argv[ ++i ];
					updateInterval = std::chrono::seconds( atoi( parameter.c_str() ) );
				} else if( parameter.compare( "--log" ) == 0 ) {
					if( i + 1 >= argc ) {
						usage();
					}
					parameter = argv[ ++i ];

					if( parameter.compare( "all" ) == 0 ) {
						for( int i = 0; i < snmp::Logger::MAX; i++ ) {
							snmp::logger.system( (snmp::Logger::System)i, true );
						}
					} else if( parameter.compare( "none" ) == 0 ) {
						for( int i = 0; i < snmp::Logger::MAX; i++ ) {
							snmp::logger.system( (snmp::Logger::System)i, false );
						}
					} else if( parameter.compare( "agentx" ) == 0 ) {
						snmp::logger.system( snmp::Logger::AGENTX, true );
					} else if( parameter.compare( "bytestream" ) == 0 ) {
						snmp::logger.system( snmp::Logger::BYTESTREAM, true );
					} else if( parameter.compare( "client" ) == 0 ) {
						snmp::logger.system( snmp::Logger::CLIENT, true );
					} else if( parameter.compare( "decoding" ) == 0 ) {
						snmp::logger.system( snmp::Logger::DECODING, true );
					} else if( parameter.compare( "encoding" ) == 0 ) {
						snmp::logger.system( snmp::Logger::ENCODING, true );
					} else if( parameter.compare( "general" ) == 0 ) {
						snmp::logger.system( snmp::Logger::GENERAL, true );
					} else if( parameter.compare( "io" ) == 0 ) {
						snmp::logger.system( snmp::Logger::IO, true );
					} else if( parameter.compare( "parsing" ) == 0 ) {
						snmp::logger.system( snmp::Logger::PARSING, true );
					} else if( parameter.compare( "transport" ) == 0 ) {
						snmp::logger.system( snmp::Logger::TRANSPORT, true );
					}
				}
			} else {
				// Note: this is a short option.
				
				// A single "-" is not a valid option.
				if( parameter.length() < 2 ) {
					usage();
				}

				// There are no short options right now.
				usage();
			}
		} else {
			// This is just a normal parameter.  It is not a special flag
			// or anything.

			if( ! hasAddress ) {
				address = parameter;
				hasAddress = true;
			} else {
				usage();
			}
		}
	}

	if( ! hasAddress ) {
		address = "tcp://localhost:705";
	}

	snmp::Context context;
	snmp::AgentX agentx( context );

	agentx.description( "Dangerous SNMP / AgentX / Process" );
	// TODO: SET OID.

	agentx.uri( address );

	agentx.addAgentCapabilities( ".1.3.6.1.4.1.41326.3.1", "Dangerous SNMP extensions for processes" );


	std::map< snmp::NumericOid, snmp::Variant > oidMap;

	snmp::AgentX::OidRegistrationEntry oidRegistrationEntry;
	oidRegistrationEntry.type = decltype( oidRegistrationEntry )::TREE;
	oidRegistrationEntry.getFunction = [ &oidMap ]( const snmp::NumericOid& oid, snmp::Variant& value ) -> bool {
		std::cout << "Request for OID '" << oid.str() << "'.\n";
		const auto& iterator = oidMap.find( oid );
		if( iterator == oidMap.end() ) {
			std::cout << "OID '" << oid.str() << "' was not found.\n";
			return false;
		}

		value = iterator->second;
		std::cout << "OID '" << oid.str() << "' was found; value is: " << value << ".\n";
		return true;
	};
	oidRegistrationEntry.getNextFunction = [ &oidMap ]( const snmp::NumericOid& oid, snmp::NumericOid& nextOid, snmp::Variant& value ) -> bool {
		std::cout << "Request for the OID after '" << oid.str() << "'.\n";
		const auto& iterator = oidMap.upper_bound( oid );
		if( iterator == oidMap.end() ) {
			std::cout << "The OID after '" << oid.str() << "' was not found.\n";
			return false;
		}

		nextOid = iterator->first;
		value = iterator->second;
		std::cout << "OID '" << nextOid.str() << "' was found; value is: " << value << ".\n";
		return true;
	};

	agentx.registerOid( "1.3.6.1.4.1.41326.3.1", oidRegistrationEntry );

	std::thread updateThread(
		[ &updateInterval, &oidMap ]() {
			while( true ) {
				auto startedTime = std::chrono::steady_clock::now();

				unsigned int processCount = 0;

				decltype( oidMap ) newOidMap;

				std::cout << "TEST AGENT: BEGIN READING PROCESS INFORMATION" << std::endl;

				DIR* directoryHandle = opendir( "/proc" );
				if( directoryHandle != NULL ) {
					struct dirent* directoryEntry = NULL;
					while( ( directoryEntry = readdir( directoryHandle ) ) ) {
						bool allNumbers = true;
						for( int i = 0, iSize = strlen(directoryEntry->d_name); i < iSize; i++ ) {
							if( directoryEntry->d_name[i] < '0' || directoryEntry->d_name[i] > '9' ) {
								allNumbers = false;
								break;
							}
						}
						if( ! allNumbers ) {
							continue;
						}

						std::string pidString( directoryEntry->d_name );
						std::string oidString;
						snmp::Variant value;

						{
							struct stat fileStats;
							std::string filename = std::string("/proc/") + pidString;
							int status = stat( filename.c_str(), &fileStats );
							if( status != 0 ) {
								std::cout << "Could not stat file: " << filename << std::endl;
							} else {
								oidString = std::string(".1.3.6.1.4.1.41326.3.1.1.1.8.") + pidString;
								value.set_uint32_t( snmp::Variant::APPLICATION_GAUGE32, (uint32_t)fileStats.st_mtime );
								newOidMap[ oidString ] = value;
							}
						}
						processCount++;

						std::string filename = std::string("/proc/") + std::string(directoryEntry->d_name) + std::string("/io");
						std::fstream fileStream;
						fileStream.open( filename, std::ios_base::in );
						if( fileStream.is_open() ) {
							std::string iokey;
							std::string iovalue;
							while( ! fileStream.eof() ) {
								fileStream >> iokey >> iovalue;

								// If, for whatever reason, our file ended early, then just stop now.
								if( fileStream.eof() ) {
									break;
								}
								//std::cout << "io file: read: " << iokey << " / " << iovalue << std::endl;

								oidString = "";
								if( iokey.compare("rchar:") == 0 ) {
									oidString = std::string(".1.3.6.1.4.1.41326.3.1.1.1.1.") + pidString;
									value.set_uint64_t( snmp::Variant::APPLICATION_COUNTER64, strtoull( iovalue.c_str(), NULL, 10 ) );
								} else if( iokey.compare("wchar:") == 0 ) {
									oidString = std::string(".1.3.6.1.4.1.41326.3.1.1.1.2.") + pidString;
									value.set_uint64_t( snmp::Variant::APPLICATION_COUNTER64, strtoull( iovalue.c_str(), NULL, 10 ) );
								} else if( iokey.compare("syscr:") == 0 ) {
									oidString = std::string(".1.3.6.1.4.1.41326.3.1.1.1.3.") + pidString;
									value.set_uint64_t( snmp::Variant::APPLICATION_COUNTER64, strtoull( iovalue.c_str(), NULL, 10 ) );
								} else if( iokey.compare("syscw:") == 0 ) {
									oidString = std::string(".1.3.6.1.4.1.41326.3.1.1.1.4.") + pidString;
									value.set_uint64_t( snmp::Variant::APPLICATION_COUNTER64, strtoull( iovalue.c_str(), NULL, 10 ) );
								} else if( iokey.compare("read_bytes:") == 0 ) {
									oidString = std::string(".1.3.6.1.4.1.41326.3.1.1.1.5.") + pidString;
									value.set_uint64_t( snmp::Variant::APPLICATION_COUNTER64, strtoull( iovalue.c_str(), NULL, 10 ) );
								} else if( iokey.compare("write_bytes:") == 0 ) {
									oidString = std::string(".1.3.6.1.4.1.41326.3.1.1.1.6.") + pidString;
									value.set_uint64_t( snmp::Variant::APPLICATION_COUNTER64, strtoull( iovalue.c_str(), NULL, 10 ) );
								} else if( iokey.compare("cancelled_write_bytes:") == 0 ) {
									oidString = std::string(".1.3.6.1.4.1.41326.3.1.1.1.7.") + pidString;
									value.set_uint64_t( snmp::Variant::APPLICATION_COUNTER64, strtoull( iovalue.c_str(), NULL, 10 ) );
								}
									
								if( oidString.length() > 0 ) {
									newOidMap[ oidString ] = value;
								}
							}
							fileStream.close();
						}
					}
					closedir( directoryHandle );
				}

				// TODO: LOCK THIS
				oidMap.swap( newOidMap );
				/* DEBUGGING:
				for( const auto& iterator : oidMap ) {
					std::cout << "   " << iterator.first.str() << " = " << iterator.second << "\n";
				}
				*/

				auto finishedTime = std::chrono::steady_clock::now();

				std::cout << "TEST AGENT: ALL DONE READING PROCESS INFORMATION FOR " << processCount << " PROCESSES." << std::endl;
				std::cout << "TEST AGENT: IT TOOK " << std::chrono::duration_cast<std::chrono::seconds>( finishedTime - startedTime ).count() << " SECONDS." << std::endl;
			
				std::this_thread::sleep_for( updateInterval );
			}
		}
	);

	// TODO: Find a nice way to stop this process.
	while( true ) {
		std::this_thread::sleep_for( std::chrono::seconds(30) );
	}

	return 0;
}

