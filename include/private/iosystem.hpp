#pragma once

#include <map>
#include <memory>
#include <mutex>
#include <thread>

#include "dangerous/snmp/exception.hpp"

#include "bytestream.hpp"


namespace dangerous { namespace snmp {

class IoSystem {
public:
	IoSystem() throw( Exception );
	~IoSystem();

	void registerInput( int handle, std::shared_ptr<ByteStream> buffer );
	void registerOutput( int handle, std::shared_ptr<ByteStream> buffer );
	
	void unregisterInput( int handle );
	void unregisterOutput( int handle );

	void main();

	/**
	 * This starts the the IoSystem as a thread.
	 **/
	void start();

	/**
	 * This stops the IoSystem thread, if it's running.
	 **/
	void stop();

	void wakeup();

protected:
	//! This is whether or not the thread should attempt to stop.
	//! If this is true, then the main loop should exit as soon
	//! as is convenient.
	bool isStopped;

	//! This is the thread for this instance.
	std::thread thisThread;

	std::mutex lock;
	std::map< int, std::shared_ptr<ByteStream> > inMap;
	std::map< int, std::shared_ptr<ByteStream> > outMap;

	int localSockets[ 2 ];
};

} }

