#pragma once

#include "dangerous/snmp/exception.hpp"
#include "dangerous/snmp/securitymodel.hpp"
#include "dangerous/snmp/types.hpp"

#include <memory>
#include <string>
#include <vector>


namespace dangerous { namespace snmp {

class ByteStream;


/**
 * A MessaqeProcessor is responsible for creating an envelope around
 * a particular payload and also removing such an envelope.  The payload
 * is "version"-agnostic; that is, all the MessageProcessor has to do
 * is encode the payload in some fashion; it will not modify that payload.
 *
 * A MessageProcessor subclass corresponds to a particular SNMP version.
 *    SNMPv1 -> MessageProcessorV1
 *    SNMPv2 -> MessageProcessorV2
 *    SNMPv3 -> MessageProcessorV3
 **/
class MessageProcessor {
public:
	enum Version {
		SNMPv1 = 0,
		SNMPv2 = 1,
		SNMPv3 = 3
	};

public:
	virtual ~MessageProcessor();

	/**
	 * This returns the SNMP version of the Message Processor.
	 * @return The enumeration value for the version.
	 **/
	Version version() const { return _version; }

	/**
	 * This encodes a new message.
	 * @param securityModel The security model to use.
	 * @param confirmedClass The appropriate ConfirmedClass type.
	 * @param pduBytes The PDU data to encode.
	 * @return A vector of the encoded bytes.
	 * @throw Exception
	 **/
	virtual std::vector<char> encode( const SecurityModel* securityModel, ConfirmedClass::Type confirmedClass, const std::vector<char>& pduBytes ) throw( Exception ) = 0;

	/**
	 * This decodes a message.
	 * @param securityModel The security model to use.
	 * @param byteStream The ByteStream to use to read the message.
	 * @param incomingSecurityModel The security model to populate with the message's information.
	 * @return A pointer to a ByteStream containing the PDU data to parse.
	 * @throw Exception
	 **/
	virtual std::unique_ptr<ByteStream> decode( const SecurityModel* securityModel, ByteStream& byteStream, std::unique_ptr<SecurityModel>& incomingSecurityModel ) throw( Exception ) = 0;

protected:
	Version _version;
};

} }

