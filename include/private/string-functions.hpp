#pragma once

#include <string>

/**
 * This returns a string containing the hexadecimal version
 * of the string given.
 * @param input The string to "convert" to hexadecimal.
 * @return A string representing the hexadecimal version of the string.  It will be twice as long.
 **/
static inline std::string hexadecimal( const std::string& input ) {
	static const char* const TRANSLATION = "0123456789ABCDEF";
	size_t length = input.length();

	std::string output;
	output.reserve( 2 * length );
	for( size_t i = 0; i < length; i++ ) {
		const unsigned char c = input[ i ];
		output.push_back( TRANSLATION[ c >> 4 ] );
		output.push_back( TRANSLATION[ c & 0x0F ] );
	}

	return output;
}

