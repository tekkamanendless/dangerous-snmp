#pragma once

#include <string>
#include <vector>

namespace dangerous { namespace snmp { namespace encoding {

class ScopedPDU {
public:
	std::string contextEngineID;
	std::string contextName;
	std::vector<char> pduBytes;
};

class SCOPEDPDU {
public:
	static constexpr const char* NAME = "ScopedPDU";
	static const int TYPE = asn1::helper::SEQUENCE::TYPE;
	typedef ScopedPDU value_type;

	static unsigned int length( const value_type& value ) {
		unsigned int size = 0;

		size += asn1::encodedSize<encoding::OCTET_STRING>( value.contextEngineID );
		size += asn1::encodedSize<encoding::OCTET_STRING>( value.contextName );
		// Then, we're just going to dump our PDU data in.
		size += value.pduBytes.size();

		return size;
	}

	static bool write( const value_type& value, char* buffer, unsigned int bufferSize ) {
		ByteStream byteStream;
		byteStream.linkFrom( buffer, bufferSize, ByteStream::READ_WRITE );
		
		BerStream berStream( &byteStream );

		bool success = true;

		success = berStream.write<encoding::OCTET_STRING>( value.contextEngineID );
		if( ! success ) {
			return false;
		}

		success = berStream.write<encoding::OCTET_STRING>( value.contextName );
		if( ! success ) {
			return false;
		}

		unsigned int bytesWritten = 0;
		success = berStream.writeBytes( value.pduBytes.data(), value.pduBytes.size(), bytesWritten );
		if( ! success ) {
			return false;
		}

		return true;
	}

	static bool read( value_type& value, char* buffer, unsigned int bufferSize ) {
		ByteStream byteStream;
		byteStream.linkFrom( buffer, bufferSize, ByteStream::READ_ONLY );
		
		BerStream berStream( &byteStream );

		bool success = true;

		success = berStream.read<encoding::OCTET_STRING>( value.contextEngineID );
		if( ! success ) {
			return false;
		}

		success = berStream.read<encoding::OCTET_STRING>( value.contextName );
		if( ! success ) {
			return false;
		}

		berStream.copyTo( value.pduBytes );
		
		return true;
	}

};

} } }

