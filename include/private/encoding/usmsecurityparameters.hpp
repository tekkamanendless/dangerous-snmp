#pragma once

namespace dangerous { namespace snmp { namespace encoding {

class UsmSecurityParameters {
public:
	// OCTET STRING
	std::string msgAuthoritativeEngineID;
	// INTEGER (0..2147483647)
	int32_t msgAuthoritativeEngineBoots;
	// INTEGER (0..2147483647)
	int32_t msgAuthoritativeEngineTime;
	// OCTET STRING (SIZE(0..32))
	std::string msgUserName;
	// OCTET STRING
	std::string msgAuthenticationParameters;
	// OCTET STRING
	std::string msgPrivacyParameters;
};

/**
 * This is the encoder/decoder for the "UsmSecurityParameters" class.
 **/
class USMSECURITYPARAMETERS {
public:
	static constexpr const char* NAME = "UsmSecurityParameters";
	static const int TYPE = asn1::helper::SEQUENCE::TYPE;
	typedef UsmSecurityParameters value_type;

	static unsigned int length( const value_type& parameters ) {
		unsigned int size = 0;

		size += asn1::encodedSize<encoding::OCTET_STRING>( parameters.msgAuthoritativeEngineID );
		size += asn1::encodedSize<encoding::INTEGER>( parameters.msgAuthoritativeEngineBoots );
		size += asn1::encodedSize<encoding::INTEGER>( parameters.msgAuthoritativeEngineTime );
		size += asn1::encodedSize<encoding::OCTET_STRING>( parameters.msgUserName );
		size += asn1::encodedSize<encoding::OCTET_STRING>( parameters.msgAuthenticationParameters );
		size += asn1::encodedSize<encoding::OCTET_STRING>( parameters.msgPrivacyParameters );

		return size;
	}

	static bool write( const value_type& parameters, char* buffer, unsigned int bufferSize ) {
		ByteStream byteStream;
		byteStream.linkFrom( buffer, bufferSize, ByteStream::READ_WRITE );
		
		BerStream berStream( &byteStream );

		bool success = true;

		success = berStream.write<encoding::OCTET_STRING>( parameters.msgAuthoritativeEngineID );
		if( ! success ) {
			return false;
		}

		success = berStream.write<encoding::INTEGER>( parameters.msgAuthoritativeEngineBoots );
		if( ! success ) {
			return false;
		}

		success = berStream.write<encoding::INTEGER>( parameters.msgAuthoritativeEngineTime );
		if( ! success ) {
			return false;
		}

		success = berStream.write<encoding::OCTET_STRING>( parameters.msgUserName );
		if( ! success ) {
			return false;
		}

		success = berStream.write<encoding::OCTET_STRING>( parameters.msgAuthenticationParameters );
		if( ! success ) {
			return false;
		}

		success = berStream.write<encoding::OCTET_STRING>( parameters.msgPrivacyParameters );
		if( ! success ) {
			return false;
		}

		return true;
	}

	static bool read( value_type& parameters, char* buffer, unsigned int bufferSize ) {
		ByteStream byteStream;
		byteStream.linkFrom( buffer, bufferSize, ByteStream::READ_ONLY );
		
		BerStream berStream( &byteStream );

		bool success = true;

		success = berStream.read<encoding::OCTET_STRING>( parameters.msgAuthoritativeEngineID );
		if( ! success ) {
			return false;
		}

		success = berStream.read<encoding::INTEGER>( parameters.msgAuthoritativeEngineBoots );
		if( ! success ) {
			return false;
		}

		success = berStream.read<encoding::INTEGER>( parameters.msgAuthoritativeEngineTime );
		if( ! success ) {
			return false;
		}

		success = berStream.read<encoding::OCTET_STRING>( parameters.msgUserName );
		if( ! success ) {
			return false;
		}

		success = berStream.read<encoding::OCTET_STRING>( parameters.msgAuthenticationParameters );
		if( ! success ) {
			return false;
		}

		success = berStream.read<encoding::OCTET_STRING>( parameters.msgPrivacyParameters );
		if( ! success ) {
			return false;
		}

		return true;
	}
};

} } }

