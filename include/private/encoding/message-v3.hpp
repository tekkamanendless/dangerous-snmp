#pragma once

#include <string>
#include <vector>

namespace dangerous { namespace snmp { namespace encoding {

/**
 * This class is a container for the message flags.
 **/
class SNMPv3MessageFlags {
public:
	//! This is set when authentication is enabled.
	static constexpr const char AUTHENTICATION = (char)0b00000001;
	//! This is set when encryption is enabled.
	//! Note that the standard requires authentication when encryption
	//! is present.
	static constexpr const char ENCRYPTION = (char)0b00000010;
	//! This is set when a response is requested.
	//! Note that its use is dictated by the standard.
	static constexpr const char REPORTABLE = (char)0b00000100;
};

/**
 * This class is the version-3 encapsulation for a message.
 * The actual payload of the message is not processed here.
 **/
class SNMPv3Message {
public:
	//! This is the version number.
	//!    3: Version 3
	int msgVersion;

	class HeaderData {
	public:
		int msgID;
		int msgMaxSize;
		std::string msgFlags;
		//! TODO
		//! This will determine the meaning of "msgSecurityParameters" below.
		int msgSecurityModel;
	} msgHeaderData;

	//! This is the BER-encoded security information.  Since there are
	//! many possible security models (in practice, we only use USM),
	//! this is flexible enough to allow for just about anything.
	std::string msgSecurityParameters;

	//! This is the actual payload of the message.
	std::vector<char> pduBytes;
};

/**
 * This is the encoder/decoder for the "SNMPv3Message.HeaderData" class.
 **/
class SNMPV3MESSAGE_HEADERDATA {
public:
	static constexpr const char* NAME = "SNMPv3Message.HeaderData";
	static const int TYPE = asn1::helper::SEQUENCE::TYPE;
	typedef typename SNMPv3Message::HeaderData value_type;

	static unsigned int length( const value_type& headerData ) {
		unsigned int size = 0;
		
		size += asn1::encodedSize<encoding::INTEGER>( headerData.msgID );
		
		size += asn1::encodedSize<encoding::INTEGER>( headerData.msgMaxSize );

		size += asn1::encodedSize<encoding::OCTET_STRING>( headerData.msgFlags );
		
		size += asn1::encodedSize<encoding::INTEGER>( headerData.msgSecurityModel );
		
		return size;
	}

	static bool write( const value_type& headerData, char* buffer, unsigned int bufferSize ) {
		ByteStream byteStream;
		byteStream.linkFrom( buffer, bufferSize, ByteStream::READ_WRITE );

		BerStream berStream( &byteStream );

		bool success = true;

		success = berStream.write<encoding::INTEGER>( headerData.msgID );
		if( ! success ) {
			return false;
		}

		success = berStream.write<encoding::INTEGER>( headerData.msgMaxSize );
		if( ! success ) {
			return false;
		}

		success = berStream.write<encoding::OCTET_STRING>( headerData.msgFlags );
		if( ! success ) {
			return false;
		}

		success = berStream.write<encoding::INTEGER>( headerData.msgSecurityModel );
		if( ! success ) {
			return false;
		}

		return true;
	}

	static bool read( value_type& headerData, char* buffer, unsigned int bufferSize ) {
		ByteStream byteStream;
		byteStream.linkFrom( buffer, bufferSize, ByteStream::READ_ONLY );
		
		BerStream berStream( &byteStream );

		bool success = true;

		success = berStream.read<encoding::INTEGER>( headerData.msgID );
		if( ! success ) {
			return false;
		}

		success = berStream.read<encoding::INTEGER>( headerData.msgMaxSize );
		if( ! success ) {
			return false;
		}

		success = berStream.read<encoding::OCTET_STRING>( headerData.msgFlags );
		if( ! success ) {
			return false;
		}

		success = berStream.read<encoding::INTEGER>( headerData.msgSecurityModel );
		if( ! success ) {
			return false;
		}

		return true;
	}
};

/**
 * This is the encoder/decoder for the "SNMPv3Message" class.
 **/
class SNMPV3MESSAGE {
public:
	static constexpr const char* NAME = "SNMPv3Message";
	static const int TYPE = asn1::helper::SEQUENCE::TYPE;
	typedef SNMPv3Message value_type;

	static unsigned int length( const value_type& message ) {
		unsigned int size = 0;
		// The header will require any space necessary to store the version.
		size += asn1::encodedSize<encoding::INTEGER>( message.msgVersion );

		size += asn1::encodedSize<SNMPV3MESSAGE_HEADERDATA>( message.msgHeaderData );

		// The header will require any space necessary to store the community string.
		size += asn1::encodedSize<encoding::OCTET_STRING>( message.msgSecurityParameters );
		
		// Then, we're just going to dump our PDU data in.
		size += message.pduBytes.size();

		return size;
	}

	static bool write( const value_type& message, char* buffer, unsigned int bufferSize ) {
		ByteStream byteStream;
		byteStream.linkFrom( buffer, bufferSize, ByteStream::READ_WRITE );
		
		BerStream berStream( &byteStream );

		bool success = true;

		success = berStream.write<encoding::INTEGER>( message.msgVersion );
		if( ! success ) {
			return false;
		}

		success = berStream.write<SNMPV3MESSAGE_HEADERDATA>( message.msgHeaderData );
		if( ! success ) {
			return false;
		}

		success = berStream.write<encoding::OCTET_STRING>( message.msgSecurityParameters );
		if( ! success ) {
			return false;
		}

		unsigned int bytesWritten = 0;
		success = berStream.writeBytes( message.pduBytes.data(), message.pduBytes.size(), bytesWritten );
		if( ! success ) {
			return false;
		}

		return true;
	}

	static bool read( value_type& message, char* buffer, unsigned int bufferSize ) {
		ByteStream byteStream;
		byteStream.linkFrom( buffer, bufferSize, ByteStream::READ_ONLY );
		
		BerStream berStream( &byteStream );

		bool success = true;

		success = berStream.read<encoding::INTEGER>( message.msgVersion );
		if( ! success ) {
			return false;
		}

		success = berStream.read<SNMPV3MESSAGE_HEADERDATA>( message.msgHeaderData );
		if( ! success ) {
			return false;
		}

		success = berStream.read<encoding::OCTET_STRING>( message.msgSecurityParameters );
		if( ! success ) {
			return false;
		}

		berStream.copyTo( message.pduBytes );
		
		return true;
	}
};

} } }

