#pragma once

#include "berstream.hpp"

#include <string>
#include <vector>

namespace dangerous { namespace snmp { namespace encoding {

/**
 * This class is the version-1 and version-2c encapsulation
 * for a message.  The actual payload of the message is
 * not processed here.
 **/
class Message {
public:
	//! This is the version number.
	//!    0: Version 1
	//!    1: Version 2c
	int version;
	
	//! This is the "community string" for the message.
	std::string communityString;

	//! This is the actual payload of the message.
	std::vector<char> pduBytes;
};

/**
 * This is the encoder/decoder for the "Message" class.
 **/
class MESSAGE {
public:
	static constexpr const char* NAME = "Message";
	static const int TYPE = asn1::helper::SEQUENCE::TYPE;
	typedef Message value_type;

	static unsigned int length( const value_type& message ) {
		unsigned int size = 0;
		// The header will require any space necessary to store the version.
		size += asn1::encodedSize<encoding::INTEGER>( message.version );
		// The header will require any space necessary to store the community string.
		size += asn1::encodedSize<encoding::OCTET_STRING>( message.communityString );
		// Then, we're just going to dump our PDU data in.
		size += message.pduBytes.size();

		return size;
	}

	static bool write( const value_type& message, char* buffer, unsigned int bufferSize ) {
		ByteStream byteStream;
		byteStream.linkFrom( buffer, bufferSize, ByteStream::READ_WRITE );

		BerStream berStream( &byteStream );

		bool success = true;

		success = berStream.write<encoding::INTEGER>( message.version );
		if( ! success ) {
			return false;
		}
		success = berStream.write<encoding::OCTET_STRING>( message.communityString );
		if( ! success ) {
			return false;
		}
		unsigned int bytesWritten = 0;
		success = berStream.writeBytes( message.pduBytes.data(), message.pduBytes.size(), bytesWritten );
		if( ! success ) {
			return false;
		}

		return true;
	}

	static bool read( value_type& message, char* buffer, unsigned int bufferSize ) {
		ByteStream byteStream;
		byteStream.linkFrom( buffer, bufferSize, ByteStream::READ_ONLY );
		
		BerStream berStream( &byteStream );

		bool success = true;

		success = berStream.read<encoding::INTEGER>( message.version );
		if( ! success ) {
			return false;
		}
		success = berStream.read<encoding::OCTET_STRING>( message.communityString );
		if( ! success ) {
			return false;
		}

		berStream.copyTo( message.pduBytes );

		return true;
	}
};

} } }

