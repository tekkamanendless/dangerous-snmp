#pragma once

#include "transport.hpp"


namespace dangerous { namespace snmp { namespace transport {

/**
 * This is the base class of both the UDP and TCP transports.
 * Because the Berkeley socket API is implemented in such a way
 * as to make any transports that use it almost identical, this
 * class serves to handle all of major work, with the subclasses
 * handling any specifics (such as the protocol number or "socket"
 * options).
 **/
class BasicSocketTransport : public Transport {
public:
	BasicSocketTransport( std::weak_ptr<Context::PrivateData> context );
	virtual ~BasicSocketTransport();

	/**
	 * This returns whether or not the transport is connected.
	 * @return true if it is connected; false if it is not.
	 **/
	virtual bool isConnected();

	/**
	 * This instructs the transport to connect to its endpoint.
	 * This may or may not succeed; to check for success, call
	 * "isConnected" afterward.
	 **/
	virtual void connect();

	/**
	 * This shuts down the transport (if it was already connected).
	 * If the transport was not connected, then this does nothing.
	 **/
	virtual void disconnect();

public:
	bool send( const std::vector<char>& bytes );
	std::vector<char> receive( const std::chrono::milliseconds& timeout );

protected:
	/**
	 * When creating a socket, this is called to set the options that
	 * should be passed to the call.  It is the responsibility of the
	 * subclass to properly fill this out.
	 * @return The relevant value for addrinfo.ai_socktype.
	 **/
	virtual int socketOptions() const;
	
	/**
	 * When creating a socket, this is called to set the protocol that
	 * should be passed to the call.  It is the responsibility of the
	 * subclass to properly fill this out.
	 * @return The relevant value for addrinfo.ai_protocol.
	 **/
	virtual int protocolNumber() const;

protected:
	//! This is the socket handle.
	int socketHandle;

	//! This is the address to which to connect.
	//! This may be any string, including an IPv4 address, IPv6 address,
	//! a DNS name, etc.
	std::string address;
	//! This is the port on which to connect.
	//! This may be any string, including a port number as well as the named
	//! "service" (if the system can translate it).
	std::string port;
};

} } }

