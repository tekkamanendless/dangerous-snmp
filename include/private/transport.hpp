#pragma once

#include "dangerous/snmp/context.hpp"

#include <chrono>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

namespace dangerous { namespace snmp {

class ByteStream;


/**
 * The Transport class represents an abstraction of the transport mechanism
 * behind SNMP.  This class is abstract and cannot be used.  Any actual
 * transport must subclass this and implement the required methods.
 *
 * In this way, we will not bind SNMP to any one particular transport, allowing
 * it to be flexible and extensible.
 **/
class Transport {
protected:
	Transport( std::weak_ptr<Context::PrivateData> context );
	
public:
	virtual ~Transport();

	unsigned int maximumIncomingMessageSize() { return _maximumIncomingMessageSize; }
	unsigned int maximumOutgoingMessageSize() { return _maximumOutgoingMessageSize; }

	/**
	 * This returns whether or not the transport is connected.
	 * @return true if it is connected; false if it is not.
	 **/
	virtual bool isConnected();

	/**
	 * This instructs the transport to connect to its endpoint.
	 * This may or may not succeed; to check for success, call
	 * "isConnected" afterward.
	 **/
	virtual void connect();

	/**
	 * This shuts down the transport (if it was already connected).
	 * If the transport was not connected, then this does nothing.
	 **/
	virtual void disconnect();

	std::shared_ptr<ByteStream> readerStream;

	std::shared_ptr<ByteStream> writerStream;
	void commitWrite();

protected:
	unsigned int _maximumIncomingMessageSize;
	unsigned int _maximumOutgoingMessageSize;

	std::weak_ptr<Context::PrivateData> context;

	void registerReadWriteStreams( int readHandle, int writeHandle );
	void unregisterReadWriteStreams( int readHandle, int writeHandle );
};

} }

