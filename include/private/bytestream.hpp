#pragma once

#include <cstdint>
#include <condition_variable>
#include <list>
#include <memory>
#include <mutex>
#include <string>
#include <vector>

namespace dangerous { namespace snmp {

/**
 * A ByteStream is an object that is basically a big wrapper around a character
 * buffer.  There is a "read head" and a "write head", and when the read head
 * reaches the write head, there is no more left to read.
 *
 * A ByteStream also has the concept of "live".  A live ByteStream is assumed to
 * be hooked up to some mechanism that is writing to it while another thread is
 * reading from it.  In a live stream, the read operation can block until more
 * information is written (or a timeout occurs).
 *
 * A ByteStream also has the notion of "packet".  A packet is a discrete chunk of data
 * that has a beginning and an end.  In the event of a read error, the ByteStream can
 * be advanced to the _next_ packet.
 *
 * Exceptions:
 *    * None; A ByteStream throws no exceptions.
 **/
class ByteStream {
public:
	/**
	 * This defines the kind of access that a ByteStream will have.  In particular,
	 * this refers to whether or not the "write" calls work.
	 **/
	enum Access {
		//! Only read operations may be performed.
		READ_ONLY,
		//! Both read and write operations may be performed.
		READ_WRITE
	};

public:
	/**
	 * Default constructor; this creates a new, empty ByteStream.
	 **/
	ByteStream();

	/**
	 * Destructor.
	 **/
	~ByteStream();

	/**
	 * This copies the bytes from the given string into the ByteStream's character buffer.
	 * @param bytes The string to copy.
	 * @param access The kind of access that the ByteStream should have.
	 **/
	void copyFrom( const std::string& bytes, Access access );
	
	/**
	 * This copies the bytes from the given vector into the ByteStream's character buffer.
	 * @param bytes The vector to copy.
	 * @param access The kind of access that the ByteStream should have.
	 **/
	void copyFrom( const std::vector<char>& bytes, Access access );
	
	/**
	 * This copies the bytes from the given ByteStream into the ByteStream's character buffer.
	 * Note that this copies the _remaining_ bytes in the ByteStream.
	 * @param bytes The ByteStream to copy.
	 * @param access The kind of access that the ByteStream should have.
	 **/
	void copyFrom( const ByteStream& byteStream, Access access );

	/**
	 * This links the bytes given to the ByteStream's character buffer.
	 * This is essentially creating a simple ByteStream wrapper around the buffer that already exists.
	 * You _must not_ free "bytes" until this ByteStream is destroyed (or is no longer used).
	 * @param bytes The character array to use.
	 * @param bytesSize The length of the character array to use.
	 * @param access The kind of access that the ByteStream should have.
	 **/
	void linkFrom( char* bytes, unsigned int bytesSize, Access access );

	/**
	 * This copies the portion of the ByteStream that's currently available for reading
	 * to the specified target.
	 * @param buffer The vector to replace.
	 **/
	void copyTo( std::vector<char>& buffer ) const;

	/**
	 * This copies the portion of the ByteStream that's currently available for reading
	 * to the specified target.
	 * @param buffer The string to replace.
	 **/
	void copyTo( std::string& buffer ) const;

	/**
	 * This compares the ByteStream to the given character buffer and returns the equivalent
	 * of "memcmp".
	 * @param buffer The character buffer to compare it with.
	 * @param bufferLength The length of the character buffer.
	 * @return -1 if the ByteStream is smaller, 0 if it is equal, or +1 if it is greater than the buffer.
	 **/
	int compareWith( const char* buffer, unsigned int bufferLength ) const;

	/**
	 * This returns whether or not the stream is in an errored state.
	 * @return true if the stream is in an errored state; false otherwise.
	 **/
	inline const bool isErrored() const { return _isErrored; }

	/**
	 * If the last "read*" function failed, then this will return whether
	 * or not the cause was a timeout.
	 * @return true if the last "read*" function failed due a timeout; false otherwise.
	 **/
	inline const bool lastReadTimedOut() const { return _lastReadTimedOut; }

	/**
	 * This returns whether or not the stream is connected to "live" data, such
	 * as a socket.  A live stream has the ability to wait (and timeout) on "read*"
	 * operations if there is not enough data.
	 * @return true if the stream is live; false otherwise.
	 **/
	inline const bool isLive() const { return _isLive; }

	/**
	 * This sets the "live" state of the stream.
	 * @param isLive The new live state of the stream.
	 **/
	void setLive( bool isLive );

	/**
	 * This returns the current read timeout for the stream.
	 * This is only useful if the stream is live.
	 * @return The timeout, in milliseconds.
	 **/
	inline std::chrono::milliseconds readTimeout() const { return _readTimeout; }

	/**
	 * This sets the read timeout for the stream.
	 * This is only useful if the stream is live.
	 * @param timeout The new timeout, in milliseconds.
	 **/
	inline void readTimeout( const std::chrono::milliseconds& timeout ) { _readTimeout = timeout; }

	/**
	 * This returns the number of bytes remaining in the stream.
	 * @return The number of bytes remaining.
	 **/
	unsigned int remainingReadLength() const;
	
	/**
	 * This returns the number of bytes remaining in the stream.
	 * @return The number of bytes remaining.
	 **/
	unsigned int remainingWriteLength() const;

	/**
	 * This resets the internal character buffer (if owned by the ByteStream).
	 * This will _not_ free any memory, but it will copy the current data in
	 * the ByteStream to the _beginning_ of the buffer; this way, it will not
	 * continue to grow unchecked indefinitely.
	 **/
	void reset();

	/*
	 * Peek functions.
	 */

	/**
	 * This reads a byte from the ByteStream.
	 * However, this does _not_ advance the read head of the ByteStream.
	 * @param byte A reference to the byte to read.
	 * @return true on success, false on failure.
	 **/
	bool peekByte( char& byte );

	/*
	 * Read functions.
	 */

	/**
	 * This reads a byte from the ByteStream.
	 * @param byte A reference to the character to store the byte.
	 * @return true on success, false on failure.
	 **/
	bool readByte( char& byte );

	/**
	 * This reads an arbitrary amount of bytes into the specified character buffer.
	 * @param buffer The character buffer in which to store the read bytes.
	 * @param bufferSize The number of bytes to read.  "buffer" must be _at least_ this size.
	 * @param bytesRead A reference to an integer to hold the number of bytes that were actually read.
	 * @return TODO
	 **/
	bool readBytes( char* buffer, unsigned int bufferSize, unsigned int& bytesRead );

	/*
	 * Write functions.
	 * All of these functions will advance the write head.
	 */

	/**
	 * This writes a byte to the ByteStream.
	 * @param byte The byte to write.
	 * @return true on success, false on failure.
	 **/
	bool writeByte( char byte );

	/**
	 * This writes the bytes given to the ByteStream.
	 * @param buffer The bytes to write.
	 * @param bufferSize The number of bytes to write.  "buffer" must be _at least_ this size.
	 * @param bytesWritten A reference to an integer to store the number of bytes actually written.
	 * @return true on success, false on failure.
	 **/
	bool writeBytes( const char* buffer, unsigned int bufferSize, unsigned int& bytesWritten );

	/*
	 * Packet functions.
	 */

	/**
	 * This ends the current packet and starts a new one.
	 **/
	void endPacket();

	/**
	 * This returns whether or not there is a next packet.
	 * @return true if there is a next packet, false if there is not.
	 **/
	bool hasNextPacket();

	/**
	 * This advances the ByteStream to the next packet, if there is one.
	 * @return true if the ByteStream actually advanced to a new packet, false if it did not.
	 **/
	bool nextPacket();

	/*
	 * Debugging functions.
	 */

	/**
	 * This returns a string that can be printed in order to see debugging information about
	 * the ByteStream, including the position of the read and write heads, as well as a hexadecimal
	 * printout of the stream (wrapped every 50 bytes).
	 * @return A string with debugging information.
	 **/
	std::string debugString();

protected:
	//! This is whether or not this ByteStream is in an errored state.
	//! A stream in this state is useless and should be discarded.
	bool _isErrored;

	//! If this is a live stream, then this will be set to true if the last
	//! "read*" function failed due to a timeout.
	bool _lastReadTimedOut;

	//! This is whether or not this ByteStream is hooked up (in some way)
	//! to live data.  A streaming ByteStream will _wait_ on "read" calls
	//! if there is not enough data to complete them.  A non-live one
	//! will not.
	bool _isLive;
	//! This is the mutex for handling streaming operations.
	mutable std::mutex liveLock;
	//! TODO: DOCUMENT THIS VARIABLE.
	mutable std::condition_variable liveConditionVariable;

	//! This is whether or not this ByteStream owns the memory pointed
	//! to by "bytes".  If so, then it must clean it up when done.
	bool ownBytes;
	//! This is the buffer allocated for this ByteStream.
	char* bytes;
	//! This is the current size of "bytes".
	unsigned int bytesSize;
	//! This is the current position for reading.
	unsigned int readIndex;
	//! This is the current position for writing.
	unsigned int writeIndex;

	//! This is the list of the boundaries (if applicable) for this stream.
	//! For example, TCP reads do not preserve message boundaries, but UDP
	//! reads do.  When applicable, these boundaries (1) can be denoted here,
	//! and (2) will be used as a higher-priority end-of-buffer.
	std::list<unsigned int> boundaries;

	//! This is the timeout for reads.
	std::chrono::milliseconds _readTimeout;

	/**
	 * This frees the memory associated with the internal character buffer.
	 * Note, however, that this has _no effect_ if the ByteStream does _not_
	 * own its own buffer.
	 *
	 * If its bytes are simply linked, then this function does nothing.
	 **/
	void freeBytes();

	/**
	 * This returns the number of bytes remaining in the stream.
	 * @return The number of bytes remaining.
	 **/
	unsigned int remainingReadLength_nolock() const;
	
	/**
	 * This returns the number of bytes remaining in the stream.
	 * @return The number of bytes remaining.
	 **/
	unsigned int remainingWriteLength_nolock() const;

	/**
	 * This attempts to wait for the given number of bytes to become available
	 * for reading.  If they all could not become available, then this returns
	 * false.
	 * @return true if "increment" bytes may now be read; false otherwise.
	 **/
	bool waitForReading( unsigned int increment );
	
	/**
	 * This attempts to increase the size of the buffer by the specified
	 * amount.  Note, however, that if this ByteStream doesn't OWN its
	 * buffer, then this call will fail.
	 * @return true if "increment" bytes may now be written; false otherwise.
	 **/
	bool grow( unsigned int increment );
	
};

} }

