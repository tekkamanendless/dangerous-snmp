#pragma once

#include "../securitymodel.hpp"
#include "../types.hpp"

#include <string>
#include <chrono>

namespace dangerous { namespace snmp { namespace securitymodel {

/**
 * The User-based Security Model (USM) associates a user (by "user name")
 * with security information.  This includes the concepts of "authentication"
 * (making sure that the message was not tampered with) and "privacy" (making
 * sure that the message could not be read by a third party; that is, encryption).
 *
 * For USM, a user name is required.  Authentication is optional, as is
 * encryption (but note that encryption requires authentication).
 *
 * From RFC-2574, section 1.5:
 *     Each SNMP engine maintains two values, snmpEngineBoots and
 *     snmpEngineTime, which taken together provide an indication of time at
 *     that SNMP engine.  Both of these values are included in an
 *     authenticated message sent to/received from that SNMP engine.  On
 *     receipt, the values are checked to ensure that the indicated
 *     timeliness value is within a Time Window of the current time.  The
 *     Time Window represents an administrative upper bound on acceptable
 *     delivery delay for protocol messages.
 *
 *     For an SNMP engine to generate a message which an authoritative SNMP
 *     engine will accept as authentic, and to verify that a message
 *     received from that authoritative SNMP engine is authentic, such an
 *     SNMP engine must first achieve timeliness synchronization with the
 *     authoritative SNMP engine.
 **/
class USM : public SecurityModel {
public:
	/**
	 * Default constructor; this is essentially "noAuthNoPriv".
	 * There is no authentication whatsoever, nor is there any privacy.
	 **/
	USM();

	/**
	 * This constructs a USM instance with the specified parameters.
	 * Note that if you specify an encryption type but no authentication type, then
	 * this will throw an Exception.
	 * @param username The username.
	 * @param authentication The kind of authentication to use.
	 * @param authenticationPassword The authentication secret that will be used, along with the agent engine ID, to generate an authentication key.
	 * @param encryption The kind of encryption to use.
	 * @param encryptionPassword The encryption secret that will be used, along with the agent engine ID, to generate an encryption key.
	 * @throw Exception
	 **/
	USM( const std::string& username, Authentication::Type authentication, const std::string& authenticationPassword, Encryption::Type encryption, const std::string& encryptionPassword ) throw( Exception );
	
	/**
	 * Copy constructor.
	 * @param usm The USM instance to copy.
	 **/
	USM( const USM& usm );

public:
	/**
	 * This returns the authoritative engine ID.
	 * @return The authoritative engine ID.
	 **/
	std::string authoritativeEngineID() const { return _authoritativeEngineID; }

	/**
	 * This sets the authoritative engine ID.
	 * In addition, if authentication is configured, this will update the
	 * authentication and encryption keys.
	 * @param authoritativeEngineID The desired authoritative engine ID.
	 **/
	void authoritativeEngineID( const std::string& authoritativeEngineID );

	/**
	 * This returns the number of times that the engine has booted.
	 *
	 * @return The number of times that the engine has booted.
	 **/
	uint32_t authoritativeEngineBoots() const { return _authoritativeEngineBoots; }

	/**
	 * This updates the number of times that the engine has booted.
	 *
	 * @param authoritativeEngineBoots The new value for the number of times that the engine has booted.
	 **/
	void authoritativeEngineBoots( uint32_t authoritativeEngineBoots ) { _authoritativeEngineBoots = authoritativeEngineBoots; }
	
	/**
	 * This returns the amount of time since the last boot of the engine,
	 * measured in seconds.
	 *
	 * @return The amount of time since the last boot of the engine, in seconds.
	 **/
	std::chrono::seconds authoritativeEngineTime() const { return _authoritativeEngineTime; }

	/**
	 * This updates the number of time since the last boot of the engine.
	 *
	 * Under normal operations, this is updated via updateTimeFromClock(), which
	 * uses the system clock to appropriately increment the value.  However,
	 * this method may be used to set the value exactly.
	 *
	 * @param authoritativeEngineTime The new value for the amount of time since the last boot of the engine.
	 **/
	void authoritativeEngineTime( std::chrono::seconds authoritativeEngineTime ) {
		_authoritativeEngineTime = authoritativeEngineTime;
		lastUpdateTime = std::chrono::system_clock::now();
	}

	bool isAuthenticated() const;

public:
	/**
	 * This will update the value of "authoritativeEngineTime" based on the amount
	 * of wall time elapsed since the last update.  This is useful because USM
	 * requires that a client have a belief about the agent's engine time value.
	 **/
	void updateTimeFromClock() {
		//! This is the number of seconds that have elapsed since the last time that
		//! we updated the engine time value.
		std::chrono::seconds changeInTime = std::chrono::duration_cast<std::chrono::seconds>( std::chrono::system_clock::now() - lastUpdateTime );
		// TODO: Check for negative result?

		// Update the engine time.
		authoritativeEngineTime( _authoritativeEngineTime + changeInTime );
	}

public:
	/**
	 * This returns the user name.
	 * @return The user name.
	 **/
	std::string username() const { return _username; };
	
	/**
	 * This returns the type of authentication used.
	 * @return The type of authentication.
	 **/
	Authentication::Type authentication() const { return _authentication; };
	
	/**
	 * This returns the authentication password.  This is strictly
	 * used to generate the authentication "key".
	 * @return The authentication password.
	 **/
	std::string authenticationPassword() const { return _authenticationPassword; };
	
	/**
	 * This returns the authentication key.
	 * @return The authentication key.
	 **/
	std::string authenticationKey() const { return _authenticationKey; };
	
	/**
	 * This returns the type of encryption used.
	 * @return The type of encryption.
	 **/
	Encryption::Type encryption() const { return _encryption; };
	
	/**
	 * This returns the encryption password.  This is strictly
	 * used to generate the encryption "key".
	 * @return The encryption password.
	 **/
	std::string encryptionPassword() const { return _encryptionPassword; }
	
	/**
	 * This returns the encryption key.
	 * @return The encryption key.
	 **/
	std::string encryptionKey() const { return _encryptionKey; }

protected:
	/*
	 * These parameters are generally given as input; they must
	 * be known in advance.
	 */

	//! The user name.
	//! OCTET STRING (SIZE(0..32))
	std::string _username;
	
	//! The type of authentication used.
	Authentication::Type _authentication;
	
	//! If authentication is to be used, then the user will have a secret
	//! authentication password.  This must be known to both the user and
	//! the agent in advance.  This password will be used in some capacity
	//! (for example, in conjunction with creating an MD5 or SHA checksum)
	//! such that the agent can know if the message was tampered with along
	//! the way.
	std::string _authenticationPassword;

	//! This is the actual authentication "key" to be used.  While the
	//! "password" might seem like the real thing, it is only truly used
	//! to generate a "key" of a known byte length (depending on the type
	//! of authentication used).
	//!    HMAC-MD5-96: 16 bytes.
	//!    HMAC-SHA-96: 20 bytes.
	std::string _authenticationKey;

	//! The type of encryption used.
	//! Note that if encryption is used, then authentication must be used
	//! as well.
	Encryption::Type _encryption;
	
	//! If encryption is to be used, then the user will have a secret
	//! encryption password.  This must be known to both the user and
	//! the agent in advance.  This password will be used, along with some
	//! semi-random "salt", to encrypt the data.  The salt will be
	//! included with the message.
	std::string _encryptionPassword;

	//! This is the actual encryption "key" to be used.  While the
	//! "password" might seem like the real thing, it is only truly used
	//! to generate a "key" of a known byte length (depending on the type
	//! of encryption used).
	//!   CBC-DES: 16 bytes.
	//!   CFB128-AES-128: 16 bytes.
	std::string _encryptionKey;

protected:
	/*
	 * These parameters are "discoverable"; they pertain to the target
	 * and do not need to be known in advance.  However, they can be.
	 */

	//! The authoritative engine ID
	//! OCTET STRING
	std::string _authoritativeEngineID;

	//! The number of times that the engine has booted.
	//! 
	//! Syntax: INTEGER (0..2147483647)
	uint32_t _authoritativeEngineBoots;

	//! The amount of time since the last boot of the engine,
	//! measured in seconds.
	//! 
	//! Syntax: INTEGER (0..2147483647)
	std::chrono::seconds _authoritativeEngineTime;

protected:
	/*
	 * This is for internal use only.
	 */

	//! This is the last time that authoritative engine information
	//! was updated.
	std::chrono::system_clock::time_point lastUpdateTime;
};

} } }

