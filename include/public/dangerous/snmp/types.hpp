#pragma once

#include "numericoid.hpp"
#include "variant.hpp"

#include <cstdint>
#include <list>
#include <memory>
#include <vector>


namespace dangerous { namespace snmp {

/**
 * This is a container class for the type of authentication used
 * in SNMPv3.
 **/
class Authentication {
public:
	/**
	 * This defines the type of authentication.
	 * TODO: WHICH RFC?
	 **/
	enum Type {
		//! No authentication will be used.
		NONE,
		//! HMAC-MD5-96 authentication.
		MD5,
		//! HMAC-SHA-96 authentication.
		SHA
	};
};

/**
 * This is a container class for the type of encryption used
 * in SNMPv3.
 **/
class Encryption {
public:
	/**
	 * This defines the type of encryption.
	 * TODO: WHICH RFC?
	 **/
	enum Type {
		//! No encryption will be used.
		NONE,
		//! CBC-DES encryption.
		DES,
		//! CFB128-AES-128 encryption.
		AES
	};
};

/**
 * This class is a wrapper around an enumeration for the "confirmed class"
 * of a PDU.  A PDU that is confirmed is one where the agent is expected to
 * send a response.  An unconfirmed PDU is not acknowledged by the agent.
 **/
class ConfirmedClass {
public:
	/**
	 * A Type represents one of the two defined values for the
	 * "confirmed class" of a PDU.
	 **/
	enum Type {
		//! No response is expected.
		Unconfirmed,
		//! A response is expected.
		Confirmed
	};
};

/**
 * This defines a variable binding, or "varbind".  This maps an OID
 * to a particular value.
 **/
class VarBind {
public:
	/**
	 * Default constructor; both the OID and the Variant are null values.
	 **/
	VarBind() {}

	/**
	 * Copy constructor; this copies the contents of another VarBind.
	 **/
	VarBind( const VarBind& varbind ) : oid( varbind.oid ), value( varbind.value ) {}

	/**
	 * OID constructor; the OID is set accordingly and the Variant is null.
	 * @param oid The value of the OID.
	 **/
	VarBind( const NumericOid& oid ) : oid( oid ) {}

	/**
	 * OID/value constructor; the OID is set accordingly and the Variant is as well.
	 * @param oid The value of the OID.
	 * @param value The value of variant.
	 **/
	VarBind( const NumericOid& oid, const Variant& value ) : oid( oid ), value( value ) {}

	//! TODO
	~VarBind() {}

public:
	//! This is the OID.
	NumericOid oid;
	//! This is its value.  Since the OID can be mapped to any kind of value,
	//! a Variant is used.
	Variant value;
};

/**
 * This defines a list of varbinds.
 **/
typedef std::vector< std::unique_ptr<VarBind> > VarBindList;

} }

