#pragma once

#include "securitymodel.hpp"
#include "pdu.hpp"

namespace dangerous { namespace snmp {

/**
 * This represents the the response that is returned from an SNMP agent
 * in response to some request that was sent.
 **/
class RawResponse {
public:
	/**
	 * Constructor.
	 **/
	RawResponse();

public:
	//! This contains SecurityModel-specific information about the agent.
	//! This can be used for TODO TODO TODO.
	std::unique_ptr<SecurityModel> securityModel;
	
	//! This is the kind of PDU.  TODO TODO TODO.
	uint8_t pduType;
	
	//! This is the PDU.
	PDU pdu;
};

} }

