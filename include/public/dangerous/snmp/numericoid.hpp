#pragma once

#include <string>
#include <vector>

#include "exception.hpp"

namespace dangerous { namespace snmp {

/**
 * A NumericOid represents an "true" OID; that is, one identified by
 * a series of integers (as opposed to a _named_ OID).  Note that a
 * "proper" OID has certain restrictions on what is _legal_ for the
 * first two integers; however, we will _not_ be enforcing that here
 * because of the use cases of _partial_ OIDs.  Any quality enforcement
 * will be done on the part of the encoders and decoders.
 *
 * An OID respects the concept of "lexicographic" sorting; that is, it
 * can be said that one OID comes _before_ another, and all will agree
 * to this statement.  Thus, OIDs can be sorted.
 **/
class NumericOid {
public:
	/**
	 * Constructor; this creates a NumericOid with no integers.
	 **/
	NumericOid() {}

	/**
	 * Copy constructor; this creates a NumericOid identical to the one given.
	 * @param oid The NumericOid to copy.
	 **/
	NumericOid( const NumericOid& oid );

	/**
	 * String constructor; this creates a NumericOid representing the text
	 * that was given.  In the event that the text does not represent a
	 * numeric OID, this will throw a ParseErrorException.
	 * @param oidString The string to parse.
	 * @throw ParseErrorException
	 **/
	NumericOid( const std::string& oidString ) throw( ParseErrorException );
	
	/**
	 * C-string constructor; this creates a NumericOid representing the text
	 * that was given.  In the event that the text does not represent a
	 * numeric OID, this will throw a ParseErrorException.
	 * @param oidString The string to parse.
	 * @throw ParseErrorException
	 **/
	NumericOid( const char* oidString ) throw( ParseErrorException );

//protected: //< TODO: MAKE THIS PROTECTED AGAIN?
public:
	//! This is the vector that represents the integers that make up
	//! the OID.
	std::vector<unsigned int> numbers;

public:
	/**
	 * This compares this NumericOid to another one.
	 * If they are equal, then this returns true.
	 * @param oid The comparison OID.
	 * @return true if they are equal; false otherwise.
	 **/
	bool equals( const NumericOid& oid ) const;

	/**
	 * This compares this NumericOid to another one.
	 * If this one is less than the other one, then this
	 * returns true.
	 * @param oid The comparison OID.
	 * @return true if this one is smaller; false otherwise.
	 **/
	bool isLessThan( const NumericOid& oid ) const;

	/**
	 * This checks to see if _this OID_ begins with the exact sequence
	 * of the OID given.
	 * @param oid The comparison OID.
	 * @return true if this one begins with the OID given; false otherwise.
	 **/
	bool beginsWith( const NumericOid& oid ) const;

	/**
	 * This returns a string representing the OID.
	 * @return A string that can be used to represent the OID.
	 **/
	std::string str() const;
};

static inline bool operator<( const NumericOid& left, const NumericOid& right ) {
	return left.isLessThan( right );
}

} }

