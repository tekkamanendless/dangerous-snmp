#pragma once

#include "types.hpp"

namespace dangerous { namespace snmp {

/**
 * This is a container class for the enumerations of the
 * PDU "errorStatus" field.
 **/
class PDUErrorStatus {
public:
	/**
	 * An ErrorStatus represents the specific error for a PDU.
	 **/
	enum ErrorStatus {
		noError = 0,
		tooBig = 1,
		noSuchName = 2, //< Only for version-1 compatibility.
		badValue = 3, //< Only for version-1 compatibility.
		readOnly = 4, //< Only for version-1 compatibility.
		genErr = 5,
		noAccess = 6,
		wrongType = 7,
		wrongLength = 8,
		wrongEncoding = 9,
		wrongValue = 10,
		noCreation = 11,
		inconsistentValue = 12,
		resourceUnavailable = 13,
		commitFailed = 14,
		undoFailed = 15,
		authorizationError = 16,
		notWritable = 17,
		inconsistentName = 18
	};

	/**
	 * This returns a pointer to a statically allocated string
	 * that contains the standard name for the error status
	 * given.
	 * @param errorStatus The error status value.
	 * @return A pointer to the name of the value.
	 **/
	static const char* name( int errorStatus );
};

/**
 * A PDU represents an SNMP Protocol Data Unit (PDU).
 * The SNMP PDU was introduced in RFC 1157 (see Section 4,
 * Protocol Specification).
 *
 * An SNMP PDU is intended to be the basic input and output
 * data structure for SNMP.
 **/
class PDU {
public:
	/**
	 * This returns a pointer to a statically allocated string
	 * that contains the standard name for the error status
	 * given.
	 * @param errorStatus The error status value.
	 * @return A pointer to the name of the value.
	 **/
	static const char* errorName( int errorStatus );

public:
	/**
	 * Default constructor.
	 * This creates a PDU with no VarBinds.
	 **/
	PDU();

	/**
	 * Copy constructor.
	 * This creates a PDU that is an exact copy of the specified PDU.
	 * @param pdu The PDU to copy.
	 **/
	PDU( const PDU& pdu );

	/**
	 * Destructor.
	 **/
	~PDU();

public:
	/**
	 * This adds a new VarBind to the PDU (with a null value).
	 * @param oid The OID to add.
	 **/
	void addOid( const NumericOid& oid );

	/**
	 * This checks to see if the PDU contains the given OID.
	 * @param oid The OID to look for.
	 * @return true if it is present, false if it is not.
	 **/
	bool containsOid( const NumericOid& oid );

public: //< TODO????
	//! This is the RequestID for the PDU.
	//! According to RFC 1157,
	//! > RequestIDs are used to distinguish among outstanding requests.  By
	//! > use of the RequestID, an SNMP application entity can correlate
	//! > incoming responses with outstanding requests.  In cases where an
	//! > unreliable datagram service is being used, the RequestID also
	//! > provides a simple means of identifying messages duplicated by the
	//! > network.
	int requestId;

	//! This is the ErrorStatus for the PDU.
	//! This was originally intended to communicate whether or not there
	//! was a major error with the request.  However, for SNMPv2c and higher,
	//! Get requests encode the error at the VarBind level.  However,
	//! this is still valid for Set requests.
	int errorStatus;
	
	//! This is the ErrorIndex for the PDU.
	//! This was originally intended to communicate the index of the VarBind
	//! that caused the error.  However, when there are multiple VarBinds,
	//! there could be multiple VarBinds with errors.  For SNMPv2c and higher
	//! Get requests, the errors are encoded at the VarBind level, so this is
	//! not used.  However, this is still valid for Set requests.
	int errorIndex;

	//! This is the list of VarBinds in the PDU.
	VarBindList varbinds;
};

/**
 * This prints a PDU to a standard output stream.
 * @param out The standard output stream.
 * @param pdu The PDU to print.
 **/
void printPdu( std::ostream& out, const PDU& pdu );

} }

