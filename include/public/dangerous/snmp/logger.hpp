#pragma once

#include <iostream>

namespace dangerous { namespace snmp {

/**
 * The Logger class is the mechanism by which Dangerous SNMP will perform
 * logging operations.
 *
 * Logging is broken down into various sections, so that you may view
 * only what you are interested in looking at.
 **/
class Logger {
public:
	//! This defines an enumeration for the various "systems" in
	//! Dangerous SNMP.
	enum System {
		GENERAL = 0, //<! Logging that has no particular category.
		ENCODING, //<! Logging related to encoding messages.
		DECODING, //!< Logging related to decoding messages.
		BYTESTREAM, //!< Logging related to ByteStream.
		PARSING, //!< Logging related to parsing.
		IO, //!< Logging related to input/output.
		TRANSPORT, //!< Logging related to transports.
		CLIENT, //!< Logging related to clients.
		AGENTX, //!< Logging related to agents.
		MAX //!< Placeholder for the maximum value of the enumeration.
	};

public:
	/**
	 * Default constructor.
	 * All logging systems are turned off.
	 **/
	Logger() {
		for( int i = 0; i < MAX; i++ ) {
			systems[ i ] = false;
		}
	}

	/**
	 * This returns the logging status for the given system.
	 * @param system The logging system.
	 * @return true if the system is on, false otherwise.
	 **/
	inline bool system( System system ) {
		if( (int)system < (int)GENERAL ) {
			return false;
		}
		if( (int)system >= (int)MAX ) {
			return false;
		}
		return systems[ system ];
	}

	/**
	 * This enables or disables logging for the given system.
	 * @param system The logging system.
	 * @param enabled true to enable the system, false to disable it.
	 **/
	inline void system( System system, bool enabled ) {
		if( (int)system < (int)GENERAL ) {
			return;
		}
		if( (int)system >= (int)MAX ) {
			return;
		}
		//DEBUG: std::cout << "Logger::system: Setting system #" << (int)system << " to " << ( enabled ? "on" : "off" ) << std::endl;
		systems[ system ] = enabled;
	}

	/**
	 * This returns the output stream for the Logger.
	 * @return std::cout.
	 **/
	inline std::ostream& out() {
		return std::cout;
	}

protected:
	//! This is the mapping of the logging system to its on/off status.
	bool systems[ Logger::MAX ];
};

//! We will expose a global Logger instance.
extern Logger logger;

} }

