#pragma once

#include <ostream>

#include "numericoid.hpp"


namespace dangerous { namespace snmp {

/**
 * A Variant defines an arbitrary value (within the scope of SNMP).
 * It can have any one of a number of different "types".  Thus, a
 * Variant can represent a null value, an integer, a string, a 64-bit
 * counter, etc.
 *
 * There are four "simple" types in SNMP (imported directly from ASN.1):
 * 1. **SIMPLE_INTEGER:**
 *    This represents a 32-bit signed integer.
 * 2. **SIMPLE_STRING:**
 *    This represents a string (technically, an array of bytes).
 * 3. **SIMPLE_OID:**
 *    This represents an SNMP Object Identifier (OID).
 * 4. **SIMPLE_NULL:**
 *    This represents a null value.  This is the default type for a
 *    Variant instance.
 *
 * There are six "application" types in SNMP (built from simple types):
 * 1. **APPLICATION_IPADDRESS:**
 *    This represents an IPv4 address (thus, it is four bytes long).
 *    In SNMP, this is implemented as a 4-byte string.
 * 2. **APPLICATION_COUNTER32:**
 *    This represents a 32-bit counter value.
 *    Counter values may only legally _increment_ (unless, of course,
 *    they are reset in some capacity or overflow).
 * 3. **APPLICATION_GAUGE32:**
 *    This represents a 32-bit unsigned integer.
 * 4. **APPLICATION_TIMETICKS:**
 *    This also represents a 32-bit unsigned integer, but the _units_
 *    are understood to be 1/100 seconds.
 * 5. **APPLICATION_OPAQUE:**
 *    This represents an arbitrary collection of bytes.
 *    Conceptually, it is the same thing as a string, except that it
 *    is understood that the contents should not be tried to be
 *    interpreted.
 * 6. **APPLICATION_COUNTER64:**
 *    This represents a 64-bit counter value.
 *    Counter values may only legally _increment_ (unless, of course,
 *    they are reset in some capacity or overflow).
 *
 * Finally, there are three "error" types in SNMP (added in SNMPv2).
 * These allow for much better error reporting in "get"-style operations.
 * 1. **CONTEXT_NOSUCHOBJECT:**
 *    This is used as a "value" to mean that there is no such OID.
 * 2. **CONTEXT_NOSUCHINSTANCE:**
 *    This is used as a "value" to mean that there is no such conceptual
 *    instance.
 * 3. **CONTEXT_ENDOFMIBVIEW:**
 *    This is used as a "value" to mean that there is no more to be found.
 *
 * ----
 *
 * If you are interested in determining the exact _type_ of a Variant, then
 * you may use the `type` method, which will return the Variant::Type of the
 * instance.
 *
 * If you are interested in extracting the value from a Variant, then you
 * must first _check_ the type by using one of the `is_...` methods, each
 * of which tests for a particular C++ return type (for example, `is_string`
 * will return true for SIMPLE_STRING, APPLICATION_IPADDRESS, and
 * APPLICATION_OPAQUE).  Once you have determined the appropriate C++ type,
 * you may then use the matching `get_...` function to retrieve the value
 * (for example, if `is_string` returns true, then you may call `get_string`).
 *
 * ----
 *
 * Implementation note: Variant is defined entirely in this header file.
 **/
class Variant {
public:
	/**
	 * This enumeration defines the different types of data that
	 * can be represented by this Variant.
	 * For example, a Variant instance may represent a simple integer,
	 * or a simple string.  However, it might also be an OID, or a null
	 * value.  In addition, there are some SNMP-specific things (such as
	 * IP address, or timeticks) that it can be.
	 **/
	enum Type {
		SIMPLE_INTEGER, //!< Stored as v.i32.
		SIMPLE_STRING, //!< Stored as v.s.
		SIMPLE_OID, //!< Stored as v.oid.
		SIMPLE_NULL, //!< Not stored.
		APPLICATION_IPADDRESS, //!< Stored as v.s.
		APPLICATION_COUNTER32, //!< Stored as v.u32.
		APPLICATION_GAUGE32, //!< Stored as v.u32.
		APPLICATION_TIMETICKS, //!< Stored as v.u32.
		APPLICATION_OPAQUE, //!< Stored as v.s.
		APPLICATION_COUNTER64, //!< Stored as v.u64.
		CONTEXT_NOSUCHOBJECT, //!< Functionally SIMPLE_NULL.
		CONTEXT_NOSUCHINSTANCE, //!< Functionally SIMPLE_NULL.
		CONTEXT_ENDOFMIBVIEW //!< Functionally SIMPLE_NULL.
	};

public:
	/**
	 * Default constructor; the Variant is set to a null value.
	 **/
	Variant() : _type( SIMPLE_NULL ) {}

	/**
	 * Copy constructor; the Variant is set to a copy of the given one.
	 **/
	Variant( const Variant& variant ) {
		_type = variant._type;

		if( variant.is_int32_t() ) {
			v.i32 = variant.v.i32;
		} else if( variant.is_uint32_t() ) {
			v.u32 = variant.v.u32;
		} else if( variant.is_uint64_t() ) {
			v.u64 = variant.v.u64;
		} else if( variant.is_string() ) {
			new( &v.s ) std::string();
			v.s = variant.v.s;
		} else if( variant.is_oid() ) {
			new( &v.oid ) NumericOid();
			v.oid = variant.v.oid;
		}
	}

	/**
	 * Assignment operator; the Variant is set to a copy of the given one.
	 **/
	Variant& operator=( const Variant& variant ) {
		_type = variant._type;

		if( variant.is_int32_t() ) {
			v.i32 = variant.v.i32;
		} else if( variant.is_uint32_t() ) {
			v.u32 = variant.v.u32;
		} else if( variant.is_uint64_t() ) {
			v.u64 = variant.v.u64;
		} else if( variant.is_string() ) {
			new( &v.s ) std::string();
			v.s = variant.v.s;
		} else if( variant.is_oid() ) {
			new( &v.oid ) NumericOid();
			v.oid = variant.v.oid;
		}

		return *this;
	}

	/**
	 * Destructor; all memory is freed.
	 * Since the Variant internally uses a union with non-trivial components,
	 * their destructors will need to be called manually.
	 **/
	~Variant() {
		set_null( SIMPLE_NULL );
	}

	/**
	 * This returns the type of the Variant.
	 * @return The Variant::Type for the variant.
	 **/
	Type type() const {
		return _type;
	}

private:
	/*
	 * Internal workings.
	 */

	/**
	 * This union is used to store the actual value.
	 **/
	union ValueUnion {
		/**
		 * Default constructor.
		 * The actual manipulation of this union will be handled by the Variant itself.
		 **/
		ValueUnion() {
		}

		/**
		 * Default destructor.
		 * The actual freeing of any member items will be handled by the Variant itself.
		 **/
		~ValueUnion() {
		}

		//! This is the signed integer component.
		int32_t i32;
		//! This is the unsigned integer component.
		uint32_t u32;
		//! This is the unsigned, 64-bit integer component.
		uint64_t u64;
		//! This is the string component.
		std::string s;
		//! This is the OID component.
		NumericOid oid;

		// TODO: MOAR! (like what?)
	};

	//! This is the type of the Variant.
	Type _type;
	//! This is the actual value of the Variant.
	ValueUnion v;

public:
	/*
	 * Type setting methods.
	 */

	/**
	 * This restores the Variant to its default, null state.
	 *
	 * Note: this will be called internally to _clear out_ the string
	 * and OID data when the type of the Variant is reassigned.
	 *
	 * @param type The new type for the Variant.
	 *    This may be SIMPLE_NULL, CONTEXT_NOSUCHOBJECT, CONTEXT_NOSUCHINSTANCE, or CONTEXT_ENDOFMIBVIEW.
	 * @throw InvalidTypeException if `type` is not one of the allowed types.
	 **/
	void set_null( Type type ) throw( InvalidTypeException ) {
		// Make sure that the new type is okay for null.
		switch( type ) {
			case SIMPLE_NULL:
			case CONTEXT_NOSUCHOBJECT:
			case CONTEXT_NOSUCHINSTANCE:
			case CONTEXT_ENDOFMIBVIEW:
				break;
			default:
				throw InvalidTypeException( "Invalid type for null." );
		}

		// Clear out any of the non-trivial data if the current
		// type has a non-trivial constructor.
		switch( this->_type ) {
			case SIMPLE_STRING:
				v.s.std::string::~string();
				break;
			case SIMPLE_OID:
				v.oid.~NumericOid();
				break;
			default:
				// No special work necessary.
				;
		}

		this->_type = type;
	}

	/**
	 * This sets the value to a signed, 32-bit integer.
	 * @param type The new type for the Variant.
	 *    This may be SIMPLE_INTEGER.
	 * @param i32 The signed, 32-bit integer.
	 * @throw InvalidTypeException if `type` is not one of the allowed types.
	 **/
	void set_int32_t( Type type, int32_t i32 = 0 ) throw( InvalidTypeException ) {
		switch( type ) {
			case SIMPLE_INTEGER:
				set_null( SIMPLE_NULL );
				v.i32 = i32;
				break;
			default:
				throw InvalidTypeException( "Invalid type for int32_t." );
		}

		this->_type = type;
	}

	/**
	 * This sets the value to an unsigned, 32-bit integer.
	 * @param type The new type for the Variant.
	 *    This may be APPLICATION_COUNTER32, APPLICATION_GAUGE32, or APPLICATION_TIMETICKS.
	 * @param u32 The unsigned, 32-bit integer.
	 * @throw InvalidTypeException if `type` is not one of the allowed types.
	 **/
	void set_uint32_t( Type type, uint32_t u32 = 0 ) throw( InvalidTypeException ) {
		switch( type ) {
			case APPLICATION_COUNTER32:
			case APPLICATION_GAUGE32:
			case APPLICATION_TIMETICKS:
				set_null( SIMPLE_NULL );
				v.u32 = u32;
				break;
			default:
				throw InvalidTypeException( "Invalid type for uint32_t." );
		}
		
		this->_type = type;
	}

	/**
	 * This sets the value to an unsigned, 64-bit integer.
	 * @param type The new type for the Variant.
	 *    This may be APPLICATION_COUNTER64.
	 * @param u64 The unsigned, 64-bit integer.
	 * @throw InvalidTypeException if `type` is not one of the allowed types.
	 **/
	void set_uint64_t( Type type, uint64_t u64 = 0 ) throw( InvalidTypeException ) {
		switch( type ) {
			case APPLICATION_COUNTER64:
				set_null( SIMPLE_NULL );
				v.u64 = u64;
				break;
			default:
				throw InvalidTypeException( "Invalid type for uint64_t." );
		}

		this->_type = type;
	}
	
	/**
	 * This sets the value to a string.
	 * @param type The new type for the Variant.
	 *    This may be SIMPLE_STRING.
	 * @param string The signed integer.
	 * @throw InvalidTypeException if `type` is not one of the allowed types.
	 **/
	void set_string( Type type, std::string string = std::string() ) throw( InvalidTypeException ) {
		switch( type ) {
			case SIMPLE_STRING:
			case APPLICATION_IPADDRESS:
			case APPLICATION_OPAQUE:
				set_null( SIMPLE_NULL );
				new( &v.s ) std::string();
				v.s = string;
				break;
			default:
				throw InvalidTypeException( "Invalid type for std::string." );
		}
		
		this->_type = type;
	}

	/**
	 * This sets the value to an OID.
	 * @param type The new type for the Variant.
	 *    This may be SIMPLE_OID.
	 * @param oid The string.
	 * @throw InvalidTypeException if `type` is not one of the allowed types.
	 **/
	void set_oid( Type type, NumericOid oid = NumericOid() ) throw( InvalidTypeException ) {
		switch( type ) {
			case SIMPLE_OID:
				set_null( SIMPLE_NULL );
				new( &v.oid ) NumericOid();
				v.oid = oid;
				break;
			default:
				throw InvalidTypeException( "Invalid type for NumericOid." );
		}

		this->_type = type;
	}

public:
	/*
	 * Type testing / casting methods.
	 *
	 * These are broken down into two categories:
	 *    1. The "is_<type>" methods.
	 *       These return true or false depending on whether or not you can call the Variant
	 *       member function "get_<type>".
	 *    2. The "get_<type>" methods.
	 *       These return references to the "<type>" requested from the Variant..
	 *       If the Variant type does not match "<type>" (and you should check with the "is_<type>" method),
	 *       then this will throw an exception.
	 */

	/**
	 * This returns whether or not the Variant represents a null value.
	 * In this case, there is no corresponding `get_null` method.
	 *
	 * @return true if the Variant is of type SIMPLE_NULL, CONTEXT_NOSUCHOBJECT, CONTEXT_NOSUCHINSTANCE, or CONTEXT_ENDOFMIBVIEW.
	 **/
	bool is_null() const {
		switch( _type ) {
			case SIMPLE_NULL:
			case CONTEXT_NOSUCHOBJECT:
			case CONTEXT_NOSUCHINSTANCE:
			case CONTEXT_ENDOFMIBVIEW:
				return true;
			default:
				return false;
		}
	}

	/**
	 * This returns whether or not the Variant represents a 32-bit signed integer.
	 * In this case, you may retrieve the value using `get_int32_t`.
	 *
	 * @return true if the Variant is of type SIMPLE_INTEGER.
	 **/
	bool is_int32_t() const {
		switch( _type ) {
			case SIMPLE_INTEGER:
				return true;
			default:
				return false;
		}
	}

	/**
	 * This returns the 32-bit signed integer value of the Variant.
	 * You should first check that this is the correct type by calling `is_int32_t`.
	 *
	 * @return The 32-bit signed integer value.
	 * @throw InvalidTypeException if the Variant is not of this format.
	 **/
	int32_t get_int32_t() const throw( InvalidTypeException ) {
		switch( _type ) {
			case SIMPLE_INTEGER:
				return v.i32;
			default:
				throw InvalidTypeException( "Invalid type for int32_t." );
		}
	}

	/**
	 * This returns whether or not the Variant represents a 32-bit unsigned integer.
	 * In this case, you may retrieve the value using `get_uint32_t`.
	 *
	 * @return true if the Variant is of type APPLICATION_COUNTER32, APPLICATION_GAUGE32, APPLICATION_TIMETICKS.
	 **/
	bool is_uint32_t() const {
		switch( _type ) {
			case APPLICATION_COUNTER32:
			case APPLICATION_GAUGE32:
			case APPLICATION_TIMETICKS:
				return true;
			default:
				return false;
		}
	}

	/**
	 * This returns the 32-bit unsigned integer value of the Variant.
	 * You should first check that this is the correct type by calling `is_uint32_t`.
	 *
	 * @return The 32-bit unsigned integer value.
	 * @throw InvalidTypeException if the Variant is not of this format.
	 **/
	uint32_t get_uint32_t() const throw( InvalidTypeException ) {
		switch( _type ) {
			case APPLICATION_COUNTER32:
			case APPLICATION_GAUGE32:
			case APPLICATION_TIMETICKS:
				return v.u32;
			default:
				throw InvalidTypeException( "Invalid type for uint32_t." );
		}
	}

	/**
	 * This returns whether or not the Variant represents a 64-bit unsigned integer.
	 * In this case, you may retrieve the value using `get_uint64_t`.
	 *
	 * @return true if the Variant is of type APPLICATION_COUNTER64.
	 **/
	bool is_uint64_t() const {
		switch( _type ) {
			case APPLICATION_COUNTER64:
				return true;
			default:
				return false;
		}
	}

	/**
	 * This returns the 64-bit unsigned integer value of the Variant.
	 * You should first check that this is the correct type by calling `is_uint64_t`.
	 *
	 * @return The 64-bit unsigned integer value.
	 * @throw InvalidTypeException if the Variant is not of this format.
	 **/
	uint64_t get_uint64_t() const throw( InvalidTypeException ) {
		switch( _type ) {
			case APPLICATION_COUNTER64:
				return v.u64;
			default:
				throw InvalidTypeException( "Invalid type for uint64_t." );
		}
	}
	
	/**
	 * This returns whether or not the Variant represents a string.
	 * In this case, you may retrieve the value using `get_string`.
	 *
	 * @return true if the Variant is of type SIMPLE_STRING, APPLICATION_IPADDRESS, or APPLICATION_OPAQUE.
	 **/
	bool is_string() const {
		switch( _type ) {
			case SIMPLE_STRING:
			case APPLICATION_IPADDRESS:
			case APPLICATION_OPAQUE:
				return true;
			default:
				return false;
		}
	}

	/**
	 * This returns the string value of the Variant.
	 * You should first check that this is the correct type by calling `is_string`.
	 *
	 * @return The string value.
	 * @throw InvalidTypeException if the Variant is not of this format.
	 **/
	const std::string& get_string() const throw( InvalidTypeException ) {
		switch( _type ) {
			case SIMPLE_STRING:
			case APPLICATION_IPADDRESS:
			case APPLICATION_OPAQUE:
				return v.s;
			default:
				throw InvalidTypeException( "Invalid type for std::string." );
		}
	}

	/**
	 * This returns whether or not the Variant represents an OID.
	 * In this case, you may retrieve the value using `get_oid`.
	 *
	 * @return true if the Variant is of type SIMPLE_OID.
	 **/
	bool is_oid() const {
		switch( _type ) {
			case SIMPLE_OID:
				return true;
			default:
				return false;
		}
	}

	/**
	 * This returns the OID value of the Variant.
	 * You should first check that this is the correct type by calling `is_oid`.
	 *
	 * @return The OID value.
	 * @throw InvalidTypeException if the Variant is not of this format.
	 **/
	const NumericOid& get_oid() const throw( InvalidTypeException ) {
		switch( _type ) {
			case SIMPLE_OID:
				return v.oid;
			default:
				throw InvalidTypeException( "Invalid type for NumericOid." );
		}
	}
};

static inline std::ostream& operator<<( std::ostream& out, const Variant& variant ) {
	switch( variant.type() ) {
		case Variant::SIMPLE_INTEGER:
			out << variant.get_int32_t();
			break;
		case Variant::SIMPLE_STRING:
			out << variant.get_string();
			break;
		case Variant::SIMPLE_OID:
			out << variant.get_oid().str();
			break;
		case Variant::SIMPLE_NULL:
			out << "null";
			break;
		case Variant::APPLICATION_IPADDRESS:
			out << variant.get_string(); //!< TODO: IPv4 formatting
			break;
		case Variant::APPLICATION_COUNTER32:
			out << variant.get_uint32_t();
			break;
		case Variant::APPLICATION_GAUGE32:
			out << variant.get_uint32_t();
			break;
		case Variant::APPLICATION_TIMETICKS:
			out << variant.get_int32_t();
			break;
		case Variant::APPLICATION_OPAQUE:
			out << variant.get_string();
			break;
		case Variant::APPLICATION_COUNTER64:
			out << variant.get_uint64_t();
			break;
		case Variant::CONTEXT_NOSUCHOBJECT:
			out << "noSuchObject";
			break;
		case Variant::CONTEXT_NOSUCHINSTANCE:
			out << "noSuchInstance";
			break;
		case Variant::CONTEXT_ENDOFMIBVIEW:
			out << "endOfMibView";
			break;
	}

	return out;
}

} }

