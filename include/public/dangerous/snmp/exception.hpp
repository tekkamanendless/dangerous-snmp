#pragma once

#include <stdexcept>

namespace dangerous { namespace snmp {

/**
 * This defines the base exception class for Dangerous SNMP.
 **/
class Exception : public std::exception {
public:
	/**
	 * Constructor.
	 *
	 * @param text The exception text.
	 **/
	explicit Exception( const char* text ) noexcept;
	/**
	 * Constructor.
	 *
	 * @param text The exception text.
	 **/
	explicit Exception( const std::string& text ) noexcept;

	/**
	 * This returns the text for the exception.
	 *
	 * @return The exception's text.
	 **/
	virtual const char* what() const noexcept;

protected:
	//! This is the exception text.
	std::string _text;
};

/**
 * This represents an authentication failure.
 **/
class AuthenticationFailureException : public Exception {
public:
	/**
	 * Constructor.
	 *
	 * @param text The exception text.
	 **/
	explicit AuthenticationFailureException( const char* text ) noexcept;
	/**
	 * Constructor.
	 *
	 * @param text The exception text.
	 **/
	explicit AuthenticationFailureException( const std::string& text ) noexcept;
};

/**
 * This represents a failed attempt to generate authentication information.
 **/
class AuthenticationGenerationException : public Exception {
public:
	/**
	 * Constructor.
	 *
	 * @param text The exception text.
	 **/
	explicit AuthenticationGenerationException( const char* text ) noexcept;
	/**
	 * Constructor.
	 *
	 * @param text The exception text.
	 **/
	explicit AuthenticationGenerationException( const std::string& text ) noexcept;
};

/**
 * This represents a failed attempt to decrypt information.
 **/
class DecryptionException : public Exception {
public:
	/**
	 * Constructor.
	 *
	 * @param text The exception text.
	 **/
	explicit DecryptionException( const char* text ) noexcept;
	/**
	 * Constructor.
	 *
	 * @param text The exception text.
	 **/
	explicit DecryptionException( const std::string& text ) noexcept;
};

/**
 * This represents a failed attempt to encrypt information.
 **/
class EncryptionException : public Exception {
public:
	/**
	 * Constructor.
	 *
	 * @param text The exception text.
	 **/
	explicit EncryptionException( const char* text ) noexcept;
	/**
	 * Constructor.
	 *
	 * @param text The exception text.
	 **/
	explicit EncryptionException( const std::string& text ) noexcept;
};

/**
 * This represents a failed type conversion or use.
 **/
class InvalidTypeException : public Exception {
public:
	/**
	 * Constructor.
	 *
	 * @param text The exception text.
	 **/
	explicit InvalidTypeException( const char* text ) noexcept;
	/**
	 * Constructor.
	 *
	 * @param text The exception text.
	 **/
	explicit InvalidTypeException( const std::string& text ) noexcept;
};

/**
 * This represents a parse-error event.
 **/
class ParseErrorException : public Exception {
public:
	/**
	 * Constructor.
	 *
	 * @param text The exception text.
	 **/
	explicit ParseErrorException( const char* text ) noexcept;
	/**
	 * Constructor.
	 *
	 * @param text The exception text.
	 **/
	explicit ParseErrorException( const std::string& text ) noexcept;
};

/**
 * This represents a timeout event.
 **/
class TimeoutException : public Exception {
public:
	/**
	 * Constructor.
	 *
	 * @param text The exception text.
	 **/
	explicit TimeoutException( const char* text ) noexcept;
	/**
	 * Constructor.
	 *
	 * @param text The exception text.
	 **/
	explicit TimeoutException( const std::string& text ) noexcept;
};

/**
 * This represents an unknown transport event.
 **/
class UnknownTransportException : public Exception {
public:
	/**
	 * Constructor.
	 *
	 * @param text The exception text.
	 **/
	explicit UnknownTransportException( const char* text ) noexcept;
	/**
	 * Constructor.
	 *
	 * @param text The exception text.
	 **/
	explicit UnknownTransportException( const std::string& text ) noexcept;
};

} }

