#pragma once

#include "securitymodel.hpp"
#include "pdu.hpp"

namespace dangerous { namespace snmp {

/**
 * This represents the result of a logical operation performed by
 * the client.
 **/
class Response {
public:
	/**
	 * The Details class adds a bit of extra information to a Response.
	 * The information is not directly related to the SNMP standard,
	 * which is why it's in a separate sub-structure.
	 **/
	class Details {
	public:
		/**
		 * Default constructor.
		 * This sets the object to reasonable defaults.
		 **/
		Details() {
			operations = 0;
		}

	public:
		//! This is the number of protocol-level operations that were
		//! performed during the course of actually getting this Response.
		unsigned int operations;
	};

public:
	/**
	 * Default constructor.
	 **/
	Response();

public:
	//! This represents any relevant details about the operations needed
	//! in order to produce this response.
	Details details;

	//! This is the PDU that contains the SNMP-level information; that is,
	//! this is the actual response.
	PDU pdu;
};

} }

