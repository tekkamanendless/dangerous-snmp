#include "dangerous/snmp/logger.hpp"

#include "context-private.hpp"
#include "transport.hpp"

#include <iostream>

namespace dangerous { namespace snmp {

Transport::Transport( std::weak_ptr<Context::PrivateData> context ) :
	readerStream( new ByteStream() ),
	writerStream( new ByteStream() ),
	context( context )
{
}

Transport::~Transport() {
}

/**
 * This returns whether or not the transport is connected.
 * @return true if it is connected; false if it is not.
 **/
bool Transport::isConnected() {
	return false;
}

/**
 * This instructs the transport to connect to its endpoint.
 * This may or may not succeed; to check for success, call
 * "isConnected" afterward.
 **/
void Transport::connect() {
}

/**
 * This shuts down the transport (if it was already connected).
 * If the transport was not connected, then this does nothing.
 **/
void Transport::disconnect() {
}

void Transport::commitWrite() {
	if( logger.system( Logger::TRANSPORT ) ) {
		logger.out() << "Transport::commitWrite: Ending writerStream packet." << std::endl;
	}
	writerStream->endPacket();
	
	if( logger.system( Logger::TRANSPORT ) ) {
		logger.out() << "Transport::commitWrite: Getting a pointer to the context." << std::endl;
	}
	auto sharedContext = context.lock();
	if( ! sharedContext ) {
		// TODO: Uh oh, looks like "context" was destroyed.
		return;
	}

	if( logger.system( Logger::TRANSPORT ) ) {
		logger.out() << "Transport::commitWrite: Waking up the io system." << std::endl;
	}
	sharedContext->io.wakeup();
}

void Transport::registerReadWriteStreams( int readHandle, int writeHandle ) {
	if( logger.system( Logger::TRANSPORT ) ) {
		logger.out() << "Transport::registerReadWriteStreams: Getting a pointer to the context." << std::endl;
	}
	auto sharedContext = context.lock();
	if( ! sharedContext ) {
		// TODO: Uh oh, looks like "context" was destroyed.
		return;
	}

	if( readHandle >= 0 ) {
		if( logger.system( Logger::TRANSPORT ) ) {
			logger.out() << "Transport::registerReadWriteStreams: Registering handle #" << readHandle << " for reading." << std::endl;
		}
		readerStream->copyFrom( std::string(""), ByteStream::READ_WRITE );
		sharedContext->io.registerInput( readHandle, readerStream );
	}
	
	if( writeHandle >= 0 ) {
		if( logger.system( Logger::TRANSPORT ) ) {
			logger.out() << "Transport::registerReadWriteStreams: Registering handle #" << readHandle << " for writing." << std::endl;
		}
		writerStream->copyFrom( std::string(""), ByteStream::READ_WRITE );
		sharedContext->io.registerOutput( writeHandle, writerStream );
	}
}

void Transport::unregisterReadWriteStreams( int readHandle, int writeHandle ) {
	if( logger.system( Logger::TRANSPORT ) ) {
		logger.out() << "Transport::unregisterReadWriteStreams: Getting a pointer to the context." << std::endl;
	}
	auto sharedContext = context.lock();
	if( ! sharedContext ) {
		// TODO: Uh oh, looks like "context" was destroyed.
		return;
	}

	if( readHandle >= 0 ) {
		if( logger.system( Logger::TRANSPORT ) ) {
			logger.out() << "Transport::unregisterReadWriteStreams: Unregistering handle #" << readHandle << " for reading." << std::endl;
		}
		sharedContext->io.unregisterInput( readHandle );
	}
	
	if( writeHandle >= 0 ) {
		if( logger.system( Logger::TRANSPORT ) ) {
			logger.out() << "Transport::unregisterReadWriteStreams: Unregistering handle #" << readHandle << " for writing." << std::endl;
		}
		sharedContext->io.unregisterOutput( writeHandle );
	}
}

} }

