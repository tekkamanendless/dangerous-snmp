# libdangerous-snmp-base

This library contains some of the "base" classes required by the other SNMP libraries.
In particular, this defines the "Context" class, which is required for performing any
operations that require I/O (from the "agentx" and "client" libraries).

I/O is handled as a single thread _per Context instance_ so that different libraries or
areas of code that use Dangerous SNMP will not conflict.  It is recommended that you use
as few Contexts as possible; typically, a single Context per application is sufficient.

## Components

### ByteStream

A ByteStream is basically an array of bytes that can be read from and written to.  It is
used as a general streaming class and wrapped by other, more protocol-specific classes to
handle proper reading and writing of structured data.

ByteStreams have the concept of being "live", which means that there is another thread
somewhere that will have a pointer to it and can be writing while the other thread is reading.

### I/O System

The goal here is to push all I/O to a single thread where it can be handled, transforming
_all_ network read/write operations into simple ByteStream operations.  Since ByteStreams
can grow over time, this thread also resets them as needed.

### Transport

A transport is a means of communicating.  The goal is to basically communicate SNMP information
over the network, but transports are also used for local AgentX communication, as well.

## TODO

* I/O System: Add some kind of mechanism for being able to reconnect on failure; we can
  already detect when a socket is _closed_ out from under us (end-of-file error).
    * I am currently handling this at the AgentX level; we'll see about other entities,
      once they get written.

