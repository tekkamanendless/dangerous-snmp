#include "dangerous/snmp/logger.hpp"

namespace dangerous { namespace snmp {

//! This is the global Logger instance that all Dangerous SNMP libraries
//! may use.
Logger logger;

} }

