#include "asn1/asn1.hpp"
#include "berstream.hpp"

#include "dangerous/snmp/logger.hpp"

#include <cstring>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <stdexcept>

namespace dangerous { namespace snmp {

/**
 * TODO
 **/
BerStream::BerStream( ByteStream* byteStream ) {
	if( byteStream == nullptr ) {
		throw std::runtime_error( "BerStream was given a null ByteStream." );
	}

	_byteStream = byteStream;
}

/**
 * TODO
 **/
bool BerStream::copyTo( std::vector<char>& buffer ) {
	_byteStream->copyTo( buffer );

	return true;
}

/**
 * This reads an ASN.1 type from the BerStream.
 * However, this does _not_ advance the read head of the BerStream.
 * @param type A reference to the integer to store the type.
 * @return true on success, false on failure.
 **/
bool BerStream::peekType( uint8_t& type ) {
	type = 0;

	char byte = 0;
	bool status = _byteStream->peekByte( byte );
	if( ! status ) {
		// TODO: Log if debugging.
		return false;
	}

	type = byte;

	return true;
}

/**
 * This reads an ASN.1 type from the BerStream.
 * @param type A reference to the integer to store the type.
 * @return true on success, false on failure.
 **/
bool BerStream::readType( uint8_t& type ) {
	type = 0;

	char byte = 0;
	bool status = _byteStream->readByte( byte );
	if( ! status ) {
		// TODO: Log if debugging.
		return false;
	}

	type = byte;

	return true;
}

/**
 * This reads an ASN.1 length from the BerStream.
 * @param length A reference to the integer to store the length.
 * @return true on success, false on failure.
 **/
bool BerStream::readLength( unsigned int& length ) {
	// Initialize the length to zero.
	length = 0;

	//! This is the first byte that we are going to read.
	uint8_t byte = 0;
	//! This is the number of bytes that we have read.
	unsigned int bytesRead = 0;

	// Pull a byte off of the buffer.
	bool success = _byteStream->readBytes( (char*)&byte, 1, bytesRead );
	if( ! success ) {
		return false;
	}

	// If the _first_ bit is not set, then the length is simply
	// the value of the byte.
	if( ( byte & 0x80 ) == 0 ) {
		length = ( byte & 0x7F );
		return true;
	}

	// The first bit was set, which means that the length is more than 127 bytes.
	// In this case, the remainder of the first byte is the number of bytes to read
	// in order to construct an integer that actually contains the length.
	//
	// This integer, in theory, could be larger than 32-bits, but it is not possible
	// in SNMP to send a 4GB value, so we are not going to worry about this.

	//! This is the number of bytes to read in order to find out the length.
	//! This is constructed by simply stripping off the first bit.
	unsigned int lengthBytes = ( byte & 0x7F );

	//! This is the buffer that we are going to use to store the new length bytes.
	char lengthBuffer[ lengthBytes ];
	
	// Read the required number of bytes.
	success = _byteStream->readBytes( lengthBuffer, lengthBytes, bytesRead );
	if( ! success ) {
		// TODO: Put this BerStream in an errored state.
		return false;
	}

	// Go through the bytes and construct the length integer.
	for( unsigned int i = 0; i < lengthBytes; i++ ) {
		//! This is the i'th byte.
		uint8_t byte = lengthBuffer[ i ];

		// Shift the length over one byte and incorporate the new byte into it.
		length <<= 8;
		length |= ( byte & 0xFF );
	}

	return true;
}

/**
 * This reads a full TLV set of data and fills out the _whole thing_ (including the
 * type, the length, and the value) exactly as read.
 * @param buffer The vector in which to store the data.
 * @return true on success, false on failure.
 **/
bool BerStream::readFullTlv( std::vector<char>& buffer ) {
	//! This is a temporary BerStream that we're going to use to store the TLV data
	//! as we read it from _this_ BerStream.  When we're done, we'll extract that
	//! data from the temporary stream and update the buffer with it.
	ByteStream byteStream;
	BerStream writerStream( &byteStream );

	// First, we need to read the type.
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "BerStream::readFullTlv: Attempting to read type." << std::endl;
	}
	uint8_t type = 0;
	bool success = readType( type );
	if( ! success ) {
		return false;
	}
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "BerStream::readFullTlv: Read type=" << (int)type << "." << std::endl;
	}
	// Then we have to commit the type to our temporary BerStream.
	success = writerStream.writeType( type );
	if( ! success ) {
		return false;
	}

	// Second, we need to read the length.
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "BerStream::readFullTlv: Attempting to read length." << std::endl;
	}
	unsigned int length = 0;
	success = readLength( length );
	if( ! success ) {
		return false;
	}
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "BerStream::readFullTlv: Read length=" << length << "." << std::endl;
	}
	// Then we have to commit the length to our temporary BerStream.
	success = writerStream.writeLength( length );
	if( ! success ) {
		return false;
	}

	// Third, we have to read the actual value (now that we know its length).
	// We will do this by blindly reading the required number of bytes.
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "BerStream::readFullTlv: Attempting to read value (in " << length << " bytes)." << std::endl;
	}
	char* newBytes = new char[ length ];
	unsigned int bytesRead = 0;
	success = _byteStream->readBytes( newBytes, length, bytesRead );
	if( ! success ) {
		delete [] newBytes;
		return false;
	}
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "BerStream::readFullTlv: Read value (bytes read: " << bytesRead << " of " << length << ")." << std::endl;
	}
	// Then we have to commit the value to our temporary BerStream.
	unsigned int bytesWritten = 0;
	success = writerStream.writeBytes( newBytes, length, bytesWritten );
	if( ! success ) {
		delete [] newBytes;
		return false;
	}
	delete [] newBytes;

	// Finally, we are going to copy the data from the temporary BerStream into the array that
	// was given to us.
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "BerStream::readFullTlv: Converting data to vector buffer." << std::endl;
	}
	writerStream.copyTo( buffer );

	return true;
}

/**
 * This reads an arbitrary amount of bytes into the specified character buffer.
 * @param buffer The character buffer in which to store the read bytes.
 * @param bufferSize The number of bytes to read.  "buffer" must be _at least_ this size.
 * @param bytesRead A reference to an integer to hold the number of bytes that were actually read.
 **/
bool BerStream::readBytes( char* buffer, unsigned int bufferSize, unsigned int& bytesRead ) {
	return _byteStream->readBytes( buffer, bufferSize, bytesRead );
}

/**
 * This writes the given ASN.1 type to the buffer.
 * @param type The type to write.
 * @return true on success, false on failure.
 **/
bool BerStream::writeType( uint8_t type ) {
	return _byteStream->writeByte( type );
}

/**
 * This writes the length to the buffer in BER.
 * @param encodedLength The length to write.
 * @return true on success, false on failure.
 **/
bool BerStream::writeLength( unsigned int encodedLength ) {
	unsigned int increment = asn1::helper::length::byteSize( encodedLength );
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "BerStream: writeLength: Encoding the length (" << encodedLength << ") in " << increment << " bytes." << std::endl;
	}
	if( increment < 1 ) {
		return true;
	}

	char buffer[ increment ];
	unsigned int bytesWritten = asn1::helper::length::bytes( encodedLength, buffer, increment );
	if( logger.system( Logger::BYTESTREAM ) ) {
		logger.out() << "BerStream: Wrote length=" << encodedLength << " in " << bytesWritten << " bytes." << std::endl;
	}

	return _byteStream->writeBytes( buffer, increment, bytesWritten );
}

/**
 * This writes the bytes given to the BerStream.
 * @param buffer The bytes to write.
 * @param bufferSize The number of bytes to write.  "buffer" must be _at least_ this size.
 * @param bytesWritten A reference to an integer to store the number of bytes actually written.
 * @return true on success, false on failure.
 **/
bool BerStream::writeBytes( const char* buffer, unsigned int bufferSize, unsigned int& bytesWritten ) {
	return _byteStream->writeBytes( buffer, bufferSize, bytesWritten );
}

} }

