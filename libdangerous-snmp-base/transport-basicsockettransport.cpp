#include "dangerous/snmp/logger.hpp"

#include "time-functions.hpp"
#include "transport/basicsockettransport.hpp"

#include <string>

#include <iostream>

#include <fcntl.h> //< For "fcntl".
#include <netdb.h> //< For "getaddrinfo".
#include <string.h> //< For "memset".
#include <unistd.h> //< For "close".

namespace dangerous { namespace snmp { namespace transport {

BasicSocketTransport::BasicSocketTransport( std::weak_ptr<Context::PrivateData> context ) : Transport( context ) {
	socketHandle = -1;
}

BasicSocketTransport::~BasicSocketTransport() {
	disconnect();
}

bool BasicSocketTransport::isConnected() {
	if( socketHandle < 0 ) {
		if( logger.system( Logger::TRANSPORT ) ) {
			logger.out() << "BasicSocketTransport::isConnected: No socket has been set up yet (trivially disconnected)." << std::endl;
		}
		return false;
	}

	// Make sure that the socket is still "open".  We will do
	// this by doing a quick "peek" for a single byte.  If we get back
	// exactly ZERO bytes (and NOT an error), then we know that this
	// socket has been closed; it's no good to us now.
	char byte = 0;
	int bytesAvailable = recv( socketHandle, &byte, sizeof(byte), MSG_PEEK | MSG_DONTWAIT );
	if( bytesAvailable == 0 ) {
		if( logger.system( Logger::TRANSPORT ) ) {
			logger.out() << "BasicSocketTransport::isConnected: No bytes are available (disconnected)." << std::endl;
		}
		return false;
	}

	return true;
}

void BasicSocketTransport::connect() {
	disconnect();

	struct addrinfo* clientAddress = NULL;
	struct addrinfo hint;
	//memset( &hint, 0, sizeof(struct addrinfo) );
	memset( &hint, 0, sizeof( decltype(hint) ) );
	hint.ai_family = AF_UNSPEC; //< Allow IPv4 or IPv6.
	hint.ai_socktype = socketOptions();
	hint.ai_flags = 0;
	hint.ai_protocol = protocolNumber();

	if( logger.system( Logger::TRANSPORT ) ) {
		logger.out() << "BasicSocketTransport::connect: SOCK_STREAM: " << SOCK_STREAM << std::endl;
		logger.out() << "BasicSocketTransport::connect: SOCK_DGRAM: " << SOCK_DGRAM << std::endl;
		logger.out() << "BasicSocketTransport::connect: Hint: socktype: " << hint.ai_socktype << ", protocol: " << hint.ai_protocol << std::endl;
	}


	int result = getaddrinfo( address.c_str(), port.c_str(), &hint, &clientAddress );
	if( result != 0 ) {
		if( logger.system( Logger::TRANSPORT ) ) {
			logger.out() << "BasicSocketTransport::connect: Could not get address information from " << address << ":" << port << "." << std::endl;
		}
		return;
	}

	int testCount = 0;
	for( struct addrinfo* testAddress = clientAddress; testAddress != NULL; testAddress = testAddress->ai_next ) {
		testCount++;

		if( logger.system( Logger::TRANSPORT ) ) {
			logger.out() << "BasicSocketTransport::connect: Address #" << testCount << ": socktype: " << clientAddress->ai_socktype << ", protocol: " << clientAddress->ai_protocol << std::endl;
		}
		socketHandle = ::socket( clientAddress->ai_family, clientAddress->ai_socktype, clientAddress->ai_protocol );
		if( socketHandle < 0 ) {
			if( logger.system( Logger::TRANSPORT ) ) {
				logger.out() << "BasicSocketTransport::connect: Address #" << testCount << ": Could not create socket." << std::endl;
			}
			continue;
		}

		result = ::connect( socketHandle, clientAddress->ai_addr, clientAddress->ai_addrlen );
		if( result != 0 ) {
			int errorNumber = errno;

			if( logger.system( Logger::TRANSPORT ) ) {
				logger.out() << "BasicSocketTransport::connect: Address #" << testCount << ": Could not connect socket: error number: " << errorNumber << "." << std::endl;
				switch( errorNumber ) {
					case EACCES:
						logger.out() << "BasicSocketTransport::connect: EACCES." << std::endl;
						break;
					case EPERM:
						logger.out() << "BasicSocketTransport::connect: EPERM." << std::endl;
						break;
					case EADDRINUSE:
						logger.out() << "BasicSocketTransport::connect: EADDRINUSE." << std::endl;
						break;
					case EAFNOSUPPORT:
						logger.out() << "BasicSocketTransport::connect: EAFNOSUPPORT." << std::endl;
						break;
					case EAGAIN:
						logger.out() << "BasicSocketTransport::connect: EAGAIN." << std::endl;
						break;
					case EALREADY:
						logger.out() << "BasicSocketTransport::connect: EALREADY." << std::endl;
						break;
					case EBADF:
						logger.out() << "BasicSocketTransport::connect: EBADF." << std::endl;
						break;
					case ECONNREFUSED:
						logger.out() << "BasicSocketTransport::connect: ECONNREFUSED." << std::endl;
						break;
					case EFAULT:
						logger.out() << "BasicSocketTransport::connect: EFAULT." << std::endl;
						break;
					case EINPROGRESS:
						logger.out() << "BasicSocketTransport::connect: EINPROGRESS." << std::endl;
						break;
					case EINTR:
						logger.out() << "BasicSocketTransport::connect: EINTR." << std::endl;
						break;
					case EISCONN:
						logger.out() << "BasicSocketTransport::connect: EISCONN." << std::endl;
						break;
					case ENETUNREACH:
						logger.out() << "BasicSocketTransport::connect: ENETUNREACH." << std::endl;
						break;
					case ENOTSOCK:
						logger.out() << "BasicSocketTransport::connect: ENOTSOCK." << std::endl;
						break;
					case ETIMEDOUT:
						logger.out() << "BasicSocketTransport::connect: ETIMEDOUT." << std::endl;
						break;
				}
			}

			shutdown( socketHandle, SHUT_RDWR );
			close( socketHandle );

			socketHandle = -1;
			continue;
		}
	}
	
	if( socketHandle < 0 ) {
		if( logger.system( Logger::TRANSPORT ) ) {
			logger.out() << "BasicSocketTransport::connect: Could not create socket (after all " << testCount << " tries)." << std::endl;
		}

		freeaddrinfo( clientAddress );
		return;
	}

	freeaddrinfo( clientAddress );

	
	// Make the socket non-blocking.
	int flags = fcntl( socketHandle, F_GETFL, 0 );
	fcntl( socketHandle, F_SETFL, flags | O_NONBLOCK );

	
	// Register the socket (for both reading and writing) with the parent class.
	registerReadWriteStreams( socketHandle, socketHandle );
}

void BasicSocketTransport::disconnect() {
	if( socketHandle < 0 ) {
		if( logger.system( Logger::TRANSPORT ) ) {
			logger.out() << "BasicSocketTransport::disconnect: Already disconnected; nothing to do." << std::endl;
		}

		return;
	}

	if( logger.system( Logger::TRANSPORT ) ) {
		logger.out() << "BasicSocketTransport::disconnect: Attempting to disconnect." << std::endl;
	}

	unregisterReadWriteStreams( socketHandle, socketHandle );

	shutdown( socketHandle, SHUT_RDWR );
	close( socketHandle );

	socketHandle = -1;
}

int BasicSocketTransport::socketOptions() const {
	return 0;
}

int BasicSocketTransport::protocolNumber() const {
	return 0;
}

} } }

