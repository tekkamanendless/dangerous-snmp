#include <iostream>
#include <thread>
#include <sstream>
#include <stdexcept>
using namespace std;
	    
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#include "iosystem.hpp"
using namespace dangerous;

#include <unittest++/UnitTest++.h>

SUITE(TestIo) { TEST(TestIoThread) {
	std::cout << "Creating IoSystem instance." << std::endl;
	snmp::IoSystem io;
	std::cout << "Starting IoSystem thread." << std::endl;
	io.start();

	std::cout << "Creating socket pair." << std::endl;
	int handles[2];
	int result = socketpair( AF_UNIX, SOCK_DGRAM | SOCK_NONBLOCK, 0 /* protocol */, handles );
	//int result = socketpair( AF_UNIX, SOCK_STREAM | SOCK_NONBLOCK, 0 /* protocol */, handles );
	if( result < 0 ) {
		throw std::runtime_error( "Could not create the sockets." );
	}

	int inputHandle = handles[0];
	int outputHandle = handles[1];

	std::shared_ptr<snmp::ByteStream> inputBuffer( new snmp::ByteStream() );
	std::shared_ptr<snmp::ByteStream> outputBuffer( new snmp::ByteStream() );

	io.registerInput( inputHandle, inputBuffer );
	io.registerOutput( outputHandle, outputBuffer );

	std::thread readerThread(
		[ &inputBuffer ]() {
			std::this_thread::sleep_for( std::chrono::seconds( 3 ) );
				
			std::cout << "Input buffer:" << std::endl;
			std::cout << inputBuffer->debugString() << std::endl;

			int maxi = 4;
			for( int i = 0; i < maxi; i++ ) {
				std::cout << "Reader: " << i << " / " << maxi << ": reading 50 bytes." << std::endl;
				char bytes[ 50 ];
				unsigned int bytesRead = 0;
				bool success = inputBuffer->readBytes( bytes, 50, bytesRead );
				std::cout << "Reader: " << i << " / " << maxi << ": Read " << bytesRead << " / " << 50 << " bytes." << std::endl;
				if( ! success ) {
					std::cout << "Reader: " << i << " / " << maxi << ": Could not read bytes." << std::endl;

					std::cout << "Reader: " << i << " / " << maxi << ": Moving on to next packet." << std::endl;
					inputBuffer->nextPacket();
					continue;
				}
			}

			std::cout << "Reader: All done." << std::endl;
			return;
		}
	);

	std::thread writerThread(
		[ &outputBuffer, &io ]() {
			for( int i = 0; i < 5; i++ ) {
				std::cout << "Waiting 1 second..." << std::endl;
				std::this_thread::sleep_for( std::chrono::seconds( 1 ) );

				int bufferSize = 10 * ( i + 1 );
				char buffer[ bufferSize ];
				for( int c = 0; c < bufferSize; c++ ) {
					buffer[ c ] = '0' + i;
				}

				std::cout << "Sending " << bufferSize << " bytes." << std::endl;
				unsigned int bytesWritten = 0;
				bool success = outputBuffer->writeBytes( buffer, bufferSize, bytesWritten );
				if( ! success ) {
					std::cout << "Writer: Could not write bytes :(" << std::endl;
					continue;
				}
				io.wakeup();

				std::cout << "Writer: Wrote " << bufferSize << " bytes." << std::endl;
			}
			
			std::cout << "Writer: All done." << std::endl;
			return;
		}
	);

	std::cout << "Waiting 8 seconds..." << std::endl;
	std::this_thread::sleep_for( std::chrono::seconds( 8 ) );

	std::cout << "Joining reader thread." << std::endl;
	readerThread.join();
	std::cout << "Joining writer thread." << std::endl;
	writerThread.join();

	std::cout << "Stopping and joining IoSystem thread." << std::endl;
	io.stop();

	std::cout << "Output buffer:" << std::endl;
	std::cout << outputBuffer->debugString() << std::endl;

	std::cout << "Input buffer:" << std::endl;
	std::cout << inputBuffer->debugString() << std::endl;

	CHECK( true );
} }

