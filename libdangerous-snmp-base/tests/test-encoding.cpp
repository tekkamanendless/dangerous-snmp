#include "dangerous/snmp/numericoid.hpp"
#include "berstream.hpp"
#include "encoding/encoding.hpp"
using namespace dangerous;

#include <unittest++/UnitTest++.h>

SUITE(Fast) {

TEST(TestEncodingNumericOid) {
	snmp::ByteStream byteStream;
	snmp::BerStream berStream( &byteStream );

	const char* OID_TEXT = ".1.3.6.1.2.1.6.13.1.1.0.0.0.0.46038.0.0.0.0.0";
	snmp::NumericOid oid( OID_TEXT );
	CHECK( berStream.write<snmp::encoding::OBJECT_IDENTIFIER>( oid ) );

	snmp::NumericOid testOid;
	CHECK( berStream.read<snmp::encoding::OBJECT_IDENTIFIER>( testOid ) );

	CHECK( oid.str().compare( OID_TEXT ) == 0 );
	CHECK( oid.equals( testOid ) );
	CHECK( testOid.str().compare( OID_TEXT ) == 0 );
}

}

