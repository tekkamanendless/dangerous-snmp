#include "dangerous/snmp/numericoid.hpp"
using namespace dangerous;

#include <unittest++/UnitTest++.h>

SUITE(Fast) {

TEST(TestNumericOid_Empty) {
	std::string oidString = "";
	snmp::NumericOid oid( oidString );
	CHECK( oid.str().compare( oidString ) == 0 );
}

TEST(TestNumericOid_Zero) {
	std::string oidString = ".0";
	snmp::NumericOid oid( oidString );
	CHECK( oid.str().compare( oidString ) == 0 );
}

TEST(TestNumericOid_ZeroZero) {
	std::string oidString = ".0.0";
	snmp::NumericOid oid( oidString );
	CHECK( oid.str().compare( oidString ) == 0 );
}

TEST(TestNumericOid_ZeroZeroZero) {
	std::string oidString = ".0.0.0";
	snmp::NumericOid oid( oidString );
	CHECK( oid.str().compare( oidString ) == 0 );
}

TEST(TestNumericOid_One) {
	std::string oidString = ".1";
	snmp::NumericOid oid( oidString );
	CHECK( oid.str().compare( oidString ) == 0 );
}

TEST(TestNumericOid_OneThree) {
	std::string oidString = ".1.3";
	snmp::NumericOid oid( oidString );
	CHECK( oid.str().compare( oidString ) == 0 );
}

TEST(TestNumericOid_DangerousLinux) {
	std::string oidString = ".1.3.6.1.4.1.41326";
	snmp::NumericOid oid( oidString );
	CHECK( oid.str().compare( oidString ) == 0 );
}

TEST(TestNumericOid_ValidButInvalid) {
	std::string oidString = ".9000";
	snmp::NumericOid oid( oidString );
	CHECK( oid.str().compare( oidString ) == 0 );
}

TEST(TestNumericOid_ValidButInvalid2) {
	std::string oidString = ".9000.9001";
	snmp::NumericOid oid( oidString );
	CHECK( oid.str().compare( oidString ) == 0 );
}

TEST(TestNumericOid_NoDot_One) {
	std::string oidString = "1";
	snmp::NumericOid oid( oidString );
	CHECK( oid.str().compare( std::string(".") + oidString ) == 0 );
}

TEST(TestNumericOid_NoDot_OneThree) {
	std::string oidString = "1.3";
	snmp::NumericOid oid( oidString );
	CHECK( oid.str().compare( std::string(".") + oidString ) == 0 );
}

}

