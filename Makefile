SUBDIRS =

SUBDIRS += libdangerous-snmp-base
SUBDIRS += libdangerous-snmp-client
SUBDIRS += libdangerous-snmp-agentx

SUBDIRS += examples

.PHONY: clean subdirs install $(SUBDIRS) docs

subdirs: $(SUBDIRS)

clean: $(SUBDIRS)

install: $(SUBDIRS)

$(SUBDIRS):
	$(MAKE) -C $@ $(MAKECMDGOALS)

docs:
	doxygen Doxyfile
	doxygen Doxyfile.private

