#include "securitymodel/snmpv2c.hpp"

namespace dangerous { namespace snmp { namespace securitymodel {

SNMPv2c::SNMPv2c( const std::string& communityString ) {
	this->_type = SecurityModel::SNMPv2c;
	this->_communityString = communityString;
}

} } }

