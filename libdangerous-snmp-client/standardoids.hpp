#pragma once

#include "dangerous/snmp/numericoid.hpp"

namespace dangerous { namespace snmp {

class StandardOids {
public:
	static const NumericOid usmStatsUnsupportedSecLevels;
	static const NumericOid usmStatsNotInTimeWindows;
	static const NumericOid usmStatsUnknownUserNames;
	static const NumericOid usmStatsUnknownEngineIDs;
	static const NumericOid usmStatsWrongDigests;
	static const NumericOid usmStatsDecryptionErrors;
};

} }

