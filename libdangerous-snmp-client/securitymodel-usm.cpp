#include "dangerous/snmp/securitymodel/usm.hpp"

#include "bytestream.hpp"
#include "encoding/encoding.hpp"
#include "encoding/usmsecurityparameters.hpp"

#include <iomanip>
#include <iostream>

#include <cstring>

#include <openssl/md5.h>
#include <openssl/sha.h>

namespace dangerous { namespace snmp { namespace securitymodel {

/**
 * Per RFC-2574, this function will take a "password" and convert it to
 * a 16-byte "key" suitable for use with MD5 authentication.
 *
 * See RFC-2574, Appendix A, http://tools.ietf.org/html/rfc2574
 *
 * @param password The password to convert to a key.
 * @param engineID The target SNMP engine ID (that is, that of the agent).
 * @return A 16-byte string representing the key.
 **/
std::string password_to_key_md5( const std::string& password, const std::string& engineID ) {
	MD5_CTX md5context;
	u_char* cp = NULL;
	u_char password_buf[64] = { 0 };
	u_long password_index = 0;
	u_long count = 0;
	
	const int passwordLength = password.length();
	const int engineLength = engineID.length();

	MD5_Init( &md5context ); //< Initialize the MD5 context.

	// Use while loop until we've done 1 Megabyte.
	while( count < 1048576 ) {
		cp = password_buf;
		for( int i = 0; i < 64; i++ ) {
			 // Take the next octet of the password, wrapping
			 // to the beginning of the password as necessary.
			 *cp++ = password[ password_index++ % passwordLength ];
		}
		MD5_Update( &md5context, password_buf, 64 );
		count += 64;
	}
	unsigned char key[ 16 ];
	MD5_Final( key, &md5context ); //< tell MD5 we're done.

	// Now localize the key with the engineID and pass
	// through MD5 to produce final key
	// May want to ensure that engineLength <= 32,
	// otherwise need to use a buffer larger than 64
	memcpy( password_buf, key, 16 );
	memcpy( password_buf+16, engineID.c_str(), engineLength );
	memcpy( password_buf+16+engineLength, key, 16 );

	MD5_Init( &md5context );
	MD5_Update( &md5context, password_buf, 32+engineLength );
	MD5_Final( key, &md5context );

	// Copy the results to our string.
	std::string returnValue( 16, 0x00 );
	for( int i = 0; i < 16; i++ ) {
		returnValue[ i ] = key[ i ];
	}
	return returnValue;
}

/**
 * Per RFC-2574, this function will take a "password" and convert it to
 * a 20-byte "key" suitable for use with SHA authentication.
 *
 * See RFC-2574, Appendix A, http://tools.ietf.org/html/rfc2574
 *
 * @param password The password to convert to a key.
 * @param engineID The target SNMP engine ID (that is, that of the agent).
 * @return A 20-byte string representing the key.
 **/
std::string password_to_key_sha( const std::string& password, const std::string& engineID ) {
	SHA_CTX shaContext;
	u_char* cp = NULL;
	u_char password_buf[72] = { 0 };
	u_long password_index = 0;
	u_long count = 0;
	
	const int passwordLength = password.length();
	const int engineLength = engineID.length();

	SHA1_Init( &shaContext ); //< Initialize the SHA context

	/* Use while loop until we've done 1 Megabyte */
	while( count < 1048576 ) {
		cp = password_buf;
		for( int i = 0; i < 64; i++ ) {
			 // Take the next octet of the password, wrapping
			 // to the beginning of the password as necessary.
			 *cp++ = password[ password_index++ % passwordLength ];
		}
		SHA1_Update( &shaContext, password_buf, 64 );
		count += 64;
	}
	unsigned char key[ 20 ];
	SHA1_Final( key, &shaContext ); //< Tell SHA we're done.

	// Now localize the key with the engineID and pass
	// through SHA to produce final key
	// May want to ensure that engineLength <= 32,
	// otherwise need to use a buffer larger than 72
	memcpy( password_buf, key, 20 );
	memcpy( password_buf+20, engineID.c_str(), engineLength );
	memcpy( password_buf+20+engineLength, key, 20 );

	SHA1_Init( &shaContext );
	SHA1_Update( &shaContext, password_buf, 40+engineLength );
	SHA1_Final( key, &shaContext );

	// Copy the results to our string.
	std::string returnValue( 20, 0x00 );
	for( int i = 0; i < 20; i++ ) {
		returnValue[ i ] = key[ i ];
	}
	return returnValue;
}

/**
 * Default constructor; this is essentially "noAuthNoPriv".
 * There is no authentication whatsoever, nor is there any privacy.
 **/
USM::USM() {
	this->_type = SecurityModel::USM;

	this->_username = "";
	this->_authentication = Authentication::NONE;
	this->_authenticationPassword = "";
	this->_authenticationKey = "";
	this->_encryption = Encryption::NONE;
	this->_encryptionPassword = "";
	this->_encryptionKey = "";

	this->_authoritativeEngineID = "";
	this->_authoritativeEngineBoots = 0;
	this->_authoritativeEngineTime = std::chrono::seconds( 0 );
}

/**
 * This constructs a USM instance with the specified parameters.
 * Note that if you specify an encryption type but no authentication type, then
 * this will throw an Exception.
 * @param username The username.
 * @param authentication The kind of authentication to use.
 * @param authenticationPassword The authentication secret that will be used, along with the agent engine ID, to generate an authentication key.
 * @param encryption The kind of encryption to use.
 * @param encryptionPassword The encryption secret that will be used, along with the agent engine ID, to generate an encryption key.
 * @throw Exception
 **/
USM::USM( const std::string& username, Authentication::Type authentication, const std::string& authenticationPassword, Encryption::Type encryption, const std::string& encryptionPassword ) throw( Exception ) {
	this->_type = SecurityModel::USM;

	this->_username = username;
	this->_authentication = authentication;
	this->_authenticationPassword = authenticationPassword;
	this->_authenticationKey = ""; //< This will be computed when we know the engine ID.
	this->_encryption = encryption;
	this->_encryptionPassword = encryptionPassword;
	this->_encryptionKey = ""; //< This will be computed when we know the engine ID.
	
	this->_authoritativeEngineID = "";
	this->_authoritativeEngineBoots = 0;
	this->_authoritativeEngineTime = std::chrono::seconds( 0 );

	if( encryption != Encryption::NONE && authentication == Authentication::NONE ) {
		throw Exception( "Cannot set encryption without setting authentication" );
	}
}

/**
 * Copy constructor.
 * @param usm The USM instance to copy.
 **/
USM::USM( const USM& usm ) {
	this->_type = SecurityModel::USM;

	this->_username = usm._username;
	this->_authentication = usm._authentication;
	this->_authenticationPassword = usm._authenticationPassword;
	this->_authenticationKey = usm._authenticationKey;
	this->_encryption = usm._encryption;
	this->_encryptionPassword = usm._encryptionPassword;
	this->_encryptionKey = usm._encryptionKey;

	this->_authoritativeEngineID = usm._authoritativeEngineID;
	this->_authoritativeEngineBoots = usm._authoritativeEngineBoots;
	this->_authoritativeEngineTime = usm._authoritativeEngineTime;
}

bool USM::isAuthenticated() const {
	return ( _authoritativeEngineID.length() > 0 ) && ( _authoritativeEngineBoots > 0 ) && ( _authoritativeEngineTime.count() > 0 );
}

/**
 * This sets the authoritative engine ID.
 * In addition, if authentication is configured, this will update the
 * authentication and encryption keys.
 * @param authoritativeEngineID The desired authoritative engine ID.
 **/
void USM::authoritativeEngineID( const std::string& authoritativeEngineID ) {
	this->_authoritativeEngineID = authoritativeEngineID;

	switch( this->_authentication ) {
		case Authentication::NONE:
			break;
		case Authentication::MD5:
			this->_authenticationKey = password_to_key_md5( this->_authenticationPassword, this->_authoritativeEngineID );
			if( this->_encryption != Encryption::NONE ) {
				this->_encryptionKey = password_to_key_md5( this->_encryptionPassword, this->_authoritativeEngineID );
			}
			break;
		case Authentication::SHA:
			this->_authenticationKey = password_to_key_sha( this->_authenticationPassword, this->_authoritativeEngineID );
			if( this->_encryption != Encryption::NONE ) {
				this->_encryptionKey = password_to_key_sha( this->_encryptionPassword, this->_authoritativeEngineID );
			}
			break;
	}
}

} } }

