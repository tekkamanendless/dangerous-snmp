#include "securitymodel/snmpv1.hpp"

namespace dangerous { namespace snmp { namespace securitymodel {

SNMPv1::SNMPv1( const std::string& communityString ) {
	this->_type = SecurityModel::SNMPv1;
	this->_communityString = communityString;
}

} } }

