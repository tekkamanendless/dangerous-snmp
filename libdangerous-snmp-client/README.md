# libdangerous-snmp-client

This library provides SNMP "client" functionality; that is, it will allow you
to connect to _remote_ SNMP "agents" and issue various SNMP commands.

This is awesome and namespaced and class-based.  Get over it.

## TODO

* Client: Add a notion of "capabilities"; that is, what can _this particular client_
  support, given his version, configuration, etc.?
* Modify Encoder -> BerHelper
    * encodeToString
    * encodeToVector
    * decodeFromString
    * decodeFromVector
* Review the BerStream class to see what improvements can be made.  My work so far
  has been a bit ugly.
* Revisit the "ProtocolEncoder" static classes to see what improvements can be wrought
  upon them.  For example, instead of playing directly with character pointers, perhaps
  they can play with ByteStreams or BerStreams, making them far simpler.
* Support asynchronous messages so that we can test getting things back out of
  order.
* Unit tests for:
    * Encoding
    * Decoding
* Client: Clean up the notion of "authenticate"; maybe break it up or rename it.
* SNMPv3/USM engine "maintenance"; that is, tracking the uptime for believability
  since both of our clocks should be ticking up.

## Usage

### UDP-based agent

This example code will set up a client to connect to an agent using UDP, a
quite-common way of connecting.

	#include <iostream>
	using namespace std;

	#include <dangerous/snmp/client.hpp>
	using namespace dangerous;
	
	int main( int argc, char** argv ) {
		//! This is the SNMP "context" that we'll be using for our application.
		snmp::Context context;
		
		//! Create a client (using our application context).
		snmp::Client client( context );
		
		// Attempt to configure the client.
		try {
			client.setUri( "udp://your-agent.example.com:161" );
			client.useVersion1( "public" );
		} catch( snmp::Exception& e ) {
			cerr << "Could not set up client: " << e.what() << endl;
			return 1;
		}
		
		// Attempt to issue a request.
		try {
			std::unique_ptr<snmp::Response> response = client.get( { "1.3.6.1.2.1.2.2.1.1.2" } );
			for( auto varbind : response->varbinds ) {
				cout << "ifIndex is: " << (int32_t)varbind.value << endl;
			}
		} catch( snmp::Exception& e ) {
			cerr << "Could not get a response for 'ifIndex.2': " << e.what() << endl;
		}

		// Attempt to issue a request.
		try {
			std::unique_ptr<snmp::Response> response = client.get( { "1.3.6.1.2.1.2.2.1.2.2" } );
			for( auto varbind : response->varbinds ) {
				cout << "ifName is: " << (std::string)varbind.value << endl;
			}
		} catch( snmp::Exception& e ) {
			cerr << "Could not get a response for 'ifName.2': " << e.what() << endl;
		}
		
		return 0;
	}

## Design

### Exceptions

The philosophy of exceptions used is the following:

1. If an error condition is going to be _handled_, then it should return a failure code (such as "false").
   This way, the caller can appropriately address the situation in as efficient a manner as possible.
   If multiple failure conditions exist, each with a particular "code" and a separate way to handle them,
   then this function should _not_ be exposed to the end-user; it should remain internal to this library.
1. If an error condition is _not_ going to be handled, then it should throw an exception.  This signals
   to the end-user that a situation came up that the library could _not_ handle on its own in a manner that
   it felt would be appropriate.  At this point, it is the user's decision what to do.  This kind of situation
   should have a limited number of _types_ of exceptions that are thrown, and all should be distinctly
   useful to the user.

### Encoding and decoding

Encoding and decoding is handled by a class called ByteStream.  That class has a loose
understanding of BER, so it's his job to work with units of "TLV" (Type, Length, Value).

ByteStream works by being given an encoder/decoder class (as a template parameter).  The
encoder/decoder class has:

* NAME, the name of the entity as best as we can match it to the standards.
* TYPE, the BER type encoding for the entity.
* value\_type, the type that this class operates on.
* read, a function that takes a "value\_type" and some bytes, and updates the value accordingly.
* length, a function that takes a "value\_type" and returns the number of bytes needed to encode it.
* write, a function that takes a "value\_type" and some bytes, and encodes the value onto the bytes.

#### Helper classes

The "Encoder" class simplifies some of the encoding work.  It takes a value (of some kind)
and returns a standard string with the BER-encoded data.

### Core ASN.1

The core ASN.1 classes and routines are located in the "asn1" namespace.

### SNMP's version of ASN.1

The SNMP-specific classes and routines are located in the "encoding" namespace.

### I/O

All I/O is handled through a thread; this is very similar to how ZeroMQ handles its I/O.  This
means that each program should have a Context that it uses to create its Client instances.  Each
context has a thread for I/O, and one should be sufficient for your program.

However, this means that libraries can have their own contexts (with their own threads), so there
won't be global variables and state littering everything.  Which is nice.

## Tests

There are currently unit tests for:

* MD5 password to 16-byte key
* SHA password to 20-byte key
* Basic I/O system functionality
* Numeric OID constructors and functions
* Numeric OID encoding and decoding

