#include "dangerous/snmp/securitymodel/usm.hpp"
using namespace dangerous;

#include <iomanip>
#include <iostream>

#include <unittest++/UnitTest++.h>

SUITE(Fast) {

TEST(TestMd5PasswordKey) {
	std::string password = "maplesyrup";
	std::string expectedKey1 = { (char)0x9f, (char)0xaf, (char)0x32, (char)0x83, (char)0x88, (char)0x4e, (char)0x92, (char)0x83, (char)0x4e, (char)0xbc, (char)0x98, (char)0x47, (char)0xd8, (char)0xed, (char)0xd9, (char)0x63 };
	std::string engineId = { (char)0x00, (char)0x00, (char)0x00, (char)0x00, (char)0x00, (char)0x00, (char)0x00, (char)0x00, (char)0x00, (char)0x00, (char)0x00, (char)0x02 };
	std::string expectedKey2 = { (char)0x52, (char)0x6f, (char)0x5e, (char)0xed, (char)0x9f, (char)0xcc, (char)0xe2, (char)0x6f, (char)0x89, (char)0x64, (char)0xc2, (char)0x93, (char)0x07, (char)0x87, (char)0xd8, (char)0x2b };

	snmp::securitymodel::USM usm( "username", snmp::Authentication::MD5, password, snmp::Encryption::NONE, "" );
	usm.authoritativeEngineID( engineId );

	std::cout << std::setw( 2 ) << std::hex;
	for( int i = 0; i < usm.authenticationKey().length(); i++ ) {
		std::cout << (unsigned int)( usm.authenticationKey()[ i ] & 0xFF ) << " ";
	}
	std::cout << std::endl;

	CHECK( usm.authenticationKey().compare( expectedKey2 ) == 0 );
}

}

