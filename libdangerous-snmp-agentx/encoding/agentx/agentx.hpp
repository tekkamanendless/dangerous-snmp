#pragma once

#include "dangerous/snmp/numericoid.hpp"
#include "dangerous/snmp/types.hpp"

#include "bytestream.hpp"

#include "../../types.hpp"


namespace dangerous { namespace snmp { namespace encoding { namespace agentx {
	
static const NumericOid internet = NumericOid( ".1.3.6.1" );

class OBJECT_IDENTIFIER {
public:
	static constexpr const char* NAME = "Object Identifier";
	
	//! This class operates on a NumericOid.
	typedef NumericOid value_type;
		
	static bool write( const value_type& value, ByteStream& byteStream ) {
		unsigned int internetLength = internet.numbers.size();
		int startIndex = 0;
		unsigned int prefix = 0;
		unsigned int numberCount = 0;
		bool success = true;

		if( value.beginsWith( internet ) && value.numbers.size() > internetLength ) {
			startIndex = internetLength;
		}

		if( startIndex != 0 ) {
			prefix = value.numbers[ startIndex ];
			startIndex++;
		}

		numberCount = value.numbers.size() - startIndex;

		success = byteStream.writeByte( (char)numberCount );
		if( ! success ) {
			return false;
		}
		success = byteStream.writeByte( (char)prefix );
		if( ! success ) {
			return false;
		}
		success = byteStream.writeByte( 0 );
		if( ! success ) {
			return false;
		}
		success = byteStream.writeByte( 0 );
		if( ! success ) {
			return false;
		}

		for( unsigned int i = startIndex; i < value.numbers.size(); i++ ) {
			uint32_t number = value.numbers[ i ];
			unsigned int bytesWritten = 0;
			success = byteStream.writeBytes( (char*)&number, sizeof(number), bytesWritten );
			if( ! success ) {
				return false;
			}
		}

		return true;
	}
	
	static bool read( value_type& value, ByteStream& byteStream ) {
		char numberCount = 0;
		char prefix = 0;
		char include = 0;
		char reserved = 0;
		bool success = true;
		
		success = byteStream.readByte( numberCount );
		if( ! success ) {
			return false;
		}
		success = byteStream.readByte( prefix );
		if( ! success ) {
			return false;
		}
		success = byteStream.readByte( include );
		if( ! success ) {
			return false;
		}
		success = byteStream.readByte( reserved );
		if( ! success ) {
			return false;
		}

		if( prefix ) {
			value = internet;
			value.numbers.push_back( prefix );
		}

		for( unsigned int i = 0; i < (unsigned int)numberCount; i++ ) {
			uint32_t number = 0;
			unsigned int bytesRead = 0;
			success = byteStream.readBytes( (char*)&number, sizeof(number), bytesRead );
			if( ! success ) {
				return false;
			}
			value.numbers.push_back( number );
		}

		return true;
	}
};

class SEARCH_RANGE {
public:
	static constexpr const char* NAME = "SearchRange";
	
	//! This class operates on a NumericOidRange.
	typedef NumericOidRange value_type;
		
	static bool write( const value_type& value, ByteStream& byteStream ) {
		unsigned int internetLength = internet.numbers.size();
		int startIndex = 0;
		unsigned int prefix = 0;
		unsigned int numberCount = 0;
		char include = value.startInclusive ? 0x01 : 0x00;
		bool success = true;

		/*
		 * Start OID
		 */

		if( value.start.beginsWith( internet ) && value.start.numbers.size() > internetLength ) {
			startIndex = internetLength;
		}

		if( startIndex != 0 ) {
			prefix = value.start.numbers[ startIndex ];
			startIndex++;
		}

		numberCount = value.start.numbers.size() - startIndex;

		success = byteStream.writeByte( (char)numberCount );
		if( ! success ) {
			return false;
		}
		success = byteStream.writeByte( (char)prefix );
		if( ! success ) {
			return false;
		}
		success = byteStream.writeByte( include );
		if( ! success ) {
			return false;
		}
		success = byteStream.writeByte( 0 );
		if( ! success ) {
			return false;
		}

		for( unsigned int i = startIndex; i < value.start.numbers.size(); i++ ) {
			uint32_t number = value.start.numbers[ i ];
			unsigned int bytesWritten = 0;
			success = byteStream.writeBytes( (char*)&number, sizeof(number), bytesWritten );
			if( ! success ) {
				return false;
			}
		}

		/*
		 * End OID
		 */

		startIndex = 0;
		prefix = 0;
		numberCount = 0;
		include = value.endInclusive ? 0x01 : 0x00;

		if( value.end.beginsWith( internet ) && value.end.numbers.size() > internetLength ) {
			startIndex = internetLength;
		}

		if( startIndex != 0 ) {
			prefix = value.end.numbers[ startIndex ];
			startIndex++;
		}

		numberCount = value.end.numbers.size() - startIndex;

		success = byteStream.writeByte( (char)numberCount );
		if( ! success ) {
			return false;
		}
		success = byteStream.writeByte( (char)prefix );
		if( ! success ) {
			return false;
		}
		success = byteStream.writeByte( include );
		if( ! success ) {
			return false;
		}
		success = byteStream.writeByte( 0 );
		if( ! success ) {
			return false;
		}

		for( unsigned int i = startIndex; i < value.end.numbers.size(); i++ ) {
			uint32_t number = value.end.numbers[ i ];
			unsigned int bytesWritten = 0;
			success = byteStream.writeBytes( (char*)&number, sizeof(number), bytesWritten );
			if( ! success ) {
				return false;
			}
		}

		return true;
	}
	
	static bool read( value_type& value, ByteStream& byteStream ) {
		char numberCount = 0;
		char prefix = 0;
		char include = 0;
		char reserved = 0;
		bool success = true;

		/*
		 * Start OID
		 */
		
		success = byteStream.readByte( numberCount );
		if( ! success ) {
			return false;
		}
		success = byteStream.readByte( prefix );
		if( ! success ) {
			return false;
		}
		success = byteStream.readByte( include );
		if( ! success ) {
			return false;
		}

		value.startInclusive = include ? true : false;

		success = byteStream.readByte( reserved );
		if( ! success ) {
			return false;
		}

		if( prefix ) {
			value.start = internet;
			value.start.numbers.push_back( prefix );
		}

		for( unsigned int i = 0; i < (unsigned int)numberCount; i++ ) {
			uint32_t number = 0;
			unsigned int bytesRead = 0;
			success = byteStream.readBytes( (char*)&number, sizeof(number), bytesRead );
			if( ! success ) {
				return false;
			}
			value.start.numbers.push_back( number );
		}
		
		/*
		 * End OID
		 */

		success = byteStream.readByte( numberCount );
		if( ! success ) {
			return false;
		}
		success = byteStream.readByte( prefix );
		if( ! success ) {
			return false;
		}
		success = byteStream.readByte( include );
		if( ! success ) {
			return false;
		}
		
		value.endInclusive = include ? true : false;

		success = byteStream.readByte( reserved );
		if( ! success ) {
			return false;
		}

		if( prefix ) {
			value.end = internet;
			value.end.numbers.push_back( prefix );
		}

		for( unsigned int i = 0; i < (unsigned int)numberCount; i++ ) {
			uint32_t number = 0;
			unsigned int bytesRead = 0;
			success = byteStream.readBytes( (char*)&number, sizeof(number), bytesRead );
			if( ! success ) {
				return false;
			}
			value.end.numbers.push_back( number );
		}
		
		return true;
	}
};

class OCTET_STRING {
public:
	static constexpr const char* NAME = "Octet String";
	
	//! This class operates on a standard string.
	typedef std::string value_type;
		
	static bool write( const value_type& value, ByteStream& byteStream ) {
		uint32_t length = value.length();

		unsigned int bytesWritten = 0;
		bool success = byteStream.writeBytes( (char*)&length, sizeof(length), bytesWritten );
		if( ! success ) {
			return false;
		}

		for( unsigned int i = 0; i < length; i++ ) {
			char byte = value[ i ];
			success = byteStream.writeByte( byte );
			if( ! success ) {
				return false;
			}
		}

		unsigned int paddingBytes = ( length % 4 == 0 ) ? 0 : ( 4 - length % 4 );

		for( unsigned int i = 0; i < paddingBytes; i++ ) {
			char byte = 0x00;
			success = byteStream.writeByte( byte );
			if( ! success ) {
				return false;
			}
		}

		return true;
	}

	static bool read( value_type& value, ByteStream& byteStream ) {
		uint32_t length = 0;

		unsigned int bytesRead = 0;
		bool success = byteStream.readBytes( (char*)&length, sizeof(length), bytesRead );
		if( ! success ) {
			return false;
		}

		value.resize( length );

		for( unsigned int i = 0; i < length; i++ ) {
			char byte = 0x00;
			success = byteStream.readByte( byte );
			if( ! success ) {
				return false;
			}

			value[ i ] = byte;
		}

		unsigned int paddingBytes = ( length % 4 == 0 ) ? 0 : ( 4 - length % 4 );

		for( unsigned int i = 0; i < paddingBytes; i++ ) {
			char byte = 0x00;
			success = byteStream.readByte( byte );
			if( ! success ) {
				return false;
			}
		}

		return true;
	}
};

class VARBIND {
public:
	static constexpr const char* NAME = "VarBind";
	
	//! This class operates on a VarBind.
	typedef VarBind value_type;
		
	static bool write( const value_type& value, ByteStream& byteStream ) {
		uint16_t type = 0x00;

		switch( value.value.type() ) {
			case Variant::SIMPLE_INTEGER:
				type = 2;
				break;
			case Variant::SIMPLE_STRING:
				type = 4;
				break;
			case Variant::SIMPLE_NULL:
				type = 5;
				break;
			case Variant::SIMPLE_OID:
				type = 6;
				break;
			case Variant::APPLICATION_IPADDRESS:
				type = 64;
				break;
			case Variant::APPLICATION_COUNTER32:
				type = 65;
				break;
			case Variant::APPLICATION_GAUGE32:
				type = 66;
				break;
			case Variant::APPLICATION_TIMETICKS:
				type = 67;
				break;
			case Variant::APPLICATION_OPAQUE:
				type = 68;
				break;
			case Variant::APPLICATION_COUNTER64:
				type = 70;
				break;
			case Variant::CONTEXT_NOSUCHOBJECT:
				type = 128;
				break;
			case Variant::CONTEXT_NOSUCHINSTANCE:
				type = 129;
				break;
			case Variant::CONTEXT_ENDOFMIBVIEW:
				type = 130;
				break;
			default:
				return false;
		}

		unsigned int bytesWritten = 0;
		bool success = byteStream.writeBytes( (char*)&type, sizeof(type), bytesWritten );
		if( ! success ) {
			return false;
		}

		uint16_t reserved = 0x0000;
		success = byteStream.writeBytes( (char*)&reserved, sizeof(reserved), bytesWritten );
		if( ! success ) {
			return false;
		}

		success = OBJECT_IDENTIFIER::write( value.oid, byteStream );
		if( ! success ) {
			return false;
		}

		switch( value.value.type() ) {
			case Variant::SIMPLE_INTEGER: {
				int32_t data = value.value.get_int32_t();
				success = byteStream.writeBytes( (char*)&data, sizeof(data), bytesWritten );
				break;
			}
			case Variant::SIMPLE_STRING:
				success = OCTET_STRING::write( value.value.get_string(), byteStream );
				break;
			case Variant::SIMPLE_NULL:
				// (No data payload required.)
				break;
			case Variant::SIMPLE_OID:
				success = OBJECT_IDENTIFIER::write( value.value.get_oid(), byteStream );
				break;
			case Variant::APPLICATION_IPADDRESS:
				success = OCTET_STRING::write( value.value.get_string(), byteStream );
				break;
			case Variant::APPLICATION_COUNTER32: {
				uint32_t data = value.value.get_uint32_t();
				success = byteStream.writeBytes( (char*)&data, sizeof(data), bytesWritten );
				break;
			}
			case Variant::APPLICATION_GAUGE32: {
				uint32_t data = value.value.get_uint32_t();
				success = byteStream.writeBytes( (char*)&data, sizeof(data), bytesWritten );
				break;
			}
			case Variant::APPLICATION_TIMETICKS: {
				uint32_t data = value.value.get_uint32_t();
				success = byteStream.writeBytes( (char*)&data, sizeof(data), bytesWritten );
				break;
			}
			case Variant::APPLICATION_OPAQUE:
				success = OCTET_STRING::write( value.value.get_string(), byteStream );
				break;
			case Variant::APPLICATION_COUNTER64: {
				uint64_t data = value.value.get_uint64_t();
				success = byteStream.writeBytes( (char*)&data, sizeof(data), bytesWritten );
				break;
			}
			case Variant::CONTEXT_NOSUCHOBJECT:
				// (No data payload required.)
				break;
			case Variant::CONTEXT_NOSUCHINSTANCE:
				// (No data payload required.)
				break;
			case Variant::CONTEXT_ENDOFMIBVIEW:
				// (No data payload required.)
				break;
			default:
				return false;
		}

		if( ! success ) {
			return false;
		}

		return true;
	}

	static bool read( value_type& value, ByteStream& byteStream ) {
		unsigned int bytesRead = 0;

		uint16_t type = 0x00;
		bool success = byteStream.readBytes( (char*)&type, sizeof(type), bytesRead );
		if( ! success ) {
			return false;
		}

		uint16_t reserved = 0x0000;
		success = byteStream.readBytes( (char*)&reserved, sizeof(reserved), bytesRead );
		if( ! success ) {
			return false;
		}

		success = OBJECT_IDENTIFIER::read( value.oid, byteStream );
		if( ! success ) {
			return false;
		}

		switch( type ) {
			case 2: {
				int32_t data = 0;
				success = byteStream.readBytes( (char*)&data, sizeof(data), bytesRead );
				if( ! success ) {
					return false;
				}
				value.value.set_int32_t( Variant::SIMPLE_INTEGER, data );
				break;
			}
			case 4: {
				std::string data;
				success = OCTET_STRING::read( data, byteStream );
				if( ! success ) {
					return false;
				}
				value.value.set_string( Variant::SIMPLE_STRING, data );
				break;
			}
			case 5:
				value.value.set_null( Variant::SIMPLE_NULL );
				break;
			case 6: {
				NumericOid data;
				success = OBJECT_IDENTIFIER::read( data, byteStream );
				if( ! success ) {
					return false;
				}
				value.value.set_oid( Variant::SIMPLE_OID, data );
				break;
			}
			case 64: {
				std::string data;
				success = OCTET_STRING::read( data, byteStream );
				if( ! success ) {
					return false;
				}
				value.value.set_string( Variant::APPLICATION_IPADDRESS, data );
				break;
			}
			case 65: {
				uint32_t data = 0;
				success = byteStream.readBytes( (char*)&data, sizeof(data), bytesRead );
				if( ! success ) {
					return false;
				}
				value.value.set_uint32_t( Variant::APPLICATION_COUNTER32, data );
				break;
			}
			case 66: {
				uint32_t data = 0;
				success = byteStream.readBytes( (char*)&data, sizeof(data), bytesRead );
				if( ! success ) {
					return false;
				}
				value.value.set_uint32_t( Variant::APPLICATION_GAUGE32, data );
				break;
			}
			case 67: {
				uint32_t data = 0;
				success = byteStream.readBytes( (char*)&data, sizeof(data), bytesRead );
				if( ! success ) {
					return false;
				}
				value.value.set_uint32_t( Variant::APPLICATION_TIMETICKS, data );
				break;
			}
			case 68: {
				std::string data;
				success = OCTET_STRING::read( data, byteStream );
				if( ! success ) {
					return false;
				}
				value.value.set_string( Variant::APPLICATION_OPAQUE, data );
				break;
			}
			case 70: {
				uint64_t data = 0;
				success = byteStream.readBytes( (char*)&data, sizeof(data), bytesRead );
				if( ! success ) {
					return false;
				}
				value.value.set_uint64_t( Variant::APPLICATION_COUNTER64, data );
				break;
			}
			case 128:
				value.value.set_null( Variant::CONTEXT_NOSUCHOBJECT );
				break;
			case 129:
				value.value.set_null( Variant::CONTEXT_NOSUCHINSTANCE );
				break;
			case 130:
				value.value.set_null( Variant::CONTEXT_ENDOFMIBVIEW );
				break;
			default:
				return false;
		}

		return true;
	}
};

} } } }
