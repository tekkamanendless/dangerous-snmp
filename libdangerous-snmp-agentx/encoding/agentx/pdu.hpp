#pragma once

#include "dangerous/snmp/numericoid.hpp"

#include "agentx.hpp"
#include "bytestream.hpp"

#include <chrono>


namespace dangerous { namespace snmp { namespace encoding { namespace agentx {

class PDU {
public:
	enum Type {
		AGENTX_OPEN_PDU = 1,
		AGENTX_CLOSE_PDU = 2,
		AGENTX_REGISTER_PDU = 3,
		AGENTX_UNREGISTER_PDU = 4,
		AGENTX_GET_PDU = 5,
		AGENTX_GETNEXT_PDU = 6,
		AGENTX_GETBULK_PDU = 7,
		AGENTX_TESTSET_PDU = 8,
		AGENTX_COMMITSET_PDU = 9,
		AGENTX_UNDOSET_PDU = 10,
		AGENTX_CLEANUPSET_PDU = 11,
		AGENTX_NOTIFY_PDU = 12,
		AGENTX_PING_PDU = 13,
		AGENTX_INDEXALLOCATE_PDU = 14,
		AGENTX_INDEXDEALLOCATE = 15,
		AGENTX_ADDAGENTCAPS_PDU = 16,
		AGENTX_REMOVEAGENTCAPS_PDU = 17,
		AGENTX_RESPONSE_PDU = 18
	};
public:
};

/**
 * This is a container class for the enumerations of the
 * PDU "error" field.
 **/
class PDUError {
public:
	/**
	 * An Error represents the specific error for a PDU.
	 **/
	enum Error {
		noAgentXError = 0,
		// RFC-1905 errors (see RFC-2741, 6.2.16).
		tooBig = 1,
		noSuchName = 2, //< Only for version-1 compatibility.
		badValue = 3, //< Only for version-1 compatibility.
		readOnly = 4, //< Only for version-1 compatibility.
		genErr = 5,
		noAccess = 6,
		wrongType = 7,
		wrongLength = 8,
		wrongEncoding = 9,
		wrongValue = 10,
		noCreation = 11,
		inconsistentValue = 12,
		resourceUnavailable = 13,
		commitFailed = 14,
		undoFailed = 15,
		authorizationError = 16,
		notWritable = 17,
		inconsistentName = 18,
		// AgentX values (see RFC-2741, 6.2.16).
		openFailed = 256,
		notOpen = 257,
		indexWrongType = 258,
		indexAlreadyAllocated = 259,
		indexNoneAvailable = 260,
		indexNotAllocated = 261,
		unsupportedContext = 262,
		duplicateRegistration = 263,
		unknownRegistration = 264,
		unknownAgentCaps = 265,
		parseError = 266,
		requestDenied = 267,
		processingError = 268
	};

	/**
	 * This returns a pointer to a statically allocated string
	 * that contains the standard name for the error status
	 * given.
	 * @param errorStatus The error status value.
	 * @return A pointer to the name of the value.
	 **/
	//TODO:static const char* name( int errorStatus );
};


class AgentxOpenPdu {
public:
	std::chrono::seconds timeout;
	NumericOid oid;
	std::string description;
};

class AGENTX_OPEN_PDU {
public:
	static constexpr const char* NAME = "agentx-Open-PDU";
	
	//! This class operates on an AgentxOpenPdu.
	typedef AgentxOpenPdu value_type;
		
	static bool write( const value_type& value, ByteStream& byteStream ) {
		uint8_t timeout = value.timeout.count();
		bool success = byteStream.writeByte( timeout );
		if( ! success ) {
			return false;
		}

		char zero = 0x00;
		success = byteStream.writeByte( zero );
		if( ! success ) {
			return false;
		}
		success = byteStream.writeByte( zero );
		if( ! success ) {
			return false;
		}
		success = byteStream.writeByte( zero );
		if( ! success ) {
			return false;
		}

		success = OBJECT_IDENTIFIER::write( value.oid, byteStream );
		if( ! success ) {
			return false;
		}

		success = OCTET_STRING::write( value.description, byteStream );
		if( ! success ) {
			return false;
		}

		return true;
	}
	
	static bool read( value_type& value, ByteStream& byteStream ) {
		char timeout = 0;
		bool success = byteStream.readByte( timeout );
		if( ! success ) {
			return false;
		}

		value.timeout = std::chrono::seconds( (unsigned int)timeout );

		char zero = 0x00;
		success = byteStream.readByte( zero );
		if( ! success ) {
			return false;
		}
		success = byteStream.readByte( zero );
		if( ! success ) {
			return false;
		}
		success = byteStream.readByte( zero );
		if( ! success ) {
			return false;
		}

		success = OBJECT_IDENTIFIER::read( value.oid, byteStream );
		if( ! success ) {
			return false;
		}

		success = OCTET_STRING::read( value.description, byteStream );
		if( ! success ) {
			return false;
		}

		return true;
	}
};

class AgentxClosePdu {
public:
	enum Reason {
		reasonOther = 1,
		reasonParseError = 2,
		reasonProtocolError = 3,
		reasonTimeouts = 4,
		reasonShutdown = 5,
		reasonByManager = 6
	};
public:
	//! This is the reason that the master agent or sub-agent
	//! closed the connection.
	Reason reason;
};

class AGENTX_CLOSE_PDU {
public:
	static constexpr const char* NAME = "agentx-Close-PDU";
	
	//! This class operates on an AgentxClosePdu.
	typedef AgentxClosePdu value_type;
		
	static bool write( const value_type& value, ByteStream& byteStream ) {
		uint8_t reason = (uint8_t)value.reason;
		bool success = byteStream.writeByte( reason );
		if( ! success ) {
			return false;
		}

		char zero = 0x00;
		success = byteStream.writeByte( zero );
		if( ! success ) {
			return false;
		}
		success = byteStream.writeByte( zero );
		if( ! success ) {
			return false;
		}
		success = byteStream.writeByte( zero );
		if( ! success ) {
			return false;
		}

		return true;
	}
	
	static bool read( value_type& value, ByteStream& byteStream ) {
		char reason = 0;
		bool success = byteStream.readByte( reason );
		if( ! success ) {
			return false;
		}

		value.reason = (AgentxClosePdu::Reason)reason;

		char zero = 0x00;
		success = byteStream.readByte( zero );
		if( ! success ) {
			return false;
		}
		success = byteStream.readByte( zero );
		if( ! success ) {
			return false;
		}
		success = byteStream.readByte( zero );
		if( ! success ) {
			return false;
		}

		return true;
	}
};

class AgentxRegisterPdu {
public:
	//! This is the timeout, if any.
	//! If this is not set, then the default will be used by the
	//! master agent.
	std::chrono::seconds timeout;
	//! This is the priority of the registration.
	uint8_t priority;
	//! If this registration represents multiple subtrees, then
	//! this is the 1-index of the number within the "subtree" OID
	//! that will be enumerated (by the master agent) between its
	//! value and that of "upperBound".
	uint8_t rangeSubId;
	//! This is the OID subtree that is being registered.  This may
	//! be either a fully-qualified OID (in which case, "isInstance"
	//! is set to true) or a subtree (in which "isInstance" is set
	//! to false).
	NumericOid subtree;
	//! This is true if "subtree" represents a fully-qualified OID.
	bool isInstance;
	//! This is the upper bound (NOT inclusive) of the number at the
	//! 1-index of "rangeSubId" within "subtree".
	uint32_t upperBound;
};

class AGENTX_REGISTER_PDU {
public:
	static constexpr const char* NAME = "agentx-Register-PDU";
	
	//! This class operates on an AgentxRegisterPdu.
	typedef AgentxRegisterPdu value_type;
		
	static bool write( const value_type& value, ByteStream& byteStream ) {
		uint8_t timeout = value.timeout.count();
		bool success = byteStream.writeByte( timeout );
		if( ! success ) {
			return false;
		}

		uint8_t priority = value.priority;
		success = byteStream.writeByte( priority );
		if( ! success ) {
			return false;
		}

		uint8_t rangeSubId = value.rangeSubId;
		success = byteStream.writeByte( rangeSubId );
		if( ! success ) {
			return false;
		}

		char zero = 0x00;
		success = byteStream.writeByte( zero );
		if( ! success ) {
			return false;
		}

		success = OBJECT_IDENTIFIER::write( value.subtree, byteStream );
		if( ! success ) {
			return false;
		}

		if( rangeSubId ) {
			unsigned int bytesWritten = 0;
			success = byteStream.writeBytes( (const char*)&value.upperBound, sizeof(value.upperBound), bytesWritten );
			if( ! success ) {
				return false;
			}
		}

		return true;
	}
	
	static bool read( value_type& value, ByteStream& byteStream ) {
		char timeout = 0;
		bool success = byteStream.readByte( timeout );
		if( ! success ) {
			return false;
		}

		value.timeout = std::chrono::seconds( (unsigned int)timeout );

		char priority = 0;
		success = byteStream.readByte( priority );
		if( ! success ) {
			return false;
		}
		
		value.priority = priority;

		char rangeSubId = 0;
		success = byteStream.readByte( rangeSubId );
		if( ! success ) {
			return false;
		}

		value.rangeSubId = rangeSubId;
		
		char zero = 0x00;
		success = byteStream.readByte( zero );
		if( ! success ) {
			return false;
		}

		success = OBJECT_IDENTIFIER::read( value.subtree, byteStream );
		if( ! success ) {
			return false;
		}

		if( rangeSubId ) {
			unsigned int bytesRead = 0;
			success = byteStream.readBytes( (char*)&value.upperBound, sizeof(value.upperBound), bytesRead );
			if( ! success ) {
				return false;
			}
		}

		return true;
	}
};

class AgentxGetPdu {
public:
	std::list<NumericOidRange> ranges;
};

class AGENTX_GET_PDU {
public:
	static constexpr const char* NAME = "agentx-Get-PDU";
	
	//! This class operates on an AgentxGetPdu.
	typedef AgentxGetPdu value_type;
		
	static bool write( const value_type& value, ByteStream& byteStream ) {
		for( const auto& range : value.ranges ) {
			bool success = SEARCH_RANGE::write( range, byteStream );
			if( ! success ) {
				return false;
			}
		}

		return true;
	}
	
	static bool read( value_type& value, ByteStream& byteStream ) {
		while( byteStream.remainingReadLength() > 0 ) {
			NumericOidRange range;

			bool success = SEARCH_RANGE::read( range, byteStream );
			if( ! success ) {
				return false;
			}

			value.ranges.push_back( range );
		}
		return true;
	}
};

class AgentxGetNextPdu {
public:
	std::list<NumericOidRange> ranges;
};

class AGENTX_GETNEXT_PDU {
public:
	static constexpr const char* NAME = "agentx-GetNext-PDU";
	
	//! This class operates on an AgentxGetNextPdu.
	typedef AgentxGetNextPdu value_type;
		
	static bool write( const value_type& value, ByteStream& byteStream ) {
		for( const auto& range : value.ranges ) {
			bool success = SEARCH_RANGE::write( range, byteStream );
			if( ! success ) {
				return false;
			}
		}

		return true;
	}
	
	static bool read( value_type& value, ByteStream& byteStream ) {
		while( byteStream.remainingReadLength() > 0 ) {
			NumericOidRange range;

			bool success = SEARCH_RANGE::read( range, byteStream );
			if( ! success ) {
				return false;
			}

			value.ranges.push_back( range );
		}
		return true;
	}
};

class AgentxResponsePdu {
public:
	uint32_t sysUpTime;
	uint16_t error;
	uint16_t index;
	std::vector< std::unique_ptr<VarBind> > varbinds;
};

class AGENTX_RESPONSE_PDU {
public:
	static constexpr const char* NAME = "agentx-Response-PDU";
	
	//! This class operates on an AgentxResponsePdu.
	typedef AgentxResponsePdu value_type;
		
	static bool write( const value_type& value, ByteStream& byteStream ) {
		bool success = true;
		unsigned int bytesWritten = 0;

		success = byteStream.writeBytes( (char*)&value.sysUpTime, sizeof(value.sysUpTime), bytesWritten );
		if( ! success ) {
			return false;
		}
		success = byteStream.writeBytes( (char*)&value.error, sizeof(value.error), bytesWritten );
		if( ! success ) {
			return false;
		}
		success = byteStream.writeBytes( (char*)&value.index, sizeof(value.index), bytesWritten );
		if( ! success ) {
			return false;
		}

		for( const auto& varbind : value.varbinds ) {
			bool success = VARBIND::write( *(varbind.get()), byteStream );
			if( ! success ) {
				return false;
			}
		}

		return true;
	}
	
	static bool read( value_type& value, ByteStream& byteStream ) {
		bool success = true;
		unsigned int bytesRead = 0;

		success = byteStream.readBytes( (char*)&value.sysUpTime, sizeof(value.sysUpTime), bytesRead );
		if( ! success ) {
			return false;
		}
		success = byteStream.readBytes( (char*)&value.error, sizeof(value.error), bytesRead );
		if( ! success ) {
			return false;
		}
		success = byteStream.readBytes( (char*)&value.index, sizeof(value.index), bytesRead );
		if( ! success ) {
			return false;
		}

		while( byteStream.remainingReadLength() > 0 ) {
			std::unique_ptr<VarBind> varbind( new VarBind() );

			bool success = VARBIND::read( *(varbind.get()), byteStream );
			if( ! success ) {
				return false;
			}

			value.varbinds.push_back( std::move(varbind) );
		}

		return true;
	}
};

class AgentxPingPdu {
public:
};

class AGENTX_PING_PDU {
public:
	static constexpr const char* NAME = "agentx-Ping-PDU";
	
	//! This class operates on an AgentxPingPdu.
	typedef AgentxPingPdu value_type;
		
	static bool write( const value_type& value, ByteStream& byteStream ) {
		return true;
	}
	
	static bool read( value_type& value, ByteStream& byteStream ) {
		return true;
	}
};

class AgentxAddAgentCapsPdu {
public:
	NumericOid oid;
	std::string description;
};

class AGENTX_ADDAGENTCAPS_PDU {
public:
	static constexpr const char* NAME = "agentx-AddAgentCaps-PDU";
	
	//! This class operates on an AgentxAddAgentCapsPdu.
	typedef AgentxAddAgentCapsPdu value_type;
		
	static bool write( const value_type& value, ByteStream& byteStream ) {
		bool success = true;

		success = OBJECT_IDENTIFIER::write( value.oid, byteStream );
		if( ! success ) {
			return false;
		}

		success = OCTET_STRING::write( value.description, byteStream );
		if( ! success ) {
			return false;
		}

		return true;
	}
	
	static bool read( value_type& value, ByteStream& byteStream ) {
		bool success = true;

		success = OBJECT_IDENTIFIER::read( value.oid, byteStream );
		if( ! success ) {
			return false;
		}

		success = OCTET_STRING::read( value.description, byteStream );
		if( ! success ) {
			return false;
		}

		return true;
	}
};

class AgentxRemoveAgentCapsPdu {
public:
	NumericOid oid;
};

class AGENTX_REMOVEAGENTCAPS_PDU {
public:
	static constexpr const char* NAME = "agentx-RemoveAgentCaps-PDU";
	
	//! This class operates on an AgentxRemoveAgentCapsPdu.
	typedef AgentxRemoveAgentCapsPdu value_type;
		
	static bool write( const value_type& value, ByteStream& byteStream ) {
		bool success = true;

		success = OBJECT_IDENTIFIER::write( value.oid, byteStream );
		if( ! success ) {
			return false;
		}

		return true;
	}
	
	static bool read( value_type& value, ByteStream& byteStream ) {
		bool success = true;

		success = OBJECT_IDENTIFIER::read( value.oid, byteStream );
		if( ! success ) {
			return false;
		}

		return true;
	}
};

} } } }

