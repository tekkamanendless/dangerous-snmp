#include "dangerous/snmp/agentx.hpp"

#include "dangerous/snmp/logger.hpp"
#include "dangerous/snmp/pdu.hpp"

#include "encoding/agentx/header.hpp"
#include "encoding/agentx/pdu.hpp"
#include "transport/tcp.hpp"
#include "transport/udp.hpp"

#include "context-private.hpp"

namespace dangerous { namespace snmp {

class AgentxPacket {
public:
	encoding::agentx::Header header;
	std::string context;
	ByteStream pduStream;
};


//! This is the default value for the "priority" of an OID registered with
//! the "Register" PDU.  This is defined in RFC-2741.
const uint8_t AgentX::DEFAULT_PRIORITY = 127;


/**
 * Constructor.
 *
 * @param context The Dangerous SNMP Context to use.
 **/
AgentX::AgentX( const Context& context ) :
	_identifier( NumericOid(".0.0") ),
	_description( "" ),
	transport( nullptr ),
	packetNumber( 0 )
{
	this->context = context.privateData;

	isStopped = true; //< We can't start the thread unless it is flagged as stopped.

	sessionId = 0;
}

/**
 * Destructor.
 *
 * If the "mailbox" thread is running, then it is stopped.
 **/
AgentX::~AgentX() {
	stopMailboxThread();

	if( logger.system( Logger::AGENTX ) ) {
		logger.out() << "AgentX::" << __FUNCTION__ << ": Destructor complete." << std::endl;
	}
}

/**
 * This sets the URI for the client.  A URI is of the form:
 *     {transport}://{address}
 *
 * Currently, the following transports are supported:
 * 
 * * udp
 *    * The address is {ip address or hostname}[:{port number or name}]
 * * tcp
 *    * The address is {ip address or hostname}[:{port number or name}]
 *
 * When this is called, the transport is created, and a "mailbox" is created
 * and started for the agent.
 * 
 * @param uri The URI to use.
 * @throw ParseErrorException, UnknownTransportException, Exception
 **/
void AgentX::uri( const std::string& uri ) throw( ParseErrorException, UnknownTransportException, Exception ) {
	// Our URI must be of the form:
	//    <transport>://<transport-specfic string>
	//
	// So, we must first determine what the transport is.  If there is no
	// transport specified, then we must fail.
	size_t position = uri.find( "://" );
	if( position == std::string::npos ) {
		throw ParseErrorException( "Could not find the transport string." );
	}

	// Extract the transport string.
	std::string transportString = uri.substr( 0, position );
	if( logger.system( Logger::AGENTX ) ) {
		logger.out() << "AgentX::" << __FUNCTION__ << ": Transport string: " << transportString << std::endl;
	}

	// Extract the transport-specific connection string.
	std::string connectionString = uri.substr( position + 3 );

	if( transportString.compare( "udp" ) == 0 ) {
		this->transport = std::unique_ptr<Transport>( new transport::Udp( context, connectionString ) );
	} else if( transportString.compare( "tcp" ) == 0 ) {
		this->transport = std::unique_ptr<Transport>( new transport::Tcp( context, connectionString ) );
	} else {
		throw UnknownTransportException( "Unknown transport: " + transportString );
	}


	// Start the mailbox thread, since we will now be requiring I/O.
	startMailboxThread();
}

/**
 * This connects the agent to the master agent.  The identifier OID
 * and description are sent to the master agent, and the "Open" command
 * is issued.
 *
 * A session ID is obtained from the master agent, and any pending communication
 * with it is voided.
 **/
void AgentX::open() {
	if( logger.system( Logger::AGENTX ) ) {
		logger.out() << "AgentX::" << __FUNCTION__ << ": Opening agent." << std::endl;
	}

	//! This is the "Open" PDU that we will send to the master agent.
	encoding::agentx::AgentxOpenPdu openPdu;
	openPdu.timeout = std::chrono::seconds( 5 ); //!< TODO: Why 5 seconds?
	openPdu.oid = identifier();
	openPdu.description = description();

	//! This is the packet that will contain our "Open" PDU.
	AgentxPacket requestPacket;
	requestPacket.header.type = encoding::agentx::PDU::AGENTX_OPEN_PDU;
	bool success = encoding::agentx::AGENTX_OPEN_PDU::write( openPdu, requestPacket.pduStream );
	if( ! success ) {
		throw Exception( "Could not encode the agentx-Open-PDU." );
	}


	// Since we will be establishing a new session, any items in the inbox
	// are now void.  Here, we will be purging them.
	std::unique_lock<std::mutex> readLock( readMutex );
			
	if( logger.system( Logger::AGENTX ) ) {
		logger.out() << "AgentX::" << __FUNCTION__ << ": Flushing the inbox." << std::endl;
	}
	inbox.clear();

	readLock.unlock();


	// Reset the session ID.
	sessionId = 0;
	// Send the "Open" PDU.
	rawWrite( requestPacket );
	
	//! This is the response from the "Open" PDU.
	AgentxPacket responsePacket;
	// Read the response from the master agent.
	mailboxRead( responsePacket, requestPacket.header.packetId );

	// The master agent will set our new session ID in its response.
	// Set our session ID accordingly.
	sessionId = responsePacket.header.sessionId;
}

/**
 * This disconnects the agent from the master agent.
 **/
void AgentX::close() {
	if( logger.system( Logger::AGENTX ) ) {
		logger.out() << "AgentX::" << __FUNCTION__ << ": Closing agent." << std::endl;
	}

	//! This is the "Close" PDU.
	encoding::agentx::AgentxClosePdu closePdu;
	closePdu.reason = encoding::agentx::AgentxClosePdu::reasonShutdown;

	//! This is the packet that will contain our "Close" PDU.
	AgentxPacket requestPacket;
	requestPacket.header.type = encoding::agentx::PDU::AGENTX_CLOSE_PDU;
	bool success = encoding::agentx::AGENTX_CLOSE_PDU::write( closePdu, requestPacket.pduStream );
	if( ! success ) {
		throw Exception( "Could not encode the agentx-Close-PDU." );
	}

	// Send the "Close" PDU.
	rawWrite( requestPacket );

	//! This is the response from the "Close" PDU.
	AgentxPacket responsePacket;
	// Read teh response from the master agent.
	mailboxRead( responsePacket, requestPacket.header.packetId );

	// Invalidate our session ID, since we are not disconnected.
	sessionId = 0;
}

/*
 * Raw protocol operations.
 */

/**
 * This sends an "AddAgentCaps" PDU to the master agent.
 * This PDU tells the master agent about a "capability" of this agent.
 * This information will be registered under "sysORID" and "sysORDescr".
 *
 * @param oid The OID that represents the capability.
 * @param description The description.
 **/
void AgentX::raw_addAgentCapabilities( const NumericOid& oid, const std::string& description ) {
	if( logger.system( Logger::AGENTX ) ) {
		logger.out() << "AgentX::" << __FUNCTION__ << ": Adding capability '" << oid.str() << "': '" << description << "'." << std::endl;
	}

	encoding::agentx::AgentxAddAgentCapsPdu requestPdu;
	requestPdu.oid = oid;
	requestPdu.description = description;

	AgentxPacket requestPacket;
	requestPacket.header.type = encoding::agentx::PDU::AGENTX_ADDAGENTCAPS_PDU;
	bool success = encoding::agentx::AGENTX_ADDAGENTCAPS_PDU::write( requestPdu, requestPacket.pduStream );
	if( ! success ) {
		throw Exception( "Could not encode the agentx-AddAgentCaps-PDU." );
	}

	try {
		rawWrite( requestPacket );

		AgentxPacket responsePacket;
		mailboxRead( responsePacket, requestPacket.header.packetId );
	} catch( Exception& e ) {
		if( transport != nullptr ) {
			transport->disconnect();
		}
	}
}

/**
 * This sends a "RemoveAgentCaps" PDU to the master agent.
 * This PDU tells the master agent that this agent no longer has
 * the capability previously registered.
 *
 * @param oid The OID that represents the capability.
 **/
void AgentX::raw_removeAgentCapabilities( const NumericOid& oid ) {
	if( logger.system( Logger::AGENTX ) ) {
		logger.out() << "AgentX::" << __FUNCTION__ << ": Removing capability '" << oid.str() << "'." << std::endl;
	}

	encoding::agentx::AgentxRemoveAgentCapsPdu requestPdu;
	requestPdu.oid = oid;

	AgentxPacket requestPacket;
	requestPacket.header.type = encoding::agentx::PDU::AGENTX_REMOVEAGENTCAPS_PDU;
	bool success = encoding::agentx::AGENTX_REMOVEAGENTCAPS_PDU::write( requestPdu, requestPacket.pduStream );
	if( ! success ) {
		throw Exception( "Could not encode the agentx-RemoveAgentCaps-PDU." );
	}

	try {
		rawWrite( requestPacket );

		AgentxPacket responsePacket;
		mailboxRead( responsePacket, requestPacket.header.packetId );
	} catch( Exception& e ) {
		if( transport != nullptr ) {
			transport->disconnect();
		}
	}
}

/**
 * This sends a "Register" PDU to the master agent.
 * This PDU tells the master agent that this agent is responsible
 * for any OIDs under the given OID, including that OID.
 *
 * @param oid The OID to register.
 * @param priority The priority of the registration.  A lower value has a higher priority.
 **/
void AgentX::raw_registerTree( const NumericOid& oid, uint8_t priority ) {
	if( logger.system( Logger::AGENTX ) ) {
		logger.out() << "AgentX::" << __FUNCTION__ << ": Registering OID tree '" << oid.str() << "'." << std::endl;
	}

	encoding::agentx::AgentxRegisterPdu registerPdu;
	registerPdu.timeout = std::chrono::seconds( 0 );
	registerPdu.priority = priority;
	registerPdu.rangeSubId = 0; //!< This represents an tree, so no sub-identifier range here.
	registerPdu.subtree = oid;
	registerPdu.upperBound = 0; //!< This represents an tree, so no sub-identifier range here.

	AgentxPacket requestPacket;
	requestPacket.header.type = encoding::agentx::PDU::AGENTX_REGISTER_PDU;
	bool success = encoding::agentx::AGENTX_REGISTER_PDU::write( registerPdu, requestPacket.pduStream );
	if( ! success ) {
		throw Exception( "Could not encode the agentx-Register-PDU." );
	}
	
	try {
		rawWrite( requestPacket );

		AgentxPacket responsePacket;
		mailboxRead( responsePacket, requestPacket.header.packetId );
	} catch( Exception& e ) {
		if( transport != nullptr ) {
			transport->disconnect();
		}
	}
}

/**
 * This sends a "Register" PDU to the master agent with a flag
 * signifying "instance" registration.
 * This PDU tells the master agent that this agent is responsible
 * for this exact OID.
 *
 * @param oid The OID to register.
 * @param priority The priority of the registration.  A lower value has a higher priority.
 **/
void AgentX::raw_registerOid( const NumericOid& oid, uint8_t priority ) {
	if( logger.system( Logger::AGENTX ) ) {
		logger.out() << "AgentX::" << __FUNCTION__ << ": Registering full OID '" << oid.str() << "'." << std::endl;
	}

	encoding::agentx::AgentxRegisterPdu registerPdu;
	registerPdu.timeout = std::chrono::seconds( 0 );
	registerPdu.priority = priority;
	registerPdu.rangeSubId = 0; //< This represents an instance, so no sub-identifier range here.
	registerPdu.subtree = oid;
	registerPdu.upperBound = 0; //< This represents an instance, so no sub-identifier range here.

	AgentxPacket requestPacket;
	requestPacket.header.type = encoding::agentx::PDU::AGENTX_REGISTER_PDU;
	requestPacket.header.flags = encoding::agentx::Header::INSTANCE_REGISTRATION;
	bool success = encoding::agentx::AGENTX_REGISTER_PDU::write( registerPdu, requestPacket.pduStream );
	if( ! success ) {
		throw Exception( "Could not encode the agentx-Register-PDU." );
	}

	try {
		rawWrite( requestPacket );

		AgentxPacket responsePacket;
		mailboxRead( responsePacket, requestPacket.header.packetId );
	} catch( Exception& e ) {
		if( transport != nullptr ) {
			transport->disconnect();
		}
	}
}


/*
 * Logical protocol operations.
 */

/**
 * This adds a new capability for the agent.
 * If this capability has already been registered, then
 * an exception will be thrown.
 *
 * @param oid The OID that represents the capability.
 * @param description The description of the capability.
 **/
void AgentX::addAgentCapabilities( const NumericOid& oid, const std::string& description ) throw( Exception ) {
	if( logger.system( Logger::AGENTX ) ) {
		logger.out() << "AgentX::" << __FUNCTION__ << ": Adding capability '" << oid.str() << "': '" << description << "'." << std::endl;
	}

	// If this capability is already registered, then throw an exception.
	if( capabilityMap.find( oid ) != capabilityMap.end() ) {
		throw Exception( "Capability '" + oid.str() + "' is already registered." );
	}

	// Update the capability map.
	capabilityMap[ oid ] = description;

	// Actually register the capability.
	raw_addAgentCapabilities( oid, description );
}

/**
 * This removes a previously registered capability.
 * If this capability has not already been registered, then
 * an exception will be thrown.
 *
 * @param oid The OID that represents the capability.
 * @throw Exception
 **/
void AgentX::removeAgentCapabilities( const NumericOid& oid ) throw( Exception ) {
	if( logger.system( Logger::AGENTX ) ) {
		logger.out() << "AgentX::" << __FUNCTION__ << ": Removing capability '" << oid.str() << "'." << std::endl;
	}

	// If this capability is not already registered, then throw an exception.
	if( capabilityMap.find( oid ) == capabilityMap.end() ) {
		throw Exception( "Capability '" + oid.str() + "' is not yet registered." );
	}

	// Update the capability map.
	capabilityMap.erase( oid );

	// Actually remove the capability.
	raw_removeAgentCapabilities( oid );
}

/**
 * This registers a new OID.
 *
 * @param oid The OID to register.
 * @param entry The registration information.
 * @throw Exception
 **/
void AgentX::registerOid( const NumericOid& oid, const AgentX::OidRegistrationEntry& entry ) throw( Exception ) {
	if( logger.system( Logger::AGENTX ) ) {
		logger.out() << "AgentX::" << __FUNCTION__ << ": Attempting to register OID '" << oid.str() << "'." << std::endl;
	}

	// Check to see if we've already registered.
	auto iterator = registrationThatCovers( oid );
	if( iterator != oidRegistrationMap.end() ) {
		switch( iterator->second.type ) {
			case OidRegistrationEntry::OBJECT:
				throw Exception( "Object already registered." );
			case OidRegistrationEntry::INSTANCE:
				throw Exception( "Instance already registered." );
			case OidRegistrationEntry::TREE:
				throw Exception( "OID already covered by a tree." );
		}
	}


	OidRegistrationEntry newEntry = entry;
	if( newEntry.priority == 0 ) {
		newEntry.priority = DEFAULT_PRIORITY;
	}

	oidRegistrationMap[ oid ] = newEntry;

	switch( newEntry.type ) {
		case OidRegistrationEntry::INSTANCE: // TODO: ???
			raw_registerOid( oid, newEntry.priority );
			break;
		case OidRegistrationEntry::OBJECT: // TODO: ???
			raw_registerOid( oid, newEntry.priority );
			break;
		case OidRegistrationEntry::TREE: // TODO: ???
			raw_registerTree( oid, newEntry.priority );
			break;
	}
}

/**
 * This returns the "oidRegistrationMap" iterator whose entry would be responsible
 * for the OID given.
 *
 * If the responsible entry is an OBJECT or INSTANCE registration, then its OID
 * will match the one given exactly.
 *
 * If the responsible entry is a TREE registration, then its OID will be the
 * beginning of the one given.
 *
 * @param oid The OID to search for.
 * @return The iterator to the entry in "oidRegistrationMap", or "oidRegistrationMap.end()" if it is not covered.
 **/
AgentX::OidRegistrationMap::const_iterator AgentX::registrationThatCovers( const NumericOid& oid ) {
	if( logger.system( Logger::AGENTX ) ) {
		logger.out() << "AgentX::" << __FUNCTION__ << ": Looking to see if OID '" << oid.str() << "' is covered." << std::endl;
	}

	//! This is the iterator to the OID closest to the one that we've been given.
	//! It has two possibilities:
	//! 1. It is the exact OID in question.
	//! 2. It is the OID that would come next, lexicographically.
	//!    This could mean that it's the "end" of the map, which is fine.
	//!    It could also be the "begin" of the map, which means that we CANNOT decrement
	//!    the iterator.
	auto iterator = oidRegistrationMap.lower_bound( oid );

	// If we found an entry, then see if it matches ours.
	if( iterator != oidRegistrationMap.end() ) {
		if( logger.system( Logger::AGENTX ) ) {
			logger.out() << "AgentX::" << __FUNCTION__ << ": Lower bound is '" << iterator->first.str() << "'." << std::endl;
		}
		if( oid.equals( iterator->first ) ) {
			if( logger.system( Logger::AGENTX ) ) {
				logger.out() << "AgentX::" << __FUNCTION__ << ": There is a direct match for OID '" << oid.str() << "'." << std::endl;
			}
			return iterator;
		}
		if( logger.system( Logger::AGENTX ) ) {
			logger.out() << "AgentX::" << __FUNCTION__ << ": No direct match for OID '" << oid.str() << "'." << std::endl;
		}
	}

	// If we found an entry that comes after another entry, then see if ours
	// is under that previous entry.
	if( iterator != oidRegistrationMap.begin() ) {
		if( logger.system( Logger::AGENTX ) ) {
			logger.out() << "AgentX::" << __FUNCTION__ << ": Decrementing the iterator." << std::endl;
		}

		// Go back one.
		iterator--;

		if( logger.system( Logger::AGENTX ) ) {
			logger.out() << "AgentX::" << __FUNCTION__ << ": New (decremented) iterator is OID '" << iterator->first.str() << "'." << std::endl;
		}
		if( iterator->second.type == OidRegistrationEntry::TREE && oid.beginsWith( iterator->first ) ) {
			if( logger.system( Logger::AGENTX ) ) {
				logger.out() << "AgentX::" << __FUNCTION__ << ": There is tree coverage by OID '" << iterator->first.str() << "'." << std::endl;
			}
			return iterator;
		}
		if( logger.system( Logger::AGENTX ) ) {
			logger.out() << "AgentX::" << __FUNCTION__ << ": No tree coverage for OID '" << oid.str() << "'." << std::endl;
		}
	}

	if( logger.system( Logger::AGENTX ) ) {
		logger.out() << "AgentX::" << __FUNCTION__ << ": OID '" << oid.str() << "' is not currently covered." << std::endl;
	}

	return oidRegistrationMap.end();
}


/**
 * This reads an AgentxPacket from the transport.
 *
 * @param packet The packet in which to store the data.
 **/
void AgentX::rawRead( AgentxPacket& packet ) {
	if( ! transport || ! transport->isConnected() ) {
		if( logger.system( Logger::AGENTX ) ) {
			logger.out() << "AgentX::" << __FUNCTION__ << ": (Not connected; not reading)." << std::endl;
		}
		throw Exception( "Not currently connected." );
	}

	bool success = encoding::agentx::HEADER::read( packet.header, *transport->readerStream );
	if( ! success ) {
		if( transport->readerStream->lastReadTimedOut() ) {
			throw TimeoutException( "Timed out reading the packet header." );
		}
		throw Exception( "Could not decode the PDU header." );
	}

	std::unique_ptr<char[]> buffer( new char[ packet.header.payloadLength ] );

	unsigned int bytesRead = 0;
	success = transport->readerStream->readBytes( buffer.get(), packet.header.payloadLength, bytesRead );
	if( ! success ) {
		throw Exception( "Could not read the PDU payload." );
	}

	unsigned int bytesWritten = 0;
	success = packet.pduStream.writeBytes( buffer.get(), bytesRead, bytesWritten );
	if( ! success ) {
		throw Exception( "Could not copy the PDU payload." );
	}

	if( ( packet.header.flags & encoding::agentx::Header::NON_DEFAULT_CONTEXT ) == encoding::agentx::Header::NON_DEFAULT_CONTEXT ) {
		success = encoding::agentx::OCTET_STRING::read( packet.context, packet.pduStream );
		if( ! success ) {
			throw Exception( "Could not read the context name." );
		}
	}
}

/**
 * This writes an AgentxPacket to the transport.
 *
 * @param packet The packet to write.
 **/
void AgentX::rawWrite( AgentxPacket& packet ) {
	if( ! transport || ! transport->isConnected() ) {
		if( logger.system( Logger::AGENTX ) ) {
			logger.out() << "AgentX::" << __FUNCTION__ << ": (Not connected; not writing)." << std::endl;
		}
		throw Exception( "Not currently connected." );
	}

	if( logger.system( Logger::AGENTX ) ) {
		logger.out() << "AgentX::" << __FUNCTION__ << ": Locking writeMutex." << std::endl;
	}
	// This lock instance will _remain_ locked until it goes out of scope; thus,
	// from here foreward, we can be assured that we are the only thread currently
	// accessing this mutex right now.
	std::unique_lock<std::mutex> writeLock( writeMutex );

	packet.header.version = encoding::agentx::Header::VERSION_1;
	
	packet.header.sessionId = sessionId;
	// If this _is_ a Response PDU, then we have already filled out the relevant
	// fields in the header to our specification.
	//
	// Otherwise, we will need to update the transaction ID and packet ID.
	if( packet.header.type == encoding::agentx::PDU::AGENTX_RESPONSE_PDU ) {
		// (header is already filled out.)
	} else {
		packet.header.transactionId = 0; //< TODO: Internal variable
		packet.header.packetId = ++packetNumber;
	}

	bool success = true;
	ByteStream contextStream;
	if( ( packet.header.flags & encoding::agentx::Header::NON_DEFAULT_CONTEXT ) == encoding::agentx::Header::NON_DEFAULT_CONTEXT ) {
		success = encoding::agentx::OCTET_STRING::write( packet.context, contextStream );
		if( ! success ) {
			throw Exception( "Could not write the context name." );
		}
	}

	packet.header.payloadLength = contextStream.remainingReadLength() + packet.pduStream.remainingReadLength();

	success = encoding::agentx::HEADER::write( packet.header, *transport->writerStream );
	if( ! success ) {
		throw Exception( "Could not encode the PDU header." );
	}

	unsigned int bytesWritten = 0;
	std::vector<char> buffer;
	contextStream.copyTo( buffer );
	success = transport->writerStream->writeBytes( buffer.data(), buffer.size(), bytesWritten );
	if( ! success ) {
		throw Exception( "Could not add the PDU payload." );
	}

	packet.pduStream.copyTo( buffer );
	success = transport->writerStream->writeBytes( buffer.data(), buffer.size(), bytesWritten );
	if( ! success ) {
		throw Exception( "Could not add the PDU payload." );
	}

	transport->commitWrite();
}

/**
 * This starts the mailbox thread, if it has not already been started.
 **/
void AgentX::startMailboxThread() {
	if( ! isStopped ) {
		return;
	}

	isStopped = false;
	mailboxThread = std::thread( std::bind( &AgentX::mailboxMain, this ) );
}

/**
 * This stops the mailbox thread, if it has not already been stopped.
 **/
void AgentX::stopMailboxThread() {
	if( isStopped ) {
		return;
	}

	isStopped = true;
	mailboxThread.join();
}

/**
 * This attempts to obtain the packet, with the specified ID, from the
 * mailbox.  If there is no such packet, then this will wait for at
 * most 5 seconds before failing.
 *
 * Exception conditions:
 * * The transport is not connected.
 * * The packet could not be found within the time limit.
 * 
 * @param packet The packet to store the data from the mailbox.
 * @param packetId The ID of the packet to retrieve.
 * @throw Exception
 **/
void AgentX::mailboxRead( AgentxPacket& packet, uint32_t packetId ) throw( Exception ) {
	if( ! transport->isConnected() ) {
		if( logger.system( Logger::AGENTX ) ) {
			logger.out() << "AgentX::" << __FUNCTION__ << ": (Not connected; not reading)." << std::endl;
		}
		throw Exception( "Not currently connected." );
	}

	if( logger.system( Logger::AGENTX ) ) {
		logger.out() << "AgentX::" << __FUNCTION__ << ": Locking readMutex." << std::endl;
	}
	// This lock instance will _remain_ locked until it goes out of scope; thus,
	// from here foreward, we can be assured that we are the only thread currently
	// accessing this mutex right now.
	std::unique_lock<std::mutex> readLock( readMutex );

	if( logger.system( Logger::AGENTX ) ) {
		logger.out() << "AgentX::" << __FUNCTION__ << ": Waiting for an entry for packet '" << packetId << "'." << std::endl;
	}
	std::chrono::seconds timeout( 5 ); //!< TODO: CONFIGURABLE?
	bool success = readConditionVariable.wait_for(
		readLock,
		timeout,
		[ this, &packet, packetId ]() {
			// See if we have a packet in our inbox that matches the packet ID
			// that we have been given.
			auto it = this->inbox.find( packetId );
			// If we do NOT have such a packet (yet), then return false to signify
			// that we haven't accomplished our task yet.
			if( it == this->inbox.end() ) {
				return false;
			}

			// We found our packet.

			std::unique_ptr<AgentxPacket> inboxPacket = std::move( it->second );
			this->inbox.erase( it );
			packet.header = inboxPacket->header;
			packet.context = inboxPacket->context;
			packet.pduStream.copyFrom( inboxPacket->pduStream, ByteStream::READ_ONLY );

			return true;
		}
	);
	
	if( ! success ) {
		throw Exception( "Timed out waiting for the packet from the mailbox." );
	}
}

/**
 * This is the main loop for the mailbox thread.
 **/
void AgentX::mailboxMain() {
	if( logger.system( Logger::AGENTX ) ) {
		logger.out() << "AgentX::" << __FUNCTION__ << ": Starting." << std::endl;
	}

	bool openInProgress = false;
	while( ! isStopped ) {
		// If we have a valid transport, then we are first going to check to make
		// sure that it is connected.  If it isn't connected, then we're going to
		// try to reconnect.
		if( transport != nullptr ) {
			if( ! transport->isConnected() ) {
				if( logger.system( Logger::AGENTX ) ) {
					logger.out() << "AgentX::" << __FUNCTION__ << ": The transport is not connected." << std::endl;
				}

				if( sessionId != 0 ) {
					if( logger.system( Logger::AGENTX ) ) {
						logger.out() << "AgentX::" << __FUNCTION__ << ": Attempting to disconnect." << std::endl;
					}

					try {
						close();
					} catch( Exception& e ) {
						if( logger.system( Logger::AGENTX ) ) {
							logger.out() << "AgentX::" << __FUNCTION__ << ": Could not disconnect: " << e.what() << std::endl;
						}
					}

					sessionId = 0;
				}
					
				if( logger.system( Logger::AGENTX ) ) {
					logger.out() << "AgentX::" << __FUNCTION__ << ": Attempting to reconnect." << std::endl;
				}

				// Attempt to reconnect the transport.
				transport->connect();

				if( ! transport->isConnected() ) {
					std::chrono::seconds waitTime( 5 );

					if( logger.system( Logger::AGENTX ) ) {
						logger.out() << "AgentX::" << __FUNCTION__ << ": The transport could not reconnect." << std::endl;
						logger.out() << "AgentX::" << __FUNCTION__ << ": Sleeping for " << waitTime.count() << " seconds." << std::endl;
					}

					std::this_thread::sleep_for( waitTime );
					continue;
				}
			}

			if( sessionId == 0 ) {
				if( logger.system( Logger::AGENTX ) ) {
					logger.out() << "AgentX::" << __FUNCTION__ << ": No session is currently open." << std::endl;
				}

				if( ! openInProgress ) {
					if( logger.system( Logger::AGENTX ) ) {
						logger.out() << "AgentX::" << __FUNCTION__ << ": No open is currently in progress." << std::endl;
					}

					openInProgress = true;
					std::thread openThread(
						[ this, &openInProgress ]() {
							// Re-establish the session (via "Open").
							try {
								open();
							} catch( Exception& e ) {
								if( logger.system( Logger::AGENTX ) ) {
									logger.out() << "AgentX::" << __FUNCTION__ << ": Could not re-open session: " << e.what() << std::endl;
								}
								
								return;
							}

							// Re-register capabilities.
							try {
								for( const auto& entry : capabilityMap ) {
									raw_addAgentCapabilities( entry.first, entry.second );
								}
							} catch( Exception& e ) {
								if( logger.system( Logger::AGENTX ) ) {
									logger.out() << "AgentX::" << __FUNCTION__ << ": Could not re-add capability: " << e.what() << std::endl;
								}
								
								return;
							}
							
							// TODO: Re-register OIDs.
							try {
								for( const auto& iterator : oidRegistrationMap ) {
									switch( iterator.second.type ) {
										case OidRegistrationEntry::INSTANCE:
											raw_registerOid( iterator.first, iterator.second.priority );
											break;
										case OidRegistrationEntry::OBJECT:
											raw_registerOid( iterator.first, iterator.second.priority );
											break;
										case OidRegistrationEntry::TREE:
											raw_registerTree( iterator.first, iterator.second.priority );
											break;
									}
								}
							} catch( Exception& e ) {
								if( logger.system( Logger::AGENTX ) ) {
									logger.out() << "AgentX::" << __FUNCTION__ << ": Could not re-register OID: " << e.what() << std::endl;
								}
								
								return;
							}

							openInProgress = false;
						}
					);
					openThread.detach();
				}
			}
		}


		//! This is the packet that we're going to be attempting to read from the
		//! transport.
		std::unique_ptr<AgentxPacket> packet( new AgentxPacket );
		
		if( logger.system( Logger::AGENTX ) ) {
			logger.out() << "AgentX::" << __FUNCTION__ << ": Attempting to read a packet." << std::endl;
		}
		try {
			// Attempt to read the packet.
			rawRead( *(packet.get()) );
		} catch( TimeoutException& e ) {
			// A timeout here is _perfectly normal_; this just means that no new packets
			// came in during this period of time.  In this event, we will simply continue
			// and try again.
			if( logger.system( Logger::AGENTX ) ) {
				logger.out() << "AgentX::" << __FUNCTION__ << ": (Timeout)" << std::endl;
			}
			continue;
		} catch( ... ) {
			// Any _other_ kind of exception is unexpected.
			
			if( logger.system( Logger::AGENTX ) ) {
				logger.out() << "AgentX::" << __FUNCTION__ << ": Unexpected exception (unknown type)." << std::endl;
				logger.out() << "AgentX::" << __FUNCTION__ << ": Disconnecting." << std::endl;
			}

			transport->disconnect();
			continue;
		}
		if( logger.system( Logger::AGENTX ) ) {
			logger.out() << "AgentX::" << __FUNCTION__ << ": Read a single packet." << std::endl;
		}

		// At this point, we have obtained a full packet from the underlying transport.
		// If it is a _response_ PDU, then it is in direct response to an administrative
		// request that we have already sent.  So, we're going to add it to our inbox
		// so that the thread that actually sent the request can also retrieve the response.
		//
		// If it is anything else, then it is a request coming _from_ the master agent,
		// and we will need to handle it.

		if( packet->header.type == encoding::agentx::PDU::AGENTX_RESPONSE_PDU ) {
			std::unique_lock<std::mutex> readLock( readMutex );
			
			if( logger.system( Logger::AGENTX ) ) {
				logger.out() << "AgentX::" << __FUNCTION__ << ": Adding packet '" << (int)packet->header.packetId << "' to the inbox." << std::endl;
			}

			inbox[ packet->header.packetId ] = std::move( packet );
			readConditionVariable.notify_all();
		} else {
			// We have been asked to _respond_ to some specific request.  Here, we
			// are going to construct the response packet and fill it out to its
			// minimal extent.
			//
			// Then, we'll be handling the different kinds of requests individually.
			// If we run into problems, then we'll send back an error.

			//! This is our response packet.
			AgentxPacket responsePacket;
			responsePacket.header.type = encoding::agentx::PDU::AGENTX_RESPONSE_PDU;
			responsePacket.header.transactionId = packet->header.transactionId;
			responsePacket.header.packetId = packet->header.packetId;

			//! This is our response PDU.
			encoding::agentx::AgentxResponsePdu responsePdu;
			responsePdu.sysUpTime = 0; //< This is ignored for sub-agents.
			responsePdu.error = encoding::agentx::PDUError::noAgentXError; //< Assume no error for now.
			responsePdu.index = 0; //< (No error, so no index is filled out.)

			// Detect the error-condition when the packet is not meant for our
			// session.
			if( packet->header.sessionId != sessionId ) {
				if( sessionId == 0 ) {
					// TODO: SHOULDN'T REALLY MATTER SINCE WE'RE ESTABLISHING SESSION
				} else {
					if( logger.system( Logger::AGENTX ) ) {
						logger.out() << "AgentX::" << __FUNCTION__ << ": Incorrect session ID.  Got " << packet->header.sessionId << "; expected " << sessionId << "." << std::endl;
					}

					responsePdu.error = encoding::agentx::PDUError::notOpen;
				}
			}

			// If there is already an error, then we're not going to do any more work.
			// If there isn't an error yet, then proceed normally with processing.
			if( responsePdu.error == encoding::agentx::PDUError::noAgentXError ) {
				switch( packet->header.type ) {
					case encoding::agentx::PDU::AGENTX_GET_PDU: {
						if( logger.system( Logger::AGENTX ) ) {
							logger.out() << "AgentX::" << __FUNCTION__ << ": Received a 'Get' PDU." << std::endl;
						}

						encoding::agentx::AgentxGetPdu pdu;
						encoding::agentx::AGENTX_GET_PDU::read( pdu, packet->pduStream );

						if( logger.system( Logger::AGENTX ) ) {
							logger.out() << "AgentX::" << __FUNCTION__ << ": 'Get' PDU (" << pdu.ranges.size() << " ranges):" << std::endl;
							for( const auto& range : pdu.ranges ) {
								logger.out() << "AgentX::" << __FUNCTION__ << ":    " <<
									"OID " << range.start.str() << std::endl;
							}
						}

						for( const auto& range : pdu.ranges ) {
							std::unique_ptr<VarBind> varbind( new VarBind() );

							varbind->oid = range.start;

							const auto& oid = range.start;
							
							if( logger.system( Logger::AGENTX ) ) {
								logger.out() << "AgentX::" << __FUNCTION__ << ": Looking for the registration that covers '" << oid.str() << "'." << std::endl;
							}
							auto iterator = registrationThatCovers( oid );
							
							bool success = false;
							if( iterator == oidRegistrationMap.end() ) {
								if( logger.system( Logger::AGENTX ) ) {
									logger.out() << "AgentX::" << __FUNCTION__ << ": No coverage for OID '" << oid.str() << "'." << std::endl;
								}
							} else {
								if( logger.system( Logger::AGENTX ) ) {
									logger.out() << "AgentX::" << __FUNCTION__ << ": OID '" << oid.str() << "' is covered by entry '" << iterator->first.str() << "'." << std::endl;
								}

								switch( iterator->second.type ) {
									case OidRegistrationEntry::OBJECT:
									case OidRegistrationEntry::INSTANCE:
										// Just play it safe for now. 
										if( oid.equals( iterator->first ) ) {
											success = iterator->second.getFunction( oid, varbind->value );
										}
										break;
									case OidRegistrationEntry::TREE:
										// Just play it safe for now. 
										if( oid.beginsWith( iterator->first ) ) {
											success = iterator->second.getFunction( oid, varbind->value );
										}
										break;
								}
							}

							if( ! success ) {
								varbind->value.set_null( Variant::CONTEXT_NOSUCHINSTANCE ); //< TODO: When to use NOSUCHOBJECT?
							}

							responsePdu.varbinds.push_back( std::move( varbind ) );
						}
						break;
					}
					case encoding::agentx::PDU::AGENTX_GETNEXT_PDU: {
						if( logger.system( Logger::AGENTX ) ) {
							logger.out() << "AgentX::" << __FUNCTION__ << ": Received a 'GetNext' PDU." << std::endl;
						}

						encoding::agentx::AgentxGetNextPdu pdu;
						encoding::agentx::AGENTX_GETNEXT_PDU::read( pdu, packet->pduStream );

						if( logger.system( Logger::AGENTX ) ) {
							logger.out() << "AgentX::" << __FUNCTION__ << ": 'GetNext' PDU (" << pdu.ranges.size() << " ranges):" << std::endl;
							for( const auto& range : pdu.ranges ) {
								logger.out() << "AgentX::" << __FUNCTION__ << ":    " <<
									"From " << range.start.str() << " " << ( range.startInclusive ? "inclusive" : "exclusive" ) << " " <<
									"to " << range.end.str() << " " << ( range.endInclusive ? "inclusive" : "exclusive" ) << std::endl;
							}
						}

						for( const auto& range : pdu.ranges ) {
							std::unique_ptr<VarBind> varbind( new VarBind() );
							
							const auto& oid = range.start;

							if( logger.system( Logger::AGENTX ) ) {
								logger.out() << "AgentX::" << __FUNCTION__ << ": Looking for the registration that covers '" << oid.str() << "'." << std::endl;
							}
							auto iterator = registrationThatCovers( oid );
							
							bool success = false;
							if( iterator == oidRegistrationMap.end() ) {
								if( logger.system( Logger::AGENTX ) ) {
									logger.out() << "AgentX::" << __FUNCTION__ << ": No coverage for OID '" << oid.str() << "'." << std::endl;
								}
							} else {
								if( logger.system( Logger::AGENTX ) ) {
									logger.out() << "AgentX::" << __FUNCTION__ << ": OID '" << oid.str() << "' is covered by entry '" << iterator->first.str() << "'." << std::endl;
								}

								while( ! success && iterator != oidRegistrationMap.end() ) {
									switch( iterator->second.type ) {
										case OidRegistrationEntry::OBJECT:
										case OidRegistrationEntry::INSTANCE:
											// If we are not allowed to *include* with this OID, then we're just going to
											// skip this entry and let the next one take care of it.
											if( ! range.startInclusive && oid.equals( iterator->first ) ) {
												break;
											}

											varbind->oid = iterator->first;
											success = iterator->second.getFunction( oid, varbind->value );
											break;
										case OidRegistrationEntry::TREE:
											// If we *are* allowed to include with this OID, then we're first going
											// to do a simple "get" check to see if it's there.  If it is, great.
											// Otherwise, we'll be doing a "get-next" normally.
											if( range.startInclusive && oid.beginsWith( iterator->first ) ) {
												varbind->oid = iterator->first;
												success = iterator->second.getFunction( oid, varbind->value );
												if( success ) {
													break;
												}
											}

											success = iterator->second.getNextFunction( oid, varbind->oid, varbind->value );
											break;
									}

									iterator++;
								}
							}

							// If the end of the range is not null, then we have to make sure that we
							// throw out this OID if it is outside of that range.
							if( range.end.numbers.size() > 0 ) {
								if( range.end.isLessThan( varbind->oid ) || ( ! range.endInclusive && varbind->oid.equals( range.end ) ) ) {
									if( logger.system( Logger::AGENTX ) ) {
										logger.out() << "AgentX::" << __FUNCTION__ << ": 'GetNext': That OID is too far." << std::endl;
									}
									success = false;
								}
							}

							if( ! success ) {
								varbind->oid = range.start;
								varbind->value.set_null( Variant::CONTEXT_ENDOFMIBVIEW );
							}

							responsePdu.varbinds.push_back( std::move( varbind ) );
						}
						break;
					}
					default:
						if( logger.system( Logger::AGENTX ) ) {
							logger.out() << "AgentX::" << __FUNCTION__ << ": !!! UNHANDLED PDU TYPE: " << (int)packet->header.type << " !!!." << std::endl;
						}

						// Since this is a last-ditch error, we are going to set the error
						// field to "genErr" (general error).
						responsePdu.error = PDUErrorStatus::genErr;
				}
			}
				
			bool success = encoding::agentx::AGENTX_RESPONSE_PDU::write( responsePdu, responsePacket.pduStream );
			if( ! success ) {
				throw Exception( "Could not encode the agentx-Response-PDU." );
			}

			rawWrite( responsePacket );
		}
	}
	
	if( logger.system( Logger::AGENTX ) ) {
		logger.out() << "AgentX::" << __FUNCTION__ << ": Finishing." << std::endl;
	}
}

} }

