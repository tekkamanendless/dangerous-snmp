#include <unittest++/UnitTest++.h>
#include <unittest++/TestReporterStdout.h>

#include <iostream>
#include <string>

int main( int argc, char** argv ) {
	bool hasSuite = false;
	std::string suite;

	for( int i = 1; i < argc; i++ ) {
		std::string parameter = argv[ i ];

		if( ! hasSuite ) {
			suite = parameter;
			hasSuite = true;
		} else {
			std::cerr << "Invalid parameter; expected a single suite name." << std::endl;
			return 1;
		}
	}

	if( hasSuite ) {
		UnitTest::TestReporterStdout reporter;
		UnitTest::TestRunner runner( reporter );
		UnitTest::TestList testList = UnitTest::Test::GetTestList();

		return runner.RunTestsIf( testList, suite.c_str(), UnitTest::True(), 0 );
		//IDEAL: return UnitTest::RunAllTests( testReporter, testList, suite.c_str(), 100 /*milliseconds*/ );
	} else {
		return UnitTest::RunAllTests();
	}
}

