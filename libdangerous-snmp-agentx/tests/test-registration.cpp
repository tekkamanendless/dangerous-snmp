#include <dangerous/snmp/agentx.hpp>
#include <dangerous/snmp/logger.hpp>
using namespace dangerous;

#include <cstring>
#include <iostream>

#include <unittest++/UnitTest++.h>

SUITE(Fast) {

TEST(TestRegistration_Simple) {
	snmp::logger.system( snmp::Logger::AGENTX, true );
	//for( int i = 0; i < snmp::Logger::MAX; i++ ) {
	//	snmp::logger.system( (snmp::Logger::System)i, true );
	//}
	
	snmp::Context context;
	snmp::AgentX agentx( context );

	snmp::NumericOid oid( "1.3.6.1.4.1.41326.999.1" );
	snmp::AgentX::OidRegistrationEntry oidRegistrationEntry;
	oidRegistrationEntry.type = snmp::AgentX::OidRegistrationEntry::OBJECT;
	
	bool success = true;
	try {
		agentx.registerOid( oid, oidRegistrationEntry );
	} catch( snmp::Exception& e ) {
		success = false;
	}

	CHECK( success );
}

TEST(TestRegistration_Simple2) {
	snmp::logger.system( snmp::Logger::AGENTX, true );
	//for( int i = 0; i < snmp::Logger::MAX; i++ ) {
	//	snmp::logger.system( (snmp::Logger::System)i, true );
	//}
	
	snmp::Context context;
	snmp::AgentX agentx( context );

	for( auto string : { "1.3.6.1.4.1.41326.999.1.1", "1.3.6.1.4.1.41326.999.2.1", "1.3.6.1.4.1.41326.999.3.1", "1.3.6.1.4.1.41326.999.1.2" } ) {
		snmp::NumericOid oid( string );
		snmp::AgentX::OidRegistrationEntry oidRegistrationEntry;
		oidRegistrationEntry.type = snmp::AgentX::OidRegistrationEntry::OBJECT;
		
		bool success = true;
		try {
			agentx.registerOid( oid, oidRegistrationEntry );
		} catch( snmp::Exception& e ) {
			success = false;
		}

		CHECK( success );
	}
}

TEST(TestRegistration_SimpleFailure) {
	snmp::logger.system( snmp::Logger::AGENTX, true );
	//for( int i = 0; i < snmp::Logger::MAX; i++ ) {
	//	snmp::logger.system( (snmp::Logger::System)i, true );
	//}
	
	snmp::Context context;
	snmp::AgentX agentx( context );

	for( auto string : { "1.3.6.1.4.1.41326.999.1.1", "1.3.6.1.4.1.41326.999.2.1", "1.3.6.1.4.1.41326.999.3.1", "1.3.6.1.4.1.41326.999.1.2" } ) {
		snmp::NumericOid oid( string );
		snmp::AgentX::OidRegistrationEntry oidRegistrationEntry;
		oidRegistrationEntry.type = snmp::AgentX::OidRegistrationEntry::OBJECT;
		
		bool success = true;
		try {
			agentx.registerOid( oid, oidRegistrationEntry );
		} catch( snmp::Exception& e ) {
			success = false;
		}

		CHECK( success );
	}

	for( auto string : { "1.3.6.1.4.1.41326.999.1.1", "1.3.6.1.4.1.41326.999.2.1", "1.3.6.1.4.1.41326.999.3.1", "1.3.6.1.4.1.41326.999.1.2" } ) {
		snmp::NumericOid oid( string );
		snmp::AgentX::OidRegistrationEntry oidRegistrationEntry;
		oidRegistrationEntry.type = snmp::AgentX::OidRegistrationEntry::OBJECT;
		
		bool success = true;
		try {
			agentx.registerOid( oid, oidRegistrationEntry );
		} catch( snmp::Exception& e ) {
			success = false;
		}

		CHECK( ! success );
	}
}

TEST(TestRegistration_TreeFailure) {
	snmp::logger.system( snmp::Logger::AGENTX, true );
	//for( int i = 0; i < snmp::Logger::MAX; i++ ) {
	//	snmp::logger.system( (snmp::Logger::System)i, true );
	//}
	
	snmp::Context context;
	snmp::AgentX agentx( context );

	for( auto string : { "1.3.6.1.4.1.41326.999.1.1", "1.3.6.1.4.1.41326.999.2.1", "1.3.6.1.4.1.41326.999.3.1", "1.3.6.1.4.1.41326.999.1.2" } ) {
		snmp::NumericOid oid( string );
		snmp::AgentX::OidRegistrationEntry oidRegistrationEntry;
		oidRegistrationEntry.type = snmp::AgentX::OidRegistrationEntry::TREE;
		
		bool success = true;
		try {
			agentx.registerOid( oid, oidRegistrationEntry );
		} catch( snmp::Exception& e ) {
			success = false;
		}

		CHECK( success );
	}

	for( auto string : { "1.3.6.1.4.1.41326.999.1.1.1", "1.3.6.1.4.1.41326.999.2.1.1", "1.3.6.1.4.1.41326.999.3.1.1", "1.3.6.1.4.1.41326.999.1.2.1" } ) {
		snmp::NumericOid oid( string );
		snmp::AgentX::OidRegistrationEntry oidRegistrationEntry;
		oidRegistrationEntry.type = snmp::AgentX::OidRegistrationEntry::OBJECT;
		
		bool success = true;
		try {
			agentx.registerOid( oid, oidRegistrationEntry );
		} catch( snmp::Exception& e ) {
			success = false;
		}

		CHECK( ! success );
	}
}

}

